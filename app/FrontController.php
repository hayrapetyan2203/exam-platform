<?php

namespace backend;

//imports important parts of application
require_once(__DIR__ . '/components/Config.php');
require_once(__DIR__ . '/components/Translation.php');

//import models
require_once (__DIR__ . '/models/UserModel.php');
require_once (__DIR__ . '/models/ArticleModel.php');
require_once (__DIR__ . '/models/GroupModel.php');
require_once (__DIR__ . '/models/TestModel.php');
require_once (__DIR__ . '/models/ExamModel.php');
require_once (__DIR__ . '/models/StatisticsModel.php');

use Config;

Config::init();
class FrontController
{
    public static function route($controller, $action, $params){
        if(!isset($_GET['locale'])){
            $_GET['locale'] = 'hy';
        }
        $_SESSION['appLocale'] = $_GET['locale'];

        $className = $controller.'Controller';
        require_once ('./controllers/'.$className.'.php');
        $controllerInstance = new $className();
        return $controllerInstance->$action();
    }
}
   
$action = 'notFound';
$params = [];
$controller = 'Base';
if(isset($_GET['controller'])){
    $controller = ucfirst(trim(stripcslashes(htmlspecialchars($_GET['controller']))));
}

if(isset($_GET['action'])){
    $action = ucfirst(trim(stripcslashes(htmlspecialchars($_GET['action']))));
}

if(isset($_GET['params'])){
    $params = trim(stripcslashes(htmlspecialchars($_GET['params'])));
}
FrontController::route($controller, $action, $params);