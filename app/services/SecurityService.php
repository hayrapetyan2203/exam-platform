<?php

namespace backend\services;

use backend\models\SecurityModel;
use Exception;

require_once (__DIR__ . "/../models/SecurityModel.php");
class SecurityService
{
    /**
     * @throws Exception
     */
    public static function addFailLoginAttempt($user){
        SecurityModel::addFailLoginAttempt($user, $_SERVER['REMOTE_ADDR']);
    }

    /**
     * @throws Exception
     */
    public static function checkLoginAttempts($user): bool
    {
        return SecurityModel::checkLoginAttempts($user, $_SERVER['REMOTE_ADDR']);
    }

}