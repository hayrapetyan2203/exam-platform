<?php

require_once './components/Config.php';
require_once './components/Translation.php';

Config::init();
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta content="width=device-width, initial-scale=1.0" name="viewport">

    <title><?=Translation::make($_SESSION['appLocale'], 'indexTitle')?></title>
    <meta content="" name="description">
    <meta content="" name="keywords">

    <!-- Favicons -->
    <!--  <link href="assets/img/favicon.png" rel="icon">-->
    <link href="assets/img/apple-touch-icon.png" rel="apple-touch-icon">

    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Nunito:300,300i,400,400i,600,600i,700,700i|Poppins:300,300i,400,400i,500,500i,600,600i,700,700i" rel="stylesheet">

    <!-- Vendor CSS Files -->
    <link href="assets/vendor/aos/aos.css" rel="stylesheet">
    <link href="assets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="assets/vendor/bootstrap-icons/bootstrap-icons.css" rel="stylesheet">
    <link href="assets/vendor/glightbox/css/glightbox.min.css" rel="stylesheet">
    <link href="assets/vendor/remixicon/remixicon.css" rel="stylesheet">
    <link href="assets/vendor/swiper/swiper-bundle.min.css" rel="stylesheet">

    <link href="https://fonts.googleapis.com/css?family=Roboto:300,400&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="assets/fonts/icomoon/style.css">
    <link rel="stylesheet" href="assets/css/owl.carousel.min.css">
    <link rel="stylesheet" href="assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="assets/css/login-style.css">
    <link href="assets/css/style.css" rel="stylesheet">
</head>

<body>
<?php require_once('views/header.php') ?>
<div class="content mt-5">
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <img src="assets/img/undraw_remotely_2j6y.svg" alt="Image" class="img-fluid">
            </div>
            <div class="col-md-6 contents">
                <div class="row justify-content-center">
                    <div class="col-md-8">
                        <div class="mb-4">
                            <h3><?=Translation::make($_SESSION['appLocale'], 'signIn') ?></h3>
                            <p class="mb-4"><?=Translation::make($_SESSION['appLocale'], 'neverShare') ?>.</p>
                        </div>

                        <form action="FrontController.php?controller=auth&action=login&locale=<?=$_SESSION['appLocale']; ?>" method="post">
                            <div class="form-group first">
                                <label for="useremail"><?=Translation::make($_SESSION['appLocale'], 'emailAddress') ?></label>
                                <input type="email" name="useremail"  required class="form-control" id="useremail">

                            </div>
                            <div class="form-group last">
                                <label for="password"><?=Translation::make($_SESSION['appLocale'], 'password') ?></label>
                                <div class="input-group" style="gap:5px">
                                    <input type="password"  class="form-control password" id="password" required name="password" aria-label="Password" aria-describedby="password-toggle">
                                    <i class="bi bi-eye-fill password-icon password-toggle"></i>
                                </div>
                            </div>

                            <div class="d-flex mb-5 align-items-center">
                                <label class="control control--checkbox mb-0"><span class="caption"><?=Translation::make($_SESSION['appLocale'], 'rememberMe') ?></span>
                                    <input type="checkbox" checked="checked"/>
                                    <div class="control__indicator"></div>
                                </label>
                                <span class="ml-auto"><a href="FrontController.php?controller=auth&action=forgotPassword&locale=<?=$_SESSION['appLocale']; ?>" class="forgot-pass"><?=Translation::make($_SESSION['appLocale'], 'forgotPassword') ?></a></span>
                            </div>

                            <input type="submit" value="<?=Translation::make($_SESSION['appLocale'], 'signIn') ?>" class="btn btn-block btn-primary">
                            <div class="mt-2">
                                <small><?=Translation::make($_SESSION['appLocale'], 'dontHaveAcc') ?>
                                    <a href="register.php?locale=<?=$_SESSION['appLocale']?>">
                                        <?=Translation::make($_SESSION['appLocale'], 'signUp') ?>
                                    </a></small>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Vendor JS Files -->
<script src="assets/vendor/purecounter/purecounter_vanilla.js"></script>
<script src="assets/vendor/aos/aos.js"></script>
<script src="assets/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
<script src="assets/vendor/glightbox/js/glightbox.min.js"></script>
<script src="assets/vendor/isotope-layout/isotope.pkgd.min.js"></script>
<script src="assets/vendor/swiper/swiper-bundle.min.js"></script>
<script src="assets/vendor/php-email-form/validate.js"></script>


<script src="assets/js/jquery-3.3.1.min.js"></script>
<script src="assets/js/popper.min.js"></script>
<script src="assets/js/bootstrap.min.js"></script>
<script>
    $(document).ready(function() {
        $('input[type="text"], input[type="password"], input[type="email"], textarea').val('');
    });
</script>
<!-- Template Main JS File -->
<script src="assets/js/main.js"></script>
</body>
</html>