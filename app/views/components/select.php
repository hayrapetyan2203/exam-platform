<?php 
    if(!isset($multiple)){
        $multiple = true;
    }
?>


<?php if($showLabel): ?>
    <label for="<?=$id ?>"><?=$label ?></label>
<?php endif; ?>

<?php if($multiple): ?>
    <select multiple class="<?=$class ?>" id="<?=$id ?>" name="<?=$id ?>" style="width: 100%">
        <?php foreach($options as $option): ?>
            <option value="<?=$option[$optionId] ?>"><?=$option[$optionName] ?></option>
        <?php endforeach; ?>
    </select>

<?php else: ?>
    <select class="<?=$class ?>" id="<?=$id ?>" name="<?=$id ?>" style="width: 100%">
        <?php foreach($options as $option): ?>
            <option value="<?=$option[$optionId] ?>"><?=$option[$optionName] ?></option>
        <?php endforeach; ?>
    </select>
<?php endif; ?>

<!-- select2 cdn -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.13/js/select2.min.js"></script>
<script>
    selectID = "<?=$id ?>";
    $('#'+selectID).select2();
</script>