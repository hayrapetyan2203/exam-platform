<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/css/bootstrap.min.css">
<link href="https://cdn.jsdelivr.net/npm/bootstrap@4.4.1/dist/css/bootstrap.min.css" rel="stylesheet">

<script src="https://code.jquery.com/jquery-1.10.2.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.4.1/dist/js/bootstrap.bundle.min.js"></script>
<?=$this->import('professor_profile', 'css'); ?>

<div class="container">
    <?=$this->render('header', [], 'layout'); ?>

    <div class="col-12 col-sm-12 col-md-12">
      <div class="card">
        <!-- <div class="card-header">
          <h4><?= Translation::make($_SESSION['appLocale'], 'watch') ?> <?= Translation::make($_SESSION['appLocale'], 'tests') ?></h4>
        </div> -->
        <div class="card-body">
          <div class="media-list position-relative">
            <div class="table-responsive" id="project-team-scroll" tabindex="1" style="height: 400px; overflow: hidden; outline: none;">
              <table class="table table-hover table-xl mb-0">
                <thead>
                  <tr>
                    <th><?= Translation::make($_SESSION['appLocale'], 'testName') ?></th>
                    <th><?= Translation::make($_SESSION['appLocale'], 'article') ?></th>
                    <th><?= Translation::make($_SESSION['appLocale'], 'maxQuestions') ?></th>
                    <th><?= Translation::make($_SESSION['appLocale'], 'edit') ?></th>
                  </tr>
                </thead>
                <tbody>
                <?php foreach($tests as $test) : ?>
                      <tr>
                        <td class="text-truncate"><?=$test['name'] ?></td>
                        <td class="text-truncate"><?=\backend\models\ArticleModel::findOne(json_decode($test['articles'])[0])['name'] ?></td>
                        <td class="text-truncate"><?=$test['max_questions'] ?></td>
                        <td class="text-truncate">
                            <button class="btn btn-primary btn-sm me-2" data-id="<?= $test['id'] ?>">
                                <?= Translation::make($_SESSION['appLocale'], 'edit') ?>
                            </button>
                            <button class="btn btn-danger btn-sm deleteButton" data-id="<?= $test['id'] ?>">
                                <?= Translation::make($_SESSION['appLocale'], 'delete') ?>
                            </button>
                        </td>
                      </tr>
                <?php endforeach; ?>
                </tbody>
              </table>
            </div>
          </div>
            <?=$this->render('pagination', [], 'layout') ?>
        </div>
      </div>
    </div>
</div>

<script>
    $('.deleteButton').click(function(e) {
        let testId = $(this).attr('data-id');
        $.get("?controller=test&action=delete&id=" + testId, function (data) {
            let response = JSON.parse(data)
            if(response.status === 200){
                location.reload();
            }
        });
    })
</script>