
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
<script src="https://cdn.jsdelivr.net/npm/chart.js"></script>
<!-- Modal for exam results chart -->
<div class="modal fade" id="examResultsModal" tabindex="-1" role="dialog" aria-labelledby="examResultsModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="examResultsModalLabel"><?=Translation::make($_SESSION['appLocale'], 'examResult'); ?></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <canvas id="examResultsChart" width="400" height="200"></canvas>
            </div>
            <div class="modal-footer">
                <h1><?=Translation::make($_SESSION['appLocale'], 'examResult'); ?> : <span id="resultCategory"></span></h1>
                <button type="button" class="btn btn-secondary" data-dismiss="modal"><?=Translation::make($_SESSION['appLocale'], 'close'); ?></button>
            </div>
        </div>
    </div>
</div>

<script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.5.4/dist/umd/popper.min.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>

