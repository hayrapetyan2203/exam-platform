<div class="col-sm-6 mb-3">
    <div class="card h-100">
        <div class="card-body">
            <h6 class="d-flex align-items-center mb-3">
                <i class="material-icons text-info mr-2"><?= Translation::make($_SESSION['appLocale'], 'watch') ?></i>
                <?= Translation::make($_SESSION['appLocale'], 'groups') ?>
            </h6>
            <?php if ($groups): ?>
                <?php foreach ($groups as $key => $group): ?>
                    <div class="d-flex align-items-center justify-content-between">
                        <p><?= $group['name'] ?></p>
                    </div>
                <?php endforeach; ?>
                <a href="?controller=group&action=studentGroupList&locale=<?=$_SESSION['appLocale']?>"><?= Translation::make($_SESSION['appLocale'], 'more') ?></a>
            <?php else: ?>
                <p class="text-center mt-5"><?= Translation::make($_SESSION['appLocale'], 'noGroups') ?></p>
            <?php endif; ?>
        </div>
    </div>
</div>