<div class="col-sm-6 mb-3 ">
    <div class="card h-100">
        <div class="card-body">
            <h6 class="d-flex align-items-center mb-3">
                <i class="material-icons text-info mr-2"><?= Translation::make($_SESSION['appLocale'], 'watch') ?></i>
                <?= Translation::make($_SESSION['appLocale'], 'exams') ?>
            </h6>
            <?php if ($exams): ?>
                <?php foreach ($exams as $key => $exam): ?>
                    <div class="d-flex align-items-center justify-content-between">
                        <p><?= $exam['name'] ?></p>
                        <?php 
                            $startingStatus = backend\models\ExamModel::getStartingStatus($exam['id']);
                         ?>
                        <div class="d-flex align-items-center" style="gap:3px;" 
                        title="
                            <?=Translation::make($_SESSION['appLocale'], $startingStatus['description']); ?>
                        ">
                            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="<?=$startingStatus['color'] ?>" class="bi bi-circle-fill" viewBox="0 0 16 16">
                              <circle cx="8" cy="8" r="8"/>
                            </svg>
                        </div>
                    </div>
                <?php endforeach; ?>
                <a href="?controller=exam&action=studentExamList&locale=<?=$_SESSION['appLocale']?>"><?= Translation::make($_SESSION['appLocale'], 'more') ?></a>
            <?php else: ?>
                <p class="text-center mt-5"><?= Translation::make($_SESSION['appLocale'], 'noExams') ?></p>
            <?php endif; ?>
        </div>
    </div>
</div>