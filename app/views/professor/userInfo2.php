<div class="card userinfo2">
    <div class="card-body">
        <div class="d-flex flex-column align-items-center text-center">
            <?php 
                $actualPath = $is_windows_os ? '/app/' . $_SESSION['user']['imagePath'] : $_SESSION['user']['imagePath'];
                $imagePath = !empty($_SESSION['user']['imagePath']) ? 
                    $actualPath : 
                        'https://bootdey.com/img/Content/avatar/avatar7.png';
            ?>
            <img src="<?=$imagePath ?>" alt="Admin" class="img-thumbnail userinfo-img " width="150" >
            <div class="mt-3">
                <h4><?=$_SESSION['user']['username'] . ' ' .  $_SESSION['user']['userSurname']?></h4>
                <p class="text-secondary mb-1"><?= ((int)$_SESSION['user']['role'] === 1) ?
                        Translation::make($_SESSION['appLocale'], 'professor') :
                        Translation::make($_SESSION['appLocale'], 'student')
                    ?></p>
            </div>
        </div>
    </div>
</div>