<canvas id="annualExamCountChart"></canvas>

<script>

  var title = "<?php echo $title; ?>";

  var january   = "<?php echo $january; ?>";
  var february  = "<?php echo $february; ?>";
  var march     = "<?php echo $march; ?>";
  var april     = "<?php echo $april; ?>";
  var may       = "<?php echo $may; ?>";
  var june      = "<?php echo $june; ?>";
  var july      = "<?php echo $july; ?>";
  var august    = "<?php echo $august; ?>";
  var september = "<?php echo $september; ?>";
  var october   = "<?php echo $october; ?>";
  var november  = "<?php echo $november; ?>";
  var december  = "<?php echo $december; ?>";
  var yValues   = JSON.parse("<?php echo json_encode($yValues); ?>");
  var xValues   = [january, february, march, april, may, june, july, august, september, october, november, december];

  var barColors = [
    "rgba(155, 99, 132, 0.5)",  //january
    "rgba(154, 162, 235, 0.5)", //february
    "rgba(155, 206, 86, 0.5)",  //march
    "rgba(255, 106, 86, 100)",  //april
    "rgba(155, 99, 132, 0.5)",  //may
    "rgba(154, 162, 235, 0.5)", //june
    "rgba(155, 206, 86, 0.5)",  //july
    "rgba(255, 106, 86, 100)",  //august
    "rgba(155, 99, 132, 0.5)",  //september
    "rgba(154, 162, 235, 0.5)", //october
    "rgba(155, 206, 86, 0.5)",  //november
    "rgba(255, 106, 86, 200)"   //december
    ];

  new Chart("annualExamCountChart", {
    type: "bar",
    data: {
      labels: xValues,
      datasets: [{
        backgroundColor: barColors,
        data: yValues
      }]
    },
    options: {
      legend: {display: false},
      title: {
        display: true,
        text: title
      }
    }
  });
</script>