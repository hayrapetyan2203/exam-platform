<div class="mt-2">
    <div class="card h-25">
        <div class="card-body">
            <h6 class="d-flex align-items-center mb-3">
                <i class="material-icons text-info mr-2"><?= Translation::make($_SESSION['appLocale'], 'watch') ?></i>
                <?= Translation::make($_SESSION['appLocale'], 'exams') ?>
            </h6>
            <?php /** @var array $exams */
            if ($exams): ?>
                <?php foreach ($exams as $key => $exam): ?>
                    <div class="d-flex align-items-center justify-content-between">
                        <p><?= $exam['name'] ?></p>
                        <div class="d-flex align-items-center" style="gap:3px;">
                            <!-- view Icon -->
                            <a href="FrontController.php?controller=exam&action=view&locale=hy&id=<?= $exam['id'] ?>">
                                <span style="position: relative; top: -2px;" title="<?= Translation::make($_SESSION['appLocale'], 'view') ?>">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-eye" viewBox="0 0 16 16">
                                        <path d="M16 8s-3-5.5-8-5.5S0 8 0 8s3 5.5 8 5.5S16 8 16 8M1.173 8a13 13 0 0 1 1.66-2.043C4.12 4.668 5.88 3.5 8 3.5s3.879 1.168 5.168 2.457A13 13 0 0 1 14.828 8q-.086.13-.195.288c-.335.48-.83 1.12-1.465 1.755C11.879 11.332 10.119 12.5 8 12.5s-3.879-1.168-5.168-2.457A13 13 0 0 1 1.172 8z" />
                                        <path d="M8 5.5a2.5 2.5 0 1 0 0 5 2.5 2.5 0 0 0 0-5M4.5 8a3.5 3.5 0 1 1 7 0 3.5 3.5 0 0 1-7 0" />
                                    </svg>
                                </span>
                            </a>
                            <!-- Edit Icon -->
                            <span class="editExam" data-toggle="modal" data-target="#editExam" data-id="<?= $exam['id'] ?>" style="position: relative; top: -2px;" title="<?= Translation::make($_SESSION['appLocale'], 'edit') ?>">
                                <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-pencil-square" viewBox="0 0 16 16">
                                  <path d="M15.502 1.94a.5.5 0 0 1 0 .706L14.459 3.69l-2-2L13.502.646a.5.5 0 0 1 .707 0l1.293 1.293zm-1.75 2.456-2-2L4.939 9.21a.5.5 0 0 0-.121.196l-.805 2.414a.25.25 0 0 0 .316.316l2.414-.805a.5.5 0 0 0 .196-.12l6.813-6.814z"/>
                                  <path fill-rule="evenodd" d="M1 13.5A1.5 1.5 0 0 0 2.5 15h11a1.5 1.5 0 0 0 1.5-1.5v-6a.5.5 0 0 0-1 0v6a.5.5 0 0 1-.5.5h-11a.5.5 0 0 1-.5-.5v-11a.5.5 0 0 1 .5-.5H9a.5.5 0 0 0 0-1H2.5A1.5 1.5 0 0 0 1 2.5z"/>
                                </svg>
                            </span>
                            <span style="position: relative; top: -2px;" title="<?= Translation::make($_SESSION['appLocale'], 'deleteGroup') ?>">
                                <svg style="cursor:pointer" data-id="<?= $exam['id'] ?>" xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-x-square-fill examDeleteButton" 
                                    viewBox="0 0 16 16">
                                     <path d="M2 0a2 2 0 0 0-2 2v12a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2V2a2 2 0 0 0-2-2zm3.354 4.646L8 7.293l2.646-2.647a.5.5 0 0 1 .708.708L8.707 8l2.647 2.646a.5.5 0 0 1-.708.708L8 8.707l-2.646 2.647a.5.5 0 0 1-.708-.708L7.293 8 4.646 5.354a.5.5 0 1 1 .708-.708"/>
                                </svg>
                            </span>
                        </div>
                    </div>
                <?php endforeach; ?>
                <a href="?controller=exam&action=list&locale=<?=$_SESSION['appLocale']?>"><?= Translation::make($_SESSION['appLocale'], 'more') ?></a>
            <?php else: ?>
                <p class="text-center mt-5"><?= Translation::make($_SESSION['appLocale'], 'noExams') ?></p>
            <?php endif; ?>
        </div>
    </div>
</div>

<script>
    $('.examDeleteButton').click(function(e) {
        let examId = $(this).attr('data-id');
        $.get("?controller=exam&action=delete&id=" + examId, function (data) {
            let response = JSON.parse(data)
            if(response.status === 200){
                location.reload();
            }
        });
    })    

    $('.examEditButton').click(function(e) {
        $('#examName').val($(this).attr('data-name'));
        $('#createExamModal').modal('toggle')
    })
</script>