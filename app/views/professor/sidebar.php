<div class="card mt-2">
    <ul class="list-group list-group-flush">
        <li class="list-group-item d-flex justify-content-between align-items-center flex-wrap">
            <h6 class="mb-0" data-toggle="modal" data-target="#messagesModal">
                <?= $this->render('message', [], 'svg'); ?>
                <span style="cursor:pointer"><?= Translation::make($_SESSION['appLocale'], 'messages') ?></span>
            </h6>
        </li>
        <li class="list-group-item d-flex justify-content-between align-items-center flex-wrap">
            <h6 class="mb-0" data-toggle="modal" data-target="#createGroupModal">
                <?= $this->render('group', [], 'svg'); ?>
                <span style="cursor:pointer"><?= Translation::make($_SESSION['appLocale'], 'createGroup') ?></span>
            </h6>
        </li>
        <li class="list-group-item d-flex justify-content-between align-items-center flex-wrap">
            <h6 class="mb-0" data-toggle="modal" data-target="#createTestModal">
                <?= $this->render('test', [], 'svg'); ?>
                <span style="cursor:pointer"><?= Translation::make($_SESSION['appLocale'], 'createTest') ?></span>
            </h6>
        </li>
        <li class="list-group-item d-flex justify-content-between align-items-center flex-wrap">
            <h6 class="mb-0" data-toggle="modal" data-target="#createExamModal">
                <?= $this->render('exam', [], 'svg'); ?>
                <span style="cursor:pointer"><?= Translation::make($_SESSION['appLocale'], 'createExam') ?></span>
            </h6>
        </li>
        <li class="list-group-item d-flex justify-content-between align-items-center flex-wrap">
            <h6 class="mb-0" data-toggle="modal" data-target="#statisticsModal">
                <?= $this->render('exam', [], 'svg'); ?>
                <a href="?controller=statistics&action=index&locale=<?=$_SESSION['appLocale']?>" class="text-dark text-decoration-none">
                    <span style="cursor:pointer"><?= Translation::make($_SESSION['appLocale'], 'statistics') ?></span>
                </a>
            </h6>
        </li>
    </ul>
</div>