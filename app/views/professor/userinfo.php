<div class="card mb-2">
    <div class="card-body">
        <div class="row">
            <div class="col-sm-4">
                <h6 class="mb-0"><?=Translation::make($_SESSION['appLocale'], 'fullname') ?></h6>
            </div>
            <div class="col-sm-8 text-secondary">
                <?=$_SESSION['user']['username'] . ' ' .  $_SESSION['user']['userSurname'] . ' ' .  $_SESSION['user']['userFathername']?>
            </div>
        </div>
        <hr>
        <div class="row">
            <div class="col-sm-4">
                <h6 class="mb-0"><?=Translation::make($_SESSION['appLocale'], 'emailAddress') ?></h6>
            </div>
            <div class="col-sm-8 text-secondary">
                <?=$_SESSION['user']['useremail'] ?>
            </div>
        </div>
        <hr>
        <div class="row">
            <div class="col-sm-4">
                <h6 class="mb-0"><?=Translation::make($_SESSION['appLocale'], 'phone') ?></h6>
            </div>
            <div class="col-sm-8 text-secondary">
                <?=$_SESSION['user']['phone'] ?>
            </div>
        </div>
        <hr>
        <div class="row">
            <div class="col-sm-4">
                <h6 class="mb-0"><?=Translation::make($_SESSION['appLocale'], 'address') ?></h6>
            </div>
            <div class="col-sm-8 text-secondary">
                <?=$_SESSION['user']['address'] ?>
            </div>
        </div>
        <hr>
        <div class="row">
            <div class="col-sm-12">
                <a class="btn btn-info text-white" id="editUserinfo" data-toggle="modal" data-target="#editUserinfoModal">
                    <?=Translation::make($_SESSION['appLocale'], 'edit') ?>
                </a>
            </div>
        </div>
    </div>
</div>