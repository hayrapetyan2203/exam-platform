<div class="px-4 d-none d-md-block">
    <div class="d-flex align-items-center">
        <div class="flex-grow-1">
            <input type="text" class="form-control my-3 searchUsers" placeholder="<?=$searchTranslation?>...">
            <div class="selectedMembers"></div>
        </div>
    </div>
</div>
<div class="messageUserslist">
</div>
