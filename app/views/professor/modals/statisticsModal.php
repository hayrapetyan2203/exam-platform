<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Your Page Title</title>
    <script src="https://code.jquery.com/jquery-3.6.4.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.0/dist/js/bootstrap.bundle.min.js"></script>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.0/dist/css/bootstrap.min.css" rel="stylesheet">
    <script src="https://cdn.jsdelivr.net/npm/chart.js"></script>
</head>
<body>

<!-- Statistics Modal -->
<div class="modal fade " id="statisticsModal" tabindex="-1" role="dialog" aria-labelledby="messagesModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-xl" role="document"> <!-- Set modal-xl for a larger modal -->
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="messagesModalLabel"><?=Translation::make($_SESSION['appLocale'], 'messages') ?></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>

            <div class="modal-body p-5">


                <div class="container mt-5">
                    <h1 class="mb-4">Statistics Page</h1>

                    <!-- Chart 1: Line Chart -->
                    <div class="row mb-4">
                        <div class="col-md-12">
                            <canvas id="lineChart" class="chart"></canvas>
                        </div>

                    </div>

                    <!-- Chart 2: Bar Chart -->
                    <div class="row mb-4">
                        <div class="col-md-12">
                            <canvas id="barChart" class="chart"></canvas>
                        </div>

                    </div>

                    <!-- Chart 3: Doughnut Chart -->
                    <div class="row mb-4">
                        <div class="col-md-6">
                            <canvas id="doughnutChart" class="chart"></canvas>
                        </div>
                        <div class="col-md-6">
                            <canvas id="pieChart" class="chart"></canvas>
                        </div>
                    </div>
                </div>


                <!-- Chart 5: Radar Chart -->
                <div class="row mb-4">
                    <div class="col-md-6">
                        <canvas id="radarChart" class="chart"></canvas>
                    </div>
                    <div class="col-md-6">
                        <canvas id="polarAreaChart" class="chart"></canvas>
                    </div>
                </div>

                <script>
                    // Chart 1: Line Chart
                    var ctxLine = document.getElementById('lineChart').getContext('2d');
                    var lineChart = new Chart(ctxLine, {
                        type: 'line',
                        data: {
                            labels: ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday'],
                            datasets: [{
                                label: 'Daily Sales',
                                data: [12, 19, 3, 5, 2, 3, 9],
                                borderColor: 'rgba(75, 192, 192, 1)',
                                borderWidth: 2,
                                fill: false
                            }]
                        },
                        options: {
                            scales: {
                                y: {
                                    beginAtZero: true
                                }
                            }
                        }
                    });

                    // Chart 2: Bar Chart
                    var ctxBar = document.getElementById('barChart').getContext('2d');
                    var barChart = new Chart(ctxBar, {
                        type: 'bar',
                        data: {
                            labels: ['Product A', 'Product B', 'Product C', 'Product D', 'Product E'],
                            datasets: [{
                                label: 'Sales',
                                data: [25, 32, 45, 22, 38],
                                backgroundColor: 'rgba(255, 99, 132, 0.7)',
                                borderColor: 'rgba(255, 99, 132, 1)',
                                borderWidth: 1
                            }]
                        },
                        options: {
                            scales: {
                                y: {
                                    beginAtZero: true
                                }
                            }
                        }
                    });

                    // Chart 3: Doughnut Chart
                    var ctxDoughnut = document.getElementById('doughnutChart').getContext('2d');
                    var doughnutChart = new Chart(ctxDoughnut, {
                        type: 'doughnut',
                        data: {
                            labels: ['Category A', 'Category B', 'Category C', 'Category D', 'Category E'],
                            datasets: [{
                                data: [15, 20, 25, 10, 30],
                                backgroundColor: ['rgba(255, 99, 132, 0.7)', 'rgba(54, 162, 235, 0.7)', 'rgba(255, 206, 86, 0.7)', 'rgba(75, 192, 192, 0.7)', 'rgba(153, 102, 255, 0.7)'],
                                hoverBackgroundColor: ['rgba(255, 99, 132, 1)', 'rgba(54, 162, 235, 1)', 'rgba(255, 206, 86, 1)', 'rgba(75, 192, 192, 1)', 'rgba(153, 102, 255, 1)'],
                                borderWidth: 1
                            }]
                        }
                    });

                    // Chart 4: Pie Chart
                    var ctxPie = document.getElementById('pieChart').getContext('2d');
                    var pieChart = new Chart(ctxPie, {
                        type: 'pie',
                        data: {
                            labels: ['Category X', 'Category Y', 'Category Z'],
                            datasets: [{
                                data: [30, 40, 30],
                                backgroundColor: ['rgba(255, 99, 132, 0.7)', 'rgba(54, 162, 235, 0.7)', 'rgba(255, 206, 86, 0.7)'],
                                hoverBackgroundColor: ['rgba(255, 99, 132, 1)', 'rgba(54, 162, 235, 1)', 'rgba(255, 206, 86, 1)'],
                                borderWidth: 1
                            }]
                        }
                    });

                    // Chart 5: Radar Chart
                    var ctxRadar = document.getElementById('radarChart').getContext('2d');
                    var radarChart = new Chart(ctxRadar, {
                        type: 'radar',
                        data: {
                            labels: ['Metric A', 'Metric B', 'Metric C', 'Metric D', 'Metric E'],
                            datasets: [{
                                label: 'Performance',
                                data: [70, 80, 65, 90, 75],
                                borderColor: 'rgba(75, 192, 192, 1)',
                                borderWidth: 2,
                                fill: true,
                                backgroundColor: 'rgba(75, 192, 192, 0.2)'
                            }]
                        },
                        options: {
                            scales: {
                                r: {
                                    beginAtZero: true
                                }
                            }
                        }
                    });

                    // Chart 6: Polar Area Chart
                    var ctxPolarArea = document.getElementById('polarAreaChart').getContext('2d');
                    var polarAreaChart = new Chart(ctxPolarArea, {
                        type: 'polarArea',
                        data: {
                            labels: ['Factor A', 'Factor B', 'Factor C', 'Factor D', 'Factor E'],
                            datasets: [{
                                data: [20, 30, 25, 35, 40],
                                backgroundColor: ['rgba(255, 99, 132, 0.7)', 'rgba(54, 162, 235, 0.7)', 'rgba(255, 206, 86, 0.7)', 'rgba(75, 192, 192, 0.7)', 'rgba(153, 102, 255, 0.7)'],
                                hoverBackgroundColor: ['rgba(255, 99, 132, 1)', 'rgba(54, 162, 235, 1)', 'rgba(255, 206, 86, 1)', 'rgba(75, 192, 192, 1)', 'rgba(153, 102, 255, 1)'],
                                borderWidth: 1
                            }]
                        }
                    });
                </script>
            </div>
        </div>
    </div>
</div>
</body>
</html>
