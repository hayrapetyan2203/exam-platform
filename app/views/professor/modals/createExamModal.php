<!-- Modal -->
<div class="modal fade" id="createExamModal" tabindex="-1" role="dialog" aria-labelledby="createGroupModalLabel"
    aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="createGroupModalLabel">
                    <?= Translation::make($_SESSION['appLocale'], 'createExam') ?></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>

            <div class="modal-body">
                <p><?= Translation::make($_SESSION['appLocale'], 'createExamDescription') ?></p>

                <form>
                    <div class="form-group">
                        <label for="examName"><?= Translation::make($_SESSION['appLocale'], 'examName') ?></label>
                        <input required type="text" class="form-control" id="examName" name="examName">
                    </div>

                    <div class="form-group">
                        <label for="chooseGroop"><?= Translation::make($_SESSION['appLocale'], 'chooseGroop') ?></label>
                        <select class="form-control" id="chooseGroop" name="chooseGroop">
                            <?php foreach ($groups as $key => $group): ?>
                                <option value="<?= $group['id'] ?>"><?= $group['name'] ?></option>
                            <?php endforeach; ?>
                        </select>
                    </div>

                    <div class="form-group">
                        <label
                            for="chooseDuration"><?= Translation::make($_SESSION['appLocale'], 'chooseDuration') ?></label>
                        <input type="number" id="chooseDuration" class="form-control">
                    </div>

                    <div class="form-group">
                        <?= $this->render('select', [
                            'id' => 'chooseArticle',
                            'class' => 'form-control',
                            'showLabel' => true,
                            'label' => Translation::make($_SESSION['appLocale'], 'chooseArticle'),
                            'options' => $articles,
                            'optionId' => 'id',
                            'optionName' => 'name',
                            'multiple' => true
                        ], 'components') ?>
                    </div>

                    <div class="form-group">
                        <?= $this->render('select', [
                            'id' => 'chooseTest',
                            'class' => 'form-control',
                            'showLabel' => true,
                            'label' => Translation::make($_SESSION['appLocale'], 'chooseTest'),
                            'options' => $tests,
                            'optionId' => 'id',
                            'optionName' => 'name',
                            'multiple' => true
                        ], 'components') ?>
                    </div>

                     <div class="form-group">
                        <?= $this->render('select', [
                            'id' => 'chooseExamType',
                            'class' => 'form-control',
                            'showLabel' => true,
                            'label' => Translation::make($_SESSION['appLocale'], 'chooseExamType'),
                            'options' => [
                                [
                                    'id' => 1,
                                    'name' => Translation::make($_SESSION['appLocale'], 'examWithEqualPointQuestions')
                                ],
                                [
                                    'id' => 2,
                                    'name' => Translation::make($_SESSION['appLocale'], 'examWithDifferentPointQuestions')
                                ]
                            ],
                            'optionId' => 'id',
                            'optionName' => 'name',
                            'multiple' => false
                        ], 'components') ?>
                    </div>

                    <button type="button" id="submitArticleCreate" name="submitArticleCreate"
                        class="btn btn-primary"><?= Translation::make($_SESSION['appLocale'], 'create') ?></button>
                </form>
            </div>
        </div>
    </div>
</div>