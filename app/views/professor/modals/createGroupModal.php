<!-- Modal -->
<div class="modal fade" id="createGroupModal" tabindex="-1" role="dialog" aria-labelledby="createGroupModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="createGroupModalLabel"><?=Translation::make($_SESSION['appLocale'], 'createGroup') ?></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p><?=Translation::make($_SESSION['appLocale'], 'createGroupDescription') ?></p>
                <form method="post" action="?controller=group&action=create">
                    <div class="form-group">
                        <label for="groupName"><?=Translation::make($_SESSION['appLocale'], 'groupName') ?></label>
                        <input required type="text" class="form-control" id="groupName" name="groupName" >
                    </div>
                    <div class="form-group">
                        <?=$this->render('select', [
                            'id'         => 'groupMembers',
                            'class'      => 'form-control',
                            'showLabel'  => true,
                            'label'      => Translation::make($_SESSION['appLocale'], 'addMembers'),
                            'options'    => UserModel::listStudents(),
                            'optionId'   => 'id',
                            'optionName' => 'fullname',
                        ], 'components') ?>
                    </div>
                    <button type="button"  id="submitGroupCreate" name="submitGroupCreate" class="btn btn-primary"><?=Translation::make($_SESSION['appLocale'], 'create') ?></button>
                </form>
            </div>
        </div>
    </div>
</div>