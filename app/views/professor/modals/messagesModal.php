<div class="modal fade" id="messagesModal" tabindex="-1" role="dialog" aria-labelledby="messagesModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-xl" role="document"> <!-- Set modal-xl for a larger modal -->
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="messagesModalLabel"><?=Translation::make($_SESSION['appLocale'], 'messages') ?></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <main class="content">
                    <div class="card">
                        <div class="row g-0">
                            <div class="col-12 col-lg-5 col-xl-3 border-right">
                                <?=$this->render('messageUserlist', ['searchTranslation' => Translation::make($_SESSION['appLocale'], 'search')], 'professor')?>
                                <hr class="d-block d-lg-none mt-1 mb-0">
                            </div>
                            <div class="col-12 col-lg-7 col-xl-9">
                                <div class="py-2 px-4 border-bottom d-none d-lg-block">
                                    <div class="d-flex align-items-center py-1">
                                        <div class="position-relative">
                                        </div>
                                        <div class="flex-grow-1 pl-3">
                                            <div id="dmUser">
                                                <strong id="username"></strong>
                                                <strong id="userSurname"></strong>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="position-relative">
                                    <div class="chat-messages p-4">
                                        <div class="chat-message-right pb-4">
                                            <div class="flex-shrink-1 bg-light rounded py-2 px-3 mr-3">
                                                <div class="messageWrapper"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="flex-grow-0 py-3 px-4 border-top">
                                    <div class="input-group">
                                                                                <input type="text" class="form-control message" id="message" name="message" 
                                        placeholder="<?=Translation::make($_SESSION['appLocale'], 'typeYourMessageHere') ?>">
                                        <button class="btn btn-primary" id="sendMessage"><?=Translation::make($_SESSION['appLocale'], 'send') ?></button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </main>
            </div>
        </div>
    </div>
</div>