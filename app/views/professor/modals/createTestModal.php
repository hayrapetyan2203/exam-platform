<style>
    .question {
        border: 1px dotted red;
        padding: 3%;
    }
</style>

<div class="modal fade" id="createTestModal" tabindex="-1" role="dialog" aria-labelledby="createTestModalLabel"
    aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <!-- Step 1: Choose Group and Articles -->
            <div class="modal-body step1">
                <h5 class="modal-title" id="createTestModalLabel">
                    <?= Translation::make($_SESSION['appLocale'], 'createTest') ?>
                </h5>
                <form>
                    <!-- Group Selection -->
                    <div class="form-group">
                        <label for="chooseGroop"><?= Translation::make($_SESSION['appLocale'], 'chooseGroop') ?></label>
                        <select class="form-control" id="chooseGroop" name="chooseGroop">
                            <?php foreach ($groups as $key => $group): ?>
                                <option value="<?= $group['id'] ?>"><?= $group['name'] ?></option>
                            <?php endforeach; ?>
                        </select>
                    </div>
                    <!-- Article Selection -->
                    <div class="form-group">
                        <div class="form-group">
                            <?= $this->render('select', [
                                'id' => 'articles',
                                'class' => 'form-control',
                                'showLabel' => true,
                                'label' => Translation::make($_SESSION['appLocale'], 'chooseArticle'),
                                'options' => $articles,
                                'optionId' => 'id',
                                'optionName' => 'name',
                            ], 'components') ?>
                        </div>
                    </div>

                    <!-- Test Details -->
                    <div class="form-group">
                        <label for="maxPoints"><?= Translation::make($_SESSION['appLocale'], 'maxPoints') ?></label>
                        <input type="number" class="form-control" id="maxPoints" name="maxPoints">
                    </div>
                    <div class="form-group">
                        <label for="maxQuestions"><?= Translation::make($_SESSION['appLocale'], 'maxQuestions') ?></label>
                        <input type="number" class="form-control" id="maxQuestions" name="maxQuestions" min="1" max="100" step="1" value="10">
                    </div>

                    <div class="form-group">
                        <label for="testName"><?= Translation::make($_SESSION['appLocale'], 'testName') ?></label>
                        <input type="text" class="form-control" id="testName"
                            placeholder="<?= Translation::make($_SESSION['appLocale'], 'testName') ?>">
                    </div>

                    <div class="form-group">
                        <label for="testName"><?= Translation::make($_SESSION['appLocale'], 'testDifficultyLevels') ?></label>
                        <label for="easyQuestionPoints" class="d-block">
                            <?= Translation::make($_SESSION['appLocale'], 'easyQuestion') ?>
                        </label>
                        <input type="number" class="form-control mb-1" id="easyQuestionPoints" value="1"
                               placeholder="<?= Translation::make($_SESSION['appLocale'], 'easyQuestion') ?>">

                        <label for="midQuestionPoints">
                            <?= Translation::make($_SESSION['appLocale'], 'midQuestion') ?>
                        </label>
                        <input type="number" class="form-control mb-1" id="midQuestionPoints" value="1"
                               placeholder="<?= Translation::make($_SESSION['appLocale'], 'midQuestion') ?>">

                        <label for="hardQuestionPoints">
                            <?= Translation::make($_SESSION['appLocale'], 'hardQuestion') ?>
                        </label>
                        <input type="number" class="form-control mb-1" id="hardQuestionPoints" value="1"
                               placeholder="<?= Translation::make($_SESSION['appLocale'], 'hardQuestion') ?>">

                    </div>
                </form>
            </div>

            <!-- Step 2: Enter Test Details and Questions -->
            <div class="modal-body step2" style="display: none;">
                <form id="testCreationForm">
                    <div class="question-section" style="max-height: 500px; overflow-y: auto;">
                        <!-- Question Template -->
                        <div class="form-group question">
                            <!-- Question Type Toggler -->
                            <label for="questionType"><?= Translation::make($_SESSION['appLocale'], 'questionType') ?></label>
                            <select class="form-control question-type" name="questionType">
                                <option value="multipleChoice"><?= Translation::make($_SESSION['appLocale'], 'multipleChoice') ?></option>
                                <option value="textQuestion"><?= Translation::make($_SESSION['appLocale'], 'textQuestion') ?></option>
                            </select>

                            <!-- Question Type Toggler -->
                            <label class="mt-2" for="questionType"><?= Translation::make($_SESSION['appLocale'], 'testDifficultyLevel') ?></label>
                            <select class="form-control question-difficulty-level" name="testDifficultyLevel" id="testDifficultyLevel">
                                <option value="easyQuestion"><?= Translation::make($_SESSION['appLocale'], 'easyQuestion') ?></option>
                                <option value="midQuestion"><?= Translation::make($_SESSION['appLocale'], 'midQuestion') ?></option>
                                <option value="hardQuestion"><?= Translation::make($_SESSION['appLocale'], 'hardQuestion') ?></option>
                            </select>

                            <!-- Question Text -->
                            <label for="questionText"><?= Translation::make($_SESSION['appLocale'], 'questionText') ?></label>
                            <input type="text" class="form-control question-text" placeholder="<?= Translation::make($_SESSION['appLocale'], 'enterQuestionText') ?>">

                            <!-- Multiple Choice Section -->
                            <div class="multiple-choice-section">
                                <label><?= Translation::make($_SESSION['appLocale'], 'answers') ?></label>
                                <div class="questionAnswers">
                                    <div class="answer-wrapper d-flex align-items-center">
                                        <input type="text" class="form-control answer me-2" placeholder="<?= Translation::make($_SESSION['appLocale'], 'answer') ?> 1">
                                        <button type="button" class="btn btn-sm btn-danger remove-answer">X</button>
                                    </div>
                                </div>
                                <button type="button" class="btn btn-info btn-sm add-answer mt-1"><?= Translation::make($_SESSION['appLocale'], 'addAnswer') ?></button>

                                <label><?= Translation::make($_SESSION['appLocale'], 'correctAnswer') ?> (1-3)</label>
                                <input type="text" class="form-control correct-answer" placeholder="<?= Translation::make($_SESSION['appLocale'], 'correctAnswersPlaceholder') ?>">
                            </div>

                            <!-- Text Question Section -->
                            <div class="text-question-section" style="display: none;">
                                <label for="textAnswer"><?= Translation::make($_SESSION['appLocale'], 'enterFreeTextAnswer') ?></label>
                                <input type="text" class="form-control correct-answer" >
                            </div>

                            <button type="button" class="btn btn-sm btn-danger remove-question mt-2"><?= Translation::make($_SESSION['appLocale'], 'removeQuestion') ?></button>
                            <hr>
                        </div>
                    </div>

                    <button type="button" class="btn btn-primary btn-sm add-question mt-5"><?= Translation::make($_SESSION['appLocale'], 'addQuestion') ?></button>
                </form>
            </div>

            <!-- Step Navigation -->
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" id="prevStep">
                    <?= Translation::make($_SESSION['appLocale'], 'back') ?>
                </button>
                <button type="button" class="btn btn-primary" id="nextStep">
                    <?= Translation::make($_SESSION['appLocale'], 'next') ?>
                </button>
            </div>
        </div>
    </div>
</div>