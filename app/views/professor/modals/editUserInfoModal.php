<div class="modal fade" id="editUserinfoModal" tabindex="-1" role="dialog" aria-labelledby="createGroupModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="createGroupModalLabel"><?=Translation::make($_SESSION['appLocale'], 'editUserinfo') ?></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form id="editForm" enctype="multipart/form-data">
                    <input type="hidden" id="edit-id" value="<?=$_SESSION['user']['id']?>">

                    <div class="form-group">
                        <label for="edit-image"><?=Translation::make($_SESSION['appLocale'], 'image') ?></label>
                        <input required type="file" class="form-control" data-path="<?=$_SESSION['user']['imagePath'] ?>" id="edit-image" name="image">
                    </div>

                    <div class="form-group">
                        <label for="edit-username"><?=Translation::make($_SESSION['appLocale'], 'userName') ?></label>
                        <input required type="text" class="form-control" id="edit-username" value="<?=$_SESSION['user']['username']?>" name="username">
                    </div>
                    <div class="form-group">
                        <label for="edit-userSurname"><?=Translation::make($_SESSION['appLocale'], 'userSurname') ?></label>
                        <input required type="text" class="form-control" id="edit-userSurname" name="userSurname" value="<?=$_SESSION['user']['userSurname']?>">
                    </div>
                    <div class="form-group">
                        <label for="edit-userFathername"><?=Translation::make($_SESSION['appLocale'], 'userFathername') ?></label>
                        <input required type="text" class="form-control" id="edit-userFathername" name="userFathername" value="<?=$_SESSION['user']['userFathername']?>">
                    </div>
                    <div class="form-group">
                        <label for="edit-useremail"><?=Translation::make($_SESSION['appLocale'], 'emailAddress') ?></label>
                        <input required readonly type="text" class="form-control" id="edit-useremail" name="useremail" value="<?=$_SESSION['user']['useremail']?>">
                    </div>
                    <div class="form-group">
                        <label for="edit-phone"><?=Translation::make($_SESSION['appLocale'], 'phone') ?></label>
                        <input required readonly type="text" class="form-control" id="edit-phone" name="phone" value="<?=$_SESSION['user']['phone']?>">
                    </div>
                    <div class="form-group">
                        <label for="edit-address"><?=Translation::make($_SESSION['appLocale'], 'address') ?></label>
                        <input required type="text" class="form-control" id="edit-address" name="address" value="<?=$_SESSION['user']['address']?>">
                    </div>
                    <button type="button" id="submitEditUserInfo" name="submitGroupCreate" class="btn btn-primary"><?=Translation::make($_SESSION['appLocale'], 'save') ?></button>
                </form>
            </div>
        </div>
    </div>
</div>

<script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
<script>
    $(document).ready(function() {


        function updateCurrentImagePreview(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('#current-image').attr('src', e.target.result);
                }

                reader.readAsDataURL(input.files[0]); // Convert to base64 string

                // Update data-path attribute with the new image path
                var newPath = URL.createObjectURL(input.files[0]);
                $('#edit-image').attr('data-path', newPath);
            }
        }

        // Trigger current image preview and data-path update on file input change
        $('#edit-image').change(function() {
            updateCurrentImagePreview(this);
        });

        $('#submitEditUserInfo').click(function () {
            // Create FormData object
            let formData = new FormData();
            formData.append('id', $('#edit-id').val());
            formData.append('username', $('#edit-username').val());
            formData.append('userSurname', $('#edit-userSurname').val());
            formData.append('userFathername', $('#edit-userFathername').val());
            formData.append('useremail', $('#edit-useremail').val());
            formData.append('phone', $('#edit-phone').val());
            formData.append('address', $('#edit-address').val());
            formData.append('imagePath', $('#edit-image').data('path'));

            // Add image file to FormData
            let imageFile = $('#edit-image')[0].files[0];
            formData.append('image', imageFile);

            // Send AJAX request
            $.ajax({
                url: "?controller=user&action=edit",
                type: "POST",
                data: formData,
                processData: false,
                contentType: false,
                success: function(data) {
                    let response = JSON.parse(data);
                    if (response.status === 200) {
                        console.log('Success');
                        location.reload();
                    } else {
                        console.log('Error: ', response.message);
                    }
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    console.log('Error:', textStatus, errorThrown);
                }
            });
        });
    });
</script>
