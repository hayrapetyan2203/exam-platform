<header id="header" class="header fixed-top">
    <div class="container-fluid container-xl d-flex align-items-center justify-content-end">

        <nav id="navbar" class="navbar">
            <ul>
                <li class="dropdown d-flex">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <?=Translation::make($_SESSION['appLocale'], 'language') ?>
                    </a>
                    <ul class="dropdown-menu">
                        <li><a href="?locale=hy">Armenian</a></li>
                        <li><a href="?locale=en">English</a></li>
                        <li><a href="?locale=ru">Russian</a></li>
                    </ul>
                </li>
            </ul>
            <a href="index.php?locale=<?=$_SESSION['appLocale']?>">
                <!-- SVG -->
                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" fill="currentColor" class="bi bi-house-fill ml-2" viewBox="0 0 16 16">
                    <path d="M8.707 1.5a1 1 0 0 0-1.414 0L.646 8.146a.5.5 0 0 0 .708.708L8 2.207l6.646 6.647a.5.5 0 0 0 .708-.708L13 5.793V2.5a.5.5 0 0 0-.5-.5h-1a.5.5 0 0 0-.5.5v1.293z"/>
                    <path d="m8 3.293 6 6V13.5a1.5 1.5 0 0 1-1.5 1.5h-9A1.5 1.5 0 0 1 2 13.5V9.293z"/>
                </svg>
                <!-- End SVG -->
            </a>

            <i class="bi bi-list mobile-nav-toggle"></i>
        </nav><!-- .navbar -->

    </div>
</header>
