<?php
$groups     = $groups   ?? UserModel::getProfessorGroups($_SESSION['user']['id'], 3);
$articles   = $articles ?? backend\models\ArticleModel::get();
$tests      = $tests    ?? backend\models\TestModel::get(3);
$is_windows_os = $is_windows_os ?? strtoupper(substr(PHP_OS, 0, 3)) === 'WIN';
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <title><?=$_SESSION['user']['username'] . ' ' .  $_SESSION['user']['userSurname']?></title>
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- project assets -->
    <?=$this->import('professor_profile', 'css'); ?>
    <?=$this->import('chat', 'css'); ?>

    <!-- cdns -->
        <!-- bootstrap css -->
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@4.4.1/dist/css/bootstrap.min.css" rel="stylesheet">
        <!-- js chart -->        
        <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.5.0/Chart.min.js"></script>

        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.13/css/select2.min.css">
        <script src="https://code.jquery.com/jquery-1.10.2.min.js"></script>
</head>

<body>
<div class="container">
    <div class="main-body">
        <!-- header -->
        <?=$this->render('header', [], 'layout'); ?>

        <!-- modals -->
        <?= $this->render('/modals/createTestModal', ['articles' => $articles, 'groups' => $groups], 'professor');?>
        <?= $this->render('/modals/messagesModal',   [], 'professor');?>
        <?= $this->render('/modals/createExamModal', [
            'articles'  => $articles, 
            'groups'    => $groups,
            'tests'     => $tests,
        ],  'professor');?>
        
        <?= $this->render('/modals/editUserInfoModal', [], 'professor');?>
        <?= $this->render('/modals/createGroupModal',  [], 'professor');?>


        <div class="row">
            <div class="col-md-4">
                <?=$this->render('userInfo2', ['is_windows_os' => $is_windows_os], 'professor') ?>
                <?=$this->render('sidebar',   [], 'professor'); ?>
                <?=$this->render('exams',     ['exams' => backend\models\ExamModel::get()], 'professor'); ?>
            </div>
            <div class="col-md-8">

                <?=$this->render('userinfo', [], 'professor') ?>

                <div class="row examsNtests">
                    <?=$this->render('groups', ['groups' => $groups], 'professor'); ?>
                    <?=$this->render('tests',  ['tests'  => $tests],  'professor'); ?>
                </div>  

                <div class="annualExamCount mt-2">
                    <?=$this->render('annualExamCount', [
                        'title'     => Translation::make($_SESSION['appLocale'], 'yearTestSummary'),
                        'january'   => Translation::make($_SESSION['appLocale'], 'january'),
                        'february'  => Translation::make($_SESSION['appLocale'], 'february'),
                        'march'     => Translation::make($_SESSION['appLocale'], 'march'),
                        'april'     => Translation::make($_SESSION['appLocale'], 'april'),
                        'may'       => Translation::make($_SESSION['appLocale'], 'may'),
                        'june'      => Translation::make($_SESSION['appLocale'], 'june'),
                        'july'      => Translation::make($_SESSION['appLocale'], 'july'),
                        'august'    => Translation::make($_SESSION['appLocale'], 'august'),
                        'september' => Translation::make($_SESSION['appLocale'], 'september'),
                        'october'   => Translation::make($_SESSION['appLocale'], 'october'),
                        'november'  => Translation::make($_SESSION['appLocale'], 'november'),
                        'december'  => Translation::make($_SESSION['appLocale'], 'december'),
                        'yValues'   => backend\models\StatisticsModel::annual(0, (int)$_SESSION['user']['id']),
                    ], 'professor'); ?>
                </div>
            </div>
        </div>
    </div>
</div>

<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.4.1/dist/js/bootstrap.bundle.min.js"></script>

<?=$this->import('app', 'js'); ?>
<?=$this->import('group', 'js'); ?>
<?=$this->import('chat', 'js'); ?>
<?=$this->import('test', 'js'); ?>
<?=$this->import('exam', 'js'); ?>

</body>
</html>