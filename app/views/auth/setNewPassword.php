<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-QWTKZyjpPEjISv5WaRU9OFeRpok6YctnYmDr5pNlyT2bRjXh0JMhjY6hW+ALEwIH" crossorigin="anonymous">
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-YvpcrYf0tY3lHB60NNkmXc5s9fDVZLESaAA55NDzOxhy9GkcIdslK1eN7N6jIeHz" crossorigin="anonymous"></script>

<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Nunito:300,300i,400,400i,600,600i,700,700i|Poppins:300,300i,400,400i,500,500i,600,600i,700,700i" rel="stylesheet">

<!-- Vendor CSS Files -->
<link href="../assets/vendor/bootstrap-icons/bootstrap-icons.css" rel="stylesheet">
<div class="container col-xxl-8 px-4 py-5">
    <div class="row flex-lg-row-reverse align-items-center g-5 py-5">
        <div class="col-10 col-sm-8 col-lg-6">
            <img src="https://img.freepik.com/free-vector/work-home-concept-flat-design_1308-103521.jpg?size=626&ext=jpg&ga=GA1.1.1448711260.1706745600&semt=ais" class="d-block mx-lg-auto img-fluid" alt="Bootstrap Themes" width="700" height="500" loading="lazy">
        </div>
        <div class="col-lg-6">
            <form action="?controller=auth&action=activateAfterForgotPassword&locale=<?=$_SESSION['appLocale']; ?>" method="post">
                <h1 class=" fw-bold lh-1 mb-3"><?=Translation::make($_SESSION['appLocale'], 'setNewPassword') ?></h1>
                <hr>
                <p><?=Translation::make($_SESSION['appLocale'], 'setNewPasswordParagraph') ?></p>

                <div class="form-group last">
                    <label for="password"><?=Translation::make($_SESSION['appLocale'], 'password') ?></label>
                    <div class="input-group" style="gap:5px">
                        <input type="password" class="form-control password" id="password" required name="password" aria-label="Password" aria-describedby="password-toggle">
                        <i class="bi bi-eye-fill password-icon password-toggle"></i>
                        <i class="bi bi-info-circle-fill password-requirements-icon" data-toggle="tooltip" title="<?=Translation::make($_SESSION['appLocale'], 'passwordRequirements') ?>"></i>
                    </div>
                </div>
                <input type="hidden" name="useremail" value="<?=$useremail?>">
                <hr>
                <div class="d-grid gap-2 d-md-flex justify-content-md-start">
                    <button type="button" class="btn btn-primary btn-sm px-4 me-md-2"><a href="register.php?locale=<?=$_SESSION['appLocale']; ?>" class="text-white text-decoration-none"><?=Translation::make($_SESSION['appLocale'], 'cancel') ?></a></button>
                    <button type="submit" class="btn btn-outline-secondary btn-sm px-4"><?=Translation::make($_SESSION['appLocale'], 'approve') ?></button>
                </div>
            </form>
        </div>
    </div>
</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.7.1/jquery.min.js"></script>
<script>
    $(document).ready(function() {
        $('.password-toggle').click(function () {
            // Find the password input and icon within the same parent container
            var passwordInput = $(this).siblings('.form-control.password');
            var passwordIcon = $(this);
            // Toggle password visibility
            if (passwordInput.attr('type') === 'password') {
                passwordInput.attr('type', 'text');
                passwordIcon.removeClass('bi-eye-fill').addClass('bi-eye-slash-fill');
            } else {
                passwordInput.attr('type', 'password');
                passwordIcon.removeClass('bi-eye-slash-fill').addClass('bi-eye-fill');
            }
        });
    });

    $(document).ready(function() {
        $('.password').keyup(function () {
            var password = $(this).val();
            var strength = 0;

            // Check for minimum length
            if (password.length >= 8) {
                strength++;
            }

            // Check for presence of uppercase letters
            if (password.match(/[A-Z]/)) {
                strength++;
            }

            // Check for presence of lowercase letters
            if (password.match(/[a-z]/)) {
                strength++;
            }

            // Check for presence of numbers
            if (password.match(/[0-9]/)) {
                strength++;
            }

            // Check for presence of special characters
            if (password.match(/[!@#$%^&*(),.?":{}|<>]/)) {
                strength++;
            }

            // Update input color based on strength
            if (strength >= 4) {
                $(this).css('color', 'green'); // Strong password
            } else if (strength >= 2) {
                $(this).css('color', 'orange'); // Medium password
            } else {
                $(this).css('color', 'red'); // Weak password
            }
        });
    });

    $(document).ready(function() {
        $('[data-toggle="tooltip"]').tooltip();
    });

</script>