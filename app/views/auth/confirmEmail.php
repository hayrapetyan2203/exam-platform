<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-QWTKZyjpPEjISv5WaRU9OFeRpok6YctnYmDr5pNlyT2bRjXh0JMhjY6hW+ALEwIH" crossorigin="anonymous">
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-YvpcrYf0tY3lHB60NNkmXc5s9fDVZLESaAA55NDzOxhy9GkcIdslK1eN7N6jIeHz" crossorigin="anonymous"></script>

<div class="container col-xxl-8 px-4 py-5">
    <div class="row flex-lg-row-reverse align-items-center g-5 py-5">
        <div class="col-10 col-sm-8 col-lg-6">
            <img src="https://img.freepik.com/free-vector/work-home-concept-flat-design_1308-103521.jpg?size=626&ext=jpg&ga=GA1.1.1448711260.1706745600&semt=ais" class="d-block mx-lg-auto img-fluid" alt="Bootstrap Themes" width="700" height="500" loading="lazy">
        </div>
        <div class="col-lg-6">
            <form action="FrontController.php?controller=auth&action=activate&locale=<?=$_SESSION['appLocale']; ?>" method="post">
                <h1 class=" fw-bold lh-1 mb-3"><?=Translation::make($_SESSION['appLocale'], 'emailSended') ?></h1>
                <input style="border:1px solid black;" name="activationCode" type="text" class="form-control">
                <input type="hidden" name="useremail" value="<?=$useremail?>">
                <hr>

                <div class="d-grid gap-2 d-md-flex justify-content-md-start">
                    <a href="" class="mx-5"><?=Translation::make($_SESSION['appLocale'], 'dontReceiveCode') ?></a>
                    <button type="button" class="btn btn-primary btn-sm px-4 me-md-2"><a href="register.php?locale=<?=$_SESSION['appLocale']; ?>" class="text-white text-decoration-none"><?=Translation::make($_SESSION['appLocale'], 'cancel') ?></a></button>
                    <button type="submit" class="btn btn-outline-secondary btn-sm px-4"><?=Translation::make($_SESSION['appLocale'], 'approve') ?></button>
                </div>
            </form>
        </div>
    </div>
</div>