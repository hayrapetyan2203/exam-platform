<?php
$articles   = $articles ?? backend\models\ArticleModel::get();
$tests      = $tests    ?? backend\models\TestModel::get();
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title><?=$_SESSION['user']['username'] . ' ' .  $_SESSION['user']['userSurname']?></title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@4.4.1/dist/css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="../../assets/css/professor_profile.css">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Nunito:300,300i,400,400i,600,600i,700,700i|Poppins:300,300i,400,400i,500,500i,600,600i,700,700i" rel="stylesheet">
    <?=$this->import('professor_profile', 'css'); ?>
    <?=$this->import('chat', 'css'); ?>
</head>
<body>
<div class="container">
    <div class="main-body">
        <nav aria-label="breadcrumb" class="main-breadcrumb">
            <ol class="breadcrumb">
                <li class="ml-auto dropdown">
                    <a href="#" class="dropdown-toggle" id="languageDropdown1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <?=Translation::make($_SESSION['appLocale'], 'language') ?>
                    </a>
                    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="languageDropdown1">
                        <div class="dropdown-item" onclick="changeLanguage('hy')">Armenian</div>
                        <div class="dropdown-item" onclick="changeLanguage('en')">English</div>
                        <div class="dropdown-item" onclick="changeLanguage('ru')">Russian</div>
                    </div>

                    <script>

                        function changeLanguage(locale) {
                            // Get the current URL
                            var url = new URL(window.location.href);

                            // Set or update the 'locale' parameter
                            url.searchParams.set('locale', locale);

                            // Reload the page with the updated URL
                            window.location.href = url.toString();
                        }
                    </script>

                </li>
                <li class="ml-2">
                    <a href="?controller=auth&action=logout&locale=<?=$_SESSION['appLocale']?>"><?=Translation::make($_SESSION['appLocale'], 'exit') ?></a>
                </li>
            </ol>
        </nav>

        <?= $this->render('/modals/messagesModal', [], 'student');?>
        <?= $this->render('/modals/editUserInfo', [], 'student');?>

        <div class="row gutters-sm">
            <div class="col-md-4 mb-3">
                <div class="card userinfo2student">
                    <div class="card-body">
                        <div class="d-flex flex-column align-items-center text-center">
                            <?php 
                                $imagePath = !empty($_SESSION['user']['imagePath']) ? 
                                    $_SESSION['user']['imagePath'] : 
                                        'https://bootdey.com/img/Content/avatar/avatar7.png';
                            ?>
                            <img src="<?=$imagePath ?>" alt="Admin" class="img-thumbnail userinfo-img " width="150" >
                            <div class="mt-3">
                                <h4><?=$_SESSION['user']['username'] . ' ' .  $_SESSION['user']['userSurname']?></h4>
                                <p class="text-secondary mb-1"><?= ((int)$_SESSION['user']['role'] === 1) ?
                                        Translation::make($_SESSION['appLocale'], 'professor') :
                                        Translation::make($_SESSION['appLocale'], 'student')
                                    ?></p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card mt-3">
                    <ul class="list-group list-group-flush">
                        <li class="list-group-item d-flex justify-content-between align-items-center flex-wrap">
                            <h6 class="mb-0" data-toggle="modal" data-target="#messagesModal">
                                <svg xmlns="http://www.w3.org/2000/svg" width="22" height="22" fill="currentColor" class="bi bi-chat-left-dots" viewBox="0 0 16 16">
                                    <path d="M14 1a1 1 0 0 1 1 1v8a1 1 0 0 1-1 1H4.414A2 2 0 0 0 3 11.586l-2 2V2a1 1 0 0 1 1-1zM2 0a2 2 0 0 0-2 2v12.793a.5.5 0 0 0 .854.353l2.853-2.853A1 1 0 0 1 4.414 12H14a2 2 0 0 0 2-2V2a2 2 0 0 0-2-2z"/>
                                    <path d="M5 6a1 1 0 1 1-2 0 1 1 0 0 1 2 0m4 0a1 1 0 1 1-2 0 1 1 0 0 1 2 0m4 0a1 1 0 1 1-2 0 1 1 0 0 1 2 0"/>
                                </svg>
                                <span class="text-danger" id="messagesCount"></span>
                                <span style="cursor:pointer"><?=Translation::make($_SESSION['appLocale'], 'messages')?></span>
                            </h6>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="col-md-8">
                <div class="card mb-3">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-sm-4">
                                <h6 class="mb-0"><?=Translation::make($_SESSION['appLocale'], 'fullname') ?></h6>
                            </div>
                            <div class="col-sm-8 text-secondary">
                                <?=$_SESSION['user']['username'] . ' ' .  $_SESSION['user']['userSurname'] . ' ' .  $_SESSION['user']['userFathername']?>
                            </div>
                        </div>
                        <hr>
                        <div class="row">
                            <div class="col-sm-4">
                                <h6 class="mb-0"><?=Translation::make($_SESSION['appLocale'], 'emailAddress') ?></h6>
                            </div>
                            <div class="col-sm-8 text-secondary">
                                <?=$_SESSION['user']['useremail'] ?>
                            </div>
                        </div>
                        <hr>
                        <div class="row">
                            <div class="col-sm-4">
                                <h6 class="mb-0"><?=Translation::make($_SESSION['appLocale'], 'phone') ?></h6>
                            </div>
                            <div class="col-sm-8 text-secondary">
                                <?=$_SESSION['user']['phone'] ?>
                            </div>
                        </div>
                        <hr>
                        <div class="row">
                            <div class="col-sm-4">
                                <h6 class="mb-0"><?=Translation::make($_SESSION['appLocale'], 'address') ?></h6>
                            </div>
                            <div class="col-sm-8 text-secondary">
                                <?=$_SESSION['user']['address'] ?>
                            </div>
                        </div>
                        <hr>
                        <div class="row">
                            <div class="col-sm-12">
                                <a class="btn btn-info text-white" id="editUserinfo" data-toggle="modal" data-target="#editUserinfoModal">
                                    <?=Translation::make($_SESSION['appLocale'], 'edit') ?>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row gutters-sm">
                    <?=$this->render('groups', ['groups' => UserModel::getStudentGroups($_SESSION['user']['id'])], 'student'); ?>
                    <?=$this->render('availableExams', ['exams' => backend\models\ExamModel::getStudentExams($_SESSION['user']['id'])], 'student'); ?>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="https://code.jquery.com/jquery-1.10.2.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.4.1/dist/js/bootstrap.bundle.min.js"></script>
<?=$this->import('chat', 'js'); ?>
</body>
</html>