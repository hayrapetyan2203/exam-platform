<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-QWTKZyjpPEjISv5WaRU9OFeRpok6YctnYmDr5pNlyT2bRjXh0JMhjY6hW+ALEwIH" crossorigin="anonymous">
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-YvpcrYf0tY3lHB60NNkmXc5s9fDVZLESaAA55NDzOxhy9GkcIdslK1eN7N6jIeHz" crossorigin="anonymous"></script>

<div class="container col-xxl-8 px-4 py-5">
    <div class="row flex-lg-row-reverse align-items-center g-5 py-5">
        <div class="col-10 col-sm-8 col-lg-6">
            <img src="https://cdni.iconscout.com/illustration/premium/thumb/forgot-password-4268397-3551744.png?f=webp" class="d-block mx-lg-auto img-fluid" alt="Bootstrap Themes" width="700" height="500" loading="lazy">
        </div>
        <div class="col-lg-6">
            <form action="?controller=auth&action=approve&locale=<?=$_SESSION['appLocale']; ?>" method="post">
                <h1 class=" fw-bold lh-1 mb-3"><?=Translation::make($_SESSION['appLocale'], 'emailSearch') ?></h1>
                <p><?=Translation::make($_SESSION['appLocale'], 'enterYourEmail') ?></p>
                <input style="border:1px solid black;" name="useremail" type="email" class="form-control" required>
                <hr>

                <div class="d-grid gap-2 d-md-flex justify-content-md-start">
                    <button type="button" class="btn btn-primary btn-sm px-4 me-md-2"><a href="login.php?locale=<?=$_GET['locale']?>" class="text-white text-decoration-none"><?=Translation::make($_SESSION['appLocale'], 'cancel') ?></a></button>
                    <button type="submit" class="btn btn-outline-secondary btn-sm px-4"><?=Translation::make($_SESSION['appLocale'], 'approve') ?></button>
                </div>
            </form>
        </div>
    </div>
</div>