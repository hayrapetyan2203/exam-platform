<nav aria-label="Page navigation example">
  <ul class="pagination justify-content-center">
    <li class="page-item"><a class="page-link" href="#"><?= Translation::make($_SESSION['appLocale'], 'back') ?></a></li>
    <li class="page-item"><a class="page-link" href="#">1</a></li>
    <li class="page-item"><a class="page-link" href="#">2</a></li>
    <li class="page-item"><a class="page-link" href="#">3</a></li>
    <li class="page-item"><a class="page-link" href="#"><?= Translation::make($_SESSION['appLocale'], 'next') ?></a></li>
  </ul>
</nav>