


<!-- Include Popper.js -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.1/umd/popper.min.js"></script>

<!-- Include Bootstrap JS -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>

<nav aria-label="breadcrumb" class="main-breadcrumb">
    <ol class="breadcrumb">
        <li class="ml-2">
            <a href="?controller=auth&action=login&locale=<?=$_SESSION['appLocale']?>"><?=Translation::make($_SESSION['appLocale'], 'back') ?></a>
        </li>
        <li class="ml-auto dropdown">
            <a href="#" class="dropdown-toggle" id="languageDropdown" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <?=Translation::make($_SESSION['appLocale'], 'language') ?>
            </a>
            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="languageDropdown">
                <div class="dropdown-item" onclick="changeLanguage('hy')">Armenian</div>
                <div class="dropdown-item" onclick="changeLanguage('en')">English</div>
                <div class="dropdown-item" onclick="changeLanguage('ru')">Russian</div>
            </div>

            <script>
                function changeLanguage(locale) {
                    // Get the current URL
                    var url = new URL(window.location.href);

                    // Set or update the 'locale' parameter
                    url.searchParams.set('locale', locale);

                    // Reload the page with the updated URL
                    window.location.href = url.toString();
                }
            </script>

        </li>
        <li class="ml-2">
            <a href="?controller=auth&action=logout&locale=<?=$_SESSION['appLocale']?>"><?=Translation::make($_SESSION['appLocale'], 'exit') ?></a>
        </li>
    </ol>
</nav>
