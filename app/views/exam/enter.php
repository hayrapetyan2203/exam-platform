<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Online Exam</title>
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.7.1/jquery.min.js"
    integrity="sha512-v2CJ7UaYy4JwqLDIrZUI/4hqeoQieOmAZNXBeQyjo21dadnwR+8ZaIJVT8EE2iyI61OV8e6M8PP2/4hpQINQ/g=="
    crossorigin="anonymous" referrerpolicy="no-referrer"></script>
    <?=$this->import('exam', 'css')?>
</head>

<body>
  <?php
  $tests = explode(',', json_decode($exam['tests']));
  $testModels = [];
  foreach ($tests as $test) {
    $testModels[] = \backend\models\TestModel::findOne($test);
  }

  $examTest = $testModels[array_rand($testModels)];
  $questions = json_decode($examTest['questions'], true) ?? [];
  ?>
  <!-- Header -->
  <div class="mt-3">
    <?= $this->render('header', [], 'layout'); ?>
  </div>

  <!-- Main content -->
  <div class="container mt-5">
    <div class="row">
      <!-- Left column - Exam Information -->
      <div class="col-lg-5">
        <div class="card exam-info-card">
          <img class="img-fluid"
            src="https://img.freepik.com/premium-vector/job-exam-test-vector-illustration_138676-243.jpg" alt="">
          <div class="card-body">
            <h5 class="card-title"><?= Translation::make($_SESSION['appLocale'], 'exam') ?></h5>
            <p class="card-text">
              <strong><?= Translation::make($_SESSION['appLocale'], 'examName') ?>:</strong> <?= $exam['name'] ?>
            </p>
            <p class="card-text">
              <strong><?= Translation::make($_SESSION['appLocale'], 'duration') ?>:</strong> <?= $exam['duration'] ?>
            </p>
            <?php
            $examStarted = \backend\models\ExamModel::findOneStarting($exam['id']);
            $startTime = strtotime($examStarted['started_at']);
            $duration = $exam['duration'];

            // Extracting the duration value
            preg_match('/\d+/', $duration, $matches);
            $durationValue = (int) $matches[0];

            // Determine if the duration is in hours or minutes
            $isHours = $durationValue < 30;

            // Convert the duration to seconds
            $durationInSeconds = $isHours ? $durationValue * 3600 : $durationValue * 60;

            // Calculate remaining time
            $currentTime = time();
            $remainingTimeInSeconds = $startTime + $durationInSeconds - $currentTime;

            // Format the remaining time
            $remainingHours = floor($remainingTimeInSeconds / 3600);
            $remainingMinutes = floor(($remainingTimeInSeconds % 3600) / 60);
            $remainingSeconds = $remainingTimeInSeconds % 60;

            // Display the remaining time
            ?>
            <p class="card-text">
              <strong><?= Translation::make($_SESSION['appLocale'], 'startedAt') ?>:</strong>
              <?= $examStarted['started_at'] ?>
            </p>
            <p class="card-text remaining-time">
              <strong><?= Translation::make($_SESSION['appLocale'], 'remainingTime') ?>:</strong>
              <?= $remainingHours ?> hours <?= $remainingMinutes ?> minutes <?= $remainingSeconds ?> seconds
            </p>
            <p class="card-text"><strong><?= Translation::make($_SESSION['appLocale'], 'professor') ?>:</strong>
              <?= UserModel::findOne($exam['professor_id'])['username'] . ' ' . UserModel::findOne($exam['professor_id'])['userSurname'] ?>
            </p>
            <button class="btn btn-primary mt-3 btn-block btn-finish-exam"
              id="examButton"><?= Translation::make($_SESSION['appLocale'], 'endExam') ?></button>
          </div>
          
          <script>

            const EXAM_TYPE_EQUAL_POINTS = 1;
            const EXAM_TYPE_DIFFERENT_POINTS = 2;
            const examType = "<?= $exam['type'] ?>";

            // end the exam
            $('#examButton').click(function () {
              changeButtonText();
              endExamForStudent();
            })

            function getQuestionsAndAnswers() {
              let questionsAndAnswers = [];
              let correctAnswers = 0;
              let incorrectAnswers = 0;

              $('.question-container').each(function () {

                $(this).css('pointer-events', 'none');

                let questionText = $(this).find('.question-text').text();
                let questionType = $(this).find('.question-type').text();
                console.log(questionType)
                let selectedAnswer = $(this).find('input[type="radio"]:checked').next('label').text();

                let selectedAnswerIndices = [];
                $(this).find('input[type="checkbox"]:checked').each(function() {
                    let index = $(this).next('label').data('index');
                    selectedAnswerIndices.push(index);
                });

                let correctAnswerIndex = $(this).find('.question-correct-answer').text();
                let correctAnswerIndices = correctAnswerIndex.split(','); // Convert the string to an array

                if(questionType === 'multipleChoice'){
                    //code for questions with checkboxes
                    if (selectedAnswerIndices.includes(Number(correctAnswerIndices))) {
                      correctAnswers++;
                      correctAnswerIndices.forEach(index => {
                        $(this).find(`label[data-index='${index.trim()}']`).css('color', 'green'); // Highlight all correct answers
                      });
                    } else {
                      incorrectAnswers++;
                      correctAnswerIndices.forEach(index => {
                        $(this).find(`label[data-index='${index.trim()}']`).css('color', 'green'); // Highlight all correct answers
                      });
                      $(this).find(`label[data-index='${selectedAnswerIndices}']`).css('color', 'red'); // Highlight the incorrect selection
                    }
                } else{
                    let selectedAnswerText = $(this).find('.form-check-input').val();
                    if (String(correctAnswerIndex) === String(selectedAnswerText)) {
                      correctAnswers++;
                      correctAnswerIndices.forEach(index => {
                        $(this).find(`label[data-index='${index.trim()}']`).css('color', 'green'); // Highlight all correct answers
                      });
                    } else {
                      incorrectAnswers++;
                      correctAnswerIndices.forEach(index => {
                        $(this).find(`label[data-index='${index.trim()}']`).css('color', 'green'); // Highlight all correct answers
                      });
                      $(this).find(`label[data-index='${selectedAnswerIndices}']`).css('color', 'red'); // Highlight the incorrect selection
                    }
                }

                questionsAndAnswers.push({
                  questionText: questionText,
                  selectedAnswer: selectedAnswer,
                  selectedAnswerIndices: selectedAnswerIndices,
                  correctAnswerIndex: correctAnswerIndex,
                });
              });
              let totalQuestionsCount = correctAnswers + incorrectAnswers;

              let correctPercentage = (correctAnswers / totalQuestionsCount) * 100;
              let incorrectPercentage = 100 - correctPercentage;
              let resultCategory =
                correctPercentage < 40 ? 'bad' :
                  correctPercentage < 80 ? 'medium' : 'good';

              return {
                questionsAndAnswers: questionsAndAnswers,
                results: {
                  correctAnswers: correctAnswers,
                  incorrectAnswers: incorrectAnswers,
                  quastionCount: totalQuestionsCount,
                  resultCategory: resultCategory
                }
              };
            }

            function getQuestionsAndAnswers2() {
              let questionsAndAnswers = [];
              let correctAnswers = 0;
              let incorrectAnswers = 0;

              $('.question-container').each(function () {

                $(this).css('pointer-events', 'none');

                let questionText = $(this).find('.question-text').text();
                let questionType = $(this).find('.question-type').text();
                console.log(questionType)
                let selectedAnswer = $(this).find('input[type="radio"]:checked').next('label').text();

                let selectedAnswerIndices = [];
                $(this).find('input[type="checkbox"]:checked').each(function() {
                    let index = $(this).next('label').data('index');
                    selectedAnswerIndices.push(index);
                });

                let correctAnswerIndex = $(this).find('.question-correct-answer').text();
                let correctAnswerIndices = correctAnswerIndex.split(','); // Convert the string to an array

                if(questionType === 'multipleChoice'){
                    //code for questions with checkboxes
                    if (selectedAnswerIndices.includes(Number(correctAnswerIndices))) {
                      correctAnswers++;
                      correctAnswerIndices.forEach(index => {
                        $(this).find(`label[data-index='${index.trim()}']`).css('color', 'green'); // Highlight all correct answers
                      });
                    } else {
                      incorrectAnswers++;
                      correctAnswerIndices.forEach(index => {
                        $(this).find(`label[data-index='${index.trim()}']`).css('color', 'green'); // Highlight all correct answers
                      });
                      $(this).find(`label[data-index='${selectedAnswerIndices}']`).css('color', 'red'); // Highlight the incorrect selection
                    }
                } else{
                    let selectedAnswerText = $(this).find('.form-check-input').val();
                    if (String(correctAnswerIndex) === String(selectedAnswerText)) {
                      correctAnswers++;
                      correctAnswerIndices.forEach(index => {
                        $(this).find(`label[data-index='${index.trim()}']`).css('color', 'green'); // Highlight all correct answers
                      });
                    } else {
                      incorrectAnswers++;
                      correctAnswerIndices.forEach(index => {
                        $(this).find(`label[data-index='${index.trim()}']`).css('color', 'green'); // Highlight all correct answers
                      });
                      $(this).find(`label[data-index='${selectedAnswerIndices}']`).css('color', 'red'); // Highlight the incorrect selection
                    }
                }

                questionsAndAnswers.push({
                  questionText: questionText,
                  selectedAnswer: selectedAnswer,
                  selectedAnswerIndices: selectedAnswerIndices,
                  correctAnswerIndex: correctAnswerIndex,
                });
              });
              let totalQuestionsCount = correctAnswers + incorrectAnswers;

              let correctPercentage = (correctAnswers / totalQuestionsCount) * 100;
              let incorrectPercentage = 100 - correctPercentage;
              let resultCategory =
                correctPercentage < 40 ? 'bad' :
                  correctPercentage < 80 ? 'medium' : 'good';

              return {
                questionsAndAnswers: questionsAndAnswers,
                results: {
                  correctAnswers: correctAnswers,
                  incorrectAnswers: incorrectAnswers,
                  quastionCount: totalQuestionsCount,
                  resultCategory: resultCategory
                }
              };
            }

            function endExamForStudent() {
              let examSummary = examType == EXAM_TYPE_EQUAL_POINTS ? getQuestionsAndAnswers() :  getQuestionsAndAnswers2();
              showExamSummaryChart(examSummary);
              saveExamResults(examSummary);
            }

            function saveExamResults(examSummary) {
              const urlParams = new URLSearchParams(window.location.search);
              const payload = {
                result: examSummary,
                examId: Number(urlParams.get('exam_id'))
              }
              $.post("?controller=exam&action=saveExamResults", payload);
            }

            function showExamSummaryChart(examSummary) {
              showExamResults(examSummary.results.correctAnswers, examSummary.results.quastionCount);
              $('#examResultsModal').modal('show'); // Show modal using jQuery
            }

            function showExamResults(correctAnswersCount, totalQuestionsCount) {
              let bad = "<?= Translation::make($_SESSION['appLocale'], 'bad'); ?>";
              let medium = "<?= Translation::make($_SESSION['appLocale'], 'medium'); ?>";
              let good = "<?= Translation::make($_SESSION['appLocale'], 'good'); ?>";
              let correctAnswers = "<?= Translation::make($_SESSION['appLocale'], 'correctAnswers'); ?>";
              let incorrectAnswers = "<?= Translation::make($_SESSION['appLocale'], 'incorrectAnswers'); ?>";
              let examResult = "<?= Translation::make($_SESSION['appLocale'], 'examResult'); ?>";

              let correctAnswersColor = '#a6d96a';
              let incorrectAnswersColor = '#d7191c';

              let canvas = document.getElementById('examResultsChart').getContext('2d');
              let correctPercentage = (correctAnswersCount / totalQuestionsCount) * 100;
              let incorrectPercentage = 100 - correctPercentage;
              let resultCategory =
                correctPercentage < 40 ? bad :
                  correctPercentage < 80 ? medium : good;

              if (window.examResultsChart.data) {
                window.examResultsChart.data.datasets[0].data = [correctPercentage, incorrectPercentage];
                window.examResultsChart.update();
              } else {
                window.examResultsChart = new Chart(canvas, {
                  type: 'pie',
                  data: {
                    labels: [correctAnswers, incorrectAnswers],
                    datasets: [{
                      label: examResult,
                      data: [correctPercentage, incorrectPercentage],
                      backgroundColor: [correctAnswersColor, incorrectAnswersColor],
                      borderWidth: 1
                    }]
                  },
                  options: {
                    responsive: true,
                    maintainAspectRatio: false,
                  }
                });
              }

              $('#resultCategory').text(correctPercentage.toFixed(2));
            }

            function changeButtonText() {
              var examFinished = <?= json_encode(Translation::make($_SESSION['appLocale'], 'examFinished')) ?>;
              $('#examButton').text(examFinished);
              $('#examButton').addClass('disabled');
              $('#examButton').css('pointer-events', 'none');
            }

            // Function to update the remaining time
            function updateRemainingTime() {
              var startTime = <?= json_encode($startTime) ?>; // PHP variable passed to JavaScript
              var durationInSeconds = <?= json_encode($durationInSeconds) ?>; // PHP variable passed to JavaScript
              var examFinished = <?= json_encode(Translation::make($_SESSION['appLocale'], 'examFinished')) ?>;

              var hour = <?= json_encode(Translation::make($_SESSION['appLocale'], 'hour')) ?>;
              var minute = <?= json_encode(Translation::make($_SESSION['appLocale'], 'minute')) ?>;
              var second = <?= json_encode(Translation::make($_SESSION['appLocale'], 'second')) ?>;

              var currentTime = Math.floor(Date.now() / 1000); // Current time in seconds
              var remainingTimeInSeconds = startTime + durationInSeconds - currentTime;

              if (remainingTimeInSeconds < 0) {
                remainingTimeInSeconds = 0; // Set remaining time to 0 if it's negative
                $('#examButton').text(examFinished);
              }

              if(remainingTimeInSeconds < 61){
                  $('.remaining-time').addClass('alert alert-danger');
              }

              var remainingHours = Math.floor(remainingTimeInSeconds / 3600);
              var remainingMinutes = Math.floor((remainingTimeInSeconds % 3600) / 60);
              var remainingSeconds = remainingTimeInSeconds % 60;

              // Format the remaining time with spaces for readability
              var formattedRemainingTime = pad(remainingHours, 2) + ' ' + hour + ' ' + pad(remainingMinutes, 2) + ' ' + minute + ' ' + pad(remainingSeconds, 2) + ' ' + second;



              // Update the HTML with the new remaining time
              $('.remaining-time').html('<strong><?= Translation::make($_SESSION['appLocale'], 'remainingTime') ?>:</strong> ' + formattedRemainingTime);
            }

            // Function to pad a number with leading zeros
            function pad(num, size) {
              var s = num + "";
              while (s.length < size) s = "0" + s;
              return s;
            }

            // Update the remaining time every second
            setInterval(updateRemainingTime, 1000);

            // Call the function initially to set the correct remaining time
            updateRemainingTime();
          </script>
        </div>
      </div>


      <!-- Right column - Questions -->
      <?php if ($examStarted['is_finished']): ?>
        <h1 class="m-auto"><?= Translation::make($_SESSION['appLocale'], 'examFinished') ?></h1>
      <?php else: ?>
        <div class="col-lg-7">
          <h2 class="mb-4"><?= Translation::make($_SESSION['appLocale'], 'test') . ' ' . $examTest['id'] ?></h2>
          <?php foreach ($questions as $key => $question): ?>
            <div class="question-container" data-id="<?= $key ?>">
              <p class="question-number"><?= Translation::make($_SESSION['appLocale'], 'questionText') ?>     <?= ++$key ?>:</p>
              <p class="question-text"><?= $question['questionText']; ?></p>
              <pre>
              </pre>
              <p class="question-correct-answer d-none"><?= $question['correctAnswerIndex']; ?></p>
              <p class="question-type d-none"><?= $question['questionType']; ?></p>
              <?php if($question['questionType'] && $question['questionType'] == 'multipleChoice'): ?>
                <?php foreach ($question['answers'] as $index => $answer): ?>
                  <div class="form-check">
                    <input class="form-check-input" type="checkbox" name="question<?= $key ?>">
                    <label class="form-check-label options-label" data-index="<?= $index ?>"><?= $answer ?></label>
                  </div>
                <?php endforeach; ?>
              <?php else : ?>
                <div class="form-check">
                  <input class="form-check-input" type="text" name="question<?= $key ?>">
                  <label class="form-check-label options-label" data-index="<?= $index ?>"> </label>
                </div>
              <?php endif; ?>
            </div>
          <?php endforeach; ?>

          <!-- Scroll to top button -->
          <div class="text-center mb-2">
            <svg xmlns="http://www.w3.org/2000/svg" width="32" height="32" fill="currentColor"
              class="bi bi-arrow-up-square-fill" viewBox="0 0 16 16" style="cursor: pointer;" onclick="scrollToTop()">
              <path
                d="M2 16a2 2 0 0 1-2-2V2a2 2 0 0 1 2-2h12a2 2 0 0 1 2 2v12a2 2 0 0 1-2 2zm6.5-4.5V5.707l2.146 2.147a.5.5 0 0 0 .708-.708l-3-3a.5.5 0 0 0-.708 0l-3 3a.5.5 0 1 0 .708.708L7.5 5.707V11.5a.5.5 0 0 0 1 0" />
            </svg>
          </div>
        </div>
      <?php endif; ?>
      <script>
        function scrollToTop() {
          window.scrollTo({
            top: 0,
            behavior: 'smooth' // Optional, smooth scrolling animation
          });
        }
      </script>
    </div>
  </div>
  </div>

  <?= $this->render('charts/examSummary', [], 'student'); ?>
</body>

</html>