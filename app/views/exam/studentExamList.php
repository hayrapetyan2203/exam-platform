<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/css/bootstrap.min.css">
<link href="https://cdn.jsdelivr.net/npm/bootstrap@4.4.1/dist/css/bootstrap.min.css" rel="stylesheet">

<script src="https://code.jquery.com/jquery-1.10.2.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.4.1/dist/js/bootstrap.bundle.min.js"></script>
<?=$this->import('professor_profile', 'css'); ?>

<div class="container">

    <?=$this->render('header', [], 'layout'); ?>
    
    <div class="row mb-2 m-5">
        <?php foreach ($exams as $exam) : ?>
        <?php 
            /*checking if exam is finished by subtracting current timestamp and duration
            * if the result is greater that started at timestamp then the exam is finished
            * $duration int hours
            */
            $currentTimestamp = time();
            $examStarted      = \backend\models\ExamModel::findOneStarted($exam['id']);

            if($examStarted)
            {
                $startedTimestamp = strtotime($examStarted['started_at']);
                $duration         = $examStarted['duration']; 

                if ($startedTimestamp) {
                    // Convert duration to seconds
                    if ($duration <= 24) {
                        // If duration is less than or equal to 24, it's in hours
                        $durationInSeconds = $duration * 3600; // Convert hours to seconds
                    } else {
                        // If duration is greater than 24, it's in minutes
                        $durationInSeconds = $duration * 60; // Convert minutes to seconds
                    }

                    // Calculate the end timestamp of the exam
                    $endTimestamp = $startedTimestamp + $durationInSeconds;

                    // Check if the current timestamp is greater than or equal to the end timestamp
                    if ($currentTimestamp >= $endTimestamp) {
                        // Exam is finished
                        \backend\models\ExamModel::finishExam($exam['id']);
                    }
                } 
            }
        ?>

            <div class="col-md-6 mb-5">
                <div class="card h-100 d-flex flex-column justify-content-between"> <!-- Added d-flex and flex-column classes -->
                    <div class="card-body">
                        <h5 class="card-title"><?= Translation::make($_SESSION['appLocale'], 'groupName') . ' - '. \backend\models\GroupModel::findOne($exam['group_id'])['name'] ?></h5>
                        <h6 class="card-subtitle mb-2 text-muted"><?= Translation::make($_SESSION['appLocale'], 'examName') . ' - '.$exam['name']?></h6>
                        <h6 class="card-subtitle mb-2 text-muted"><?= Translation::make($_SESSION['appLocale'], 'testDuration') . ' - '.$exam['duration']?></h6>
                        <p class="card-text">
                            <?php 
                                $articles = explode(',', json_decode($exam['articles']));
                                $articleNames = [];
                                foreach ($articles as $article) {
                                    $articleNames[] = \backend\models\ArticleModel::findOne($article)['name'];
                                }
                             ?>
                            <p class="mt-1"><?= Translation::make($_SESSION['appLocale'], 'articles')?> </p>
                            <ul>
                                <?php foreach($articleNames as $articleName): ?>
                                    <li><?=$articleName ?></li>
                                <?php endforeach; ?>
                            </ul>
                        </p>
                    </div>
                    <div class="card-footer"> 
                        <?php 
                            $examStarted    = \backend\models\ExamModel::findOneStarted($exam['id']);
                            $examFinished   = \backend\models\ExamModel::findOneFinished($exam['id']);
                            $examNotStarted = \backend\models\ExamModel::examNotStarted($exam['id']);
                         ?>


                        
                            <?php if ($examStarted): ?>
                                <a href="?controller=exam&action=enter&exam_id=<?= $exam['id'] ?>&locale=<?=$_SESSION['appLocale']?>"  data-id="<?= $exam['id'] ?>" style="cursor: pointer;" class="enterExam w-100 btn btn-warning">
                                    <?= Translation::make($_SESSION['appLocale'], 'enter') ?>
                                </a>
                            <?php elseif ($examFinished): ?>
                                <a href="#" style="pointer-events:none;" data-id="<?= $exam['id'] ?>" class="startExamButton w-100 btn btn-danger">
                                    <?= Translation::make($_SESSION['appLocale'], 'examFinished') ?>
                                </a>
                            <?php elseif ($examNotStarted): ?>
                                <a href="#" style="pointer-events: none;" data-id="<?= $exam['id'] ?>" class="startExamButton w-100 btn btn-success">
                                    <?= Translation::make($_SESSION['appLocale'], 'examNotStarted') ?>
                                </a>
                            <?php endif; ?>
                        
                    </div>
                </div>
            </div>
        <?php endforeach; ?>
    </div>
</div>
<?=$this->render('pagination', [], 'layout') ?>