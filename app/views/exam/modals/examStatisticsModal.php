<script src="https://code.jquery.com/jquery-3.6.4.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.0/dist/js/bootstrap.bundle.min.js"></script>
<link href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.0/dist/css/bootstrap.min.css" rel="stylesheet">
<script src="https://cdn.jsdelivr.net/npm/chart.js"></script>
<link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/css/select2.min.css" rel="stylesheet">
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/js/select2.min.js"></script>

<!-- Statistics Modal -->
<div class="modal fade" id="examStatisticsModal" tabindex="-1" role="dialog" aria-labelledby="messagesModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-xl" role="document"> <!-- Set modal-xl for a larger modal -->
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="messagesModalLabel"><?= Translation::make($_SESSION['appLocale'], 'statistics') ?></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body p-5">
                <div class="container mt-5">
                    <h1 class="mb-4">
                        <?= Translation::make($_SESSION['appLocale'], 'participantsCount') ?>
                        <span id="participantsCountSpan"></span>
                    </h1>
                    <input type="hidden" id="exam-id">
                    <!-- Chart 1: Students Count - Pie Chart -->
                    <div class="row mb-4">
                        <div class="col-md-12">
                            <canvas id="studentsCountBarChart" class="chart"></canvas>
                        </div> 

                        <div class="col-md-12">
                            <h2><?= Translation::make($_SESSION['appLocale'], 'participantStatistics') ?></h2>
                            <div id="studentResultsTable"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>