<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/css/bootstrap.min.css">
<link href="https://cdn.jsdelivr.net/npm/bootstrap@4.4.1/dist/css/bootstrap.min.css" rel="stylesheet">

<script src="https://code.jquery.com/jquery-1.10.2.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.4.1/dist/js/bootstrap.bundle.min.js"></script>
<?=$this->import('professor_profile', 'css'); ?>
<?=$this->import('app', 'js'); ?>

<div class="container">
    <?=$this->render('header', [], 'layout'); ?>

    <div class="row mb-2 m-5">
        <?php foreach ($exams as $exam) : ?>
        <?php 
            /*checking if exam is finished by subtracting current timestamp and duration
            * if the result is greater that started at timestamp then the exam is finished
            * $duration int hours
            */
            \backend\models\ExamModel::checkExamFinished($exam['id']);
        ?>

            <div class="col-md-6 mb-5">
                <div class="card h-100 d-flex flex-column justify-content-between"> <!-- Added d-flex and flex-column classes -->
                    <div class="card-body">
                        <?php if(\backend\models\GroupModel::findOne($exam['group_id'])): ?>
                            <h5 class="card-title"><?= Translation::make($_SESSION['appLocale'], 'groupName') . ' - '. \backend\models\GroupModel::findOne($exam['group_id'])['name'] ?></h5>
                        <?php endif; ?>
                        <h6 class="card-subtitle mb-2 text-muted"><?= Translation::make($_SESSION['appLocale'], 'examName') . ' - '.$exam['name']?></h6>
                        <h6 class="card-subtitle mb-2 text-muted"><?= Translation::make($_SESSION['appLocale'], 'testDuration') . ' - '.$exam['duration']?></h6>
                        <p class="card-text">
                            <?php 
                                $articles = explode(',', json_decode($exam['articles']));
                                $articleNames = [];
                                foreach ($articles as $article) {
                                    $articleNames[] = \backend\models\ArticleModel::findOne($article)['name'];
                                }
                             ?>
                            <p class="mt-1"><?= Translation::make($_SESSION['appLocale'], 'articles')?> </p>
                            <ul>
                                <?php foreach($articleNames as $articleName): ?>
                                    <li><?=$articleName ?></li>
                                <?php endforeach; ?>
                            </ul>
                        </p>
                    </div>
                    <div class="card-footer"> 
                        <?php 
                            $examStarted    = \backend\models\ExamModel::findOneStarted($exam['id']);
                            $examFinished   = \backend\models\ExamModel::findOneFinished($exam['id']);
                            $examNotStarted = \backend\models\ExamModel::examNotStarted($exam['id']);
                            $examColor = $examStarted ? 'btn-warning' : ($examFinished ? 'btn-danger':' btn-primary') ;
                         ?>
                        <a href="#" data-id="<?= $exam['id'] ?>" class="startExamButton w-100 btn <?=$examColor ?>">
                            <?php if ($examStarted): ?>
                                <?= Translation::make($_SESSION['appLocale'], 'examStarted') ?>
                            <?php elseif ($examFinished): ?>
                                <?= Translation::make($_SESSION['appLocale'], 'examFinished') ?>
                            <?php elseif ($examNotStarted): ?>
                                <?= Translation::make($_SESSION['appLocale'], 'getStarted') ?>
                            <?php endif; ?>
                        </a>

                        <button class="btn btn-secondary w-100 mt-1 showExamStatistics" data-id="<?=$exam['id']?>">
                            <?= Translation::make($_SESSION['appLocale'], 'examResult') ?>
                        </button>
                    </div>
                    
                    <?= $this->render('/modals/examStatisticsModal', [], 'exam');?>
                </div>
            </div>
        <?php endforeach; ?>
    </div>
</div>
<?php $startedText = Translation::make($_SESSION['appLocale'], 'examStarted') ?>
<?=$this->render('pagination', [], 'layout') ?>

<?=$this->import('exam', 'js') ?>