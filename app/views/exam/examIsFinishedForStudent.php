<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-QWTKZyjpPEjISv5WaRU9OFeRpok6YctnYmDr5pNlyT2bRjXh0JMhjY6hW+ALEwIH" crossorigin="anonymous">
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-YvpcrYf0tY3lHB60NNkmXc5s9fDVZLESaAA55NDzOxhy9GkcIdslK1eN7N6jIeHz" crossorigin="anonymous"></script>

<div class="container col-xxl-8 px-4 py-5">
    <div class="row flex-lg-row-reverse align-items-center g-5 py-5">
        <div class="col-10 col-sm-8 col-lg-6">
            <img src="https://static.vecteezy.com/system/resources/previews/004/896/095/original/paper-test-with-timer-checklist-exam-concept-illustration-flat-design-eps10-vector.jpg" width="700" loading="lazy">
        </div>
        <div class="col-lg-6">
            <h1 class=" fw-bold lh-1 mb-3"><?=Translation::make($_SESSION['appLocale'], 'examIsEndedForStudent') ?></h1>
            <button onclick="history.back()" class="btn btn-primary"><?=Translation::make($_SESSION['appLocale'], 'back') ?></button>
        </div>
    </div>
</div>