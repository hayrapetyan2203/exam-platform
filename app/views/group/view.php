<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <title><?= $_SESSION['user']['username'] . ' ' .  $_SESSION['user']['userSurname'] ?></title>
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <!-- project assets -->
  <?= $this->import('professor_profile', 'css'); ?>
  <?= $this->import('group', 'css'); ?>

  <!-- cdns -->
  <!-- bootstrap css -->
  <link href="https://cdn.jsdelivr.net/npm/bootstrap@4.4.1/dist/css/bootstrap.min.css" rel="stylesheet">
  <!-- js chart -->
  <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.5.0/Chart.min.js"></script>

  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.13/css/select2.min.css">
</head>

<body>
  <div class="container">
    <div class="main-body">
      <!-- header -->
      <?= $this->render('header', [], 'layout'); ?>

      <section class="bg-white text dark p-5">
        <h1><?= $group['name'] ?></h1>
      </section>

      <section>
        <div class="container py-5">

          <div class="row">

            <div class="col-md-6 col-lg-5 col-xl-4 mb-4 mb-md-0">

              <h5 class="font-weight-bold mb-3 text-center text-lg-start text-white bg-dark p-3">
                <?= Translation::make($_SESSION['appLocale'], 'groupMembers') ?>
              </h5>

              <div class="card">
                <div class="card-body">

                  <ul class="list-unstjoyled mb-0">
                    <?php foreach ($members as $k => $member) : ?>
                      <li class="p-2 border-bottom bg-body-tertiary">
                        <a href="#" class="d-flex justify-content-between">
                          <div class="d-flex flex-row">
                            <img src="https://e7.pngegg.com/pngimages/84/165/png-clipart-united-states-avatar-organization-information-user-avatar-service-computer-wallpaper-thumbnail.png" alt="avatar"
                              class="rounded-circle d-flex align-self-center me-3 shadow-1-strong" width="60">
                            <div class="pt-1">
                              <p class="fw-bold mb-0"><?= $member['fullname'] ?></p>
                            </div>
                          </div>
                        </a>
                      </li>
                    <?php endforeach; ?>
                  </ul>

                </div>
              </div>

            </div>

            <div class="col-md-6 col-lg-7 col-xl-8">

              <form action="FrontController.php?controller=group&action=addMessage&locale=hy&id=<?= $_GET['id'] ?>" method="post">
                <ul class="list-unstyled">
                  <?php foreach ($messages as $k => $message) : ?>
                    <li class="d-flex justify-content-start mb-4">
                      <img src="https://e7.pngegg.com/pngimages/84/165/png-clipart-united-states-avatar-organization-information-user-avatar-service-computer-wallpaper-thumbnail.png" alt="avatar"
                        class="rounded-circle d-flex align-self-start me-3 shadow-1-strong" width="60">
                       

                      <div class="card" style="width: 100%;">
                        <div class="card-header d-flex justify-content-between p-3">
                          <p class="fw-bold mb-0"><?=$message['fullname']?></p>
                          <p class="text-muted small mb-0"><i class="far fa-clock"></i> <?=date($message['created_at'])?></p>
                        </div>
                        <div class="card-body">
                          <p class="mb-0">
                            <?=$message['message'];?>
                          </p>
                        </div>
                      </div>
                    </li>
                  <?php endforeach; ?>

                  <li class="bg-white mb-3">
                    <div data-mdb-input-init class="form-outline">
                      <textarea class="form-control bg-body-tertiary" id="textAreaExample2" name="message" rows="4"></textarea>
                      <label class="form-label" for="textAreaExample2"></label>
                    </div>
                  </li>
                  <input type="hidden" name="user_id" value="<?= $_SESSION['user']['id'] ?>">
                  <button type="submit" data-mdb-button-init data-mdb-ripple-init class="btn btn-info btn-rounded float-end">
                    <?= Translation::make($_SESSION['appLocale'], 'send') ?>
                  </button>
                </ul>
              </form>

            </div>

          </div>

        </div>
      </section>
    </div>
  </div>

  <script src="https://code.jquery.com/jquery-1.10.2.min.js"></script>
  <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.4.1/dist/js/bootstrap.bundle.min.js"></script>

  <?= $this->import('group', 'js'); ?>

</body>

</html>