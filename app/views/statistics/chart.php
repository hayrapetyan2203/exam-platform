<div class="wg-box">
    <div class="flex items-center justify-between">
        <h5><?= Translation::make($_SESSION['appLocale'], 'examCount') ?></h5>
        <div class="dropdown default">
            <button class="btn btn-secondary dropdown-toggle" type="button" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <span class="icon-more"><i class="icon-more-horizontal"></i></span>
            </button>
            <ul class="dropdown-menu dropdown-menu-end">
                <li>
                    <a href="javascript:void(0);">This Week</a>
                </li>
                <li>
                    <a href="javascript:void(0);">Last Week</a>
                </li>
            </ul>
        </div>
    </div>
    <div id="line-chart-5"></div>
</div>