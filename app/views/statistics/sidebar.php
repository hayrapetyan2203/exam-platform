<div class="section-menu-left">
    <div class="box-logo">
        <a href="index.html" id="site-logo-inner">
            <h5><?= Translation::make($_SESSION['appLocale'], 'statistics') ?></h5>
        </a>
        <div class="button-show-hide">
            <i class="icon-menu-left"></i>
        </div>
    </div>
    <div class="section-menu-left-wrap">
        <div class="center">
            <div class="center-item">
                <div class="center-heading"></div>
                <ul class="menu-list">
                    <li class="menu-item has-children active">
                        <a href="javascript:void(0);" class="menu-item-button">
                            <div class="icon"><i class="icon-grid"></i></div>
                            <div class="text"><?= Translation::make($_SESSION['appLocale'], 'statistics') ?></div>
                        </a>
                        <ul class="sub-menu" style="display: block;">
                            <li class="sub-menu-item">
                                
                                <a href="?controller=statistics&action=index&locale=<?= $_SESSION['appLocale'] ?>#" 
                                    class="<?=$this->get['action'] === 'index' ? 'active' : ''  ?>">
                                    <div class="text"><?= Translation::make($_SESSION['appLocale'], 'mainPage') ?></div>
                                </a>
                            </li>
                            <li class="sub-menu-item">
                                <a href="?controller=statistics&action=students&locale=<?= $_SESSION['appLocale'] ?>#"
                                    class="<?=$this->get['action'] === 'students' ? 'active' : ''  ?>">
                                    <div class="text"><?= Translation::make($_SESSION['appLocale'], 'studentList') ?></div>
                                </a>
                            </li>
                            <li class="sub-menu-item">
                                <a href="?controller=statistics&action=articleStats&locale=<?= $_SESSION['appLocale'] ?>#"
                                   class="<?=$this->get['action'] === 'articleStats' ? 'active' : ''  ?>">
                                    <div class="text"><?= Translation::make($_SESSION['appLocale'], 'articleStats') ?></div>
                                </a>
                            </li>
                        </ul>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>