<style>
    .buttons{
        display: contents;
    }
    .buttons i {
        font-size: 20px;
    }
</style>
<div class="wg-box">
    <div class="flex items-center justify-between">
        <h5><?= Translation::make($_SESSION['appLocale'], 'topStudents') ?></h5>
        <div class="dropdown default">
            <button class="btn btn-secondary dropdown-toggle" type="button" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <span class="view-all"><?= Translation::make($_SESSION['appLocale'], 'viewAll') ?><i class="icon-chevron-down"></i></span>
            </button>
            <ul class="dropdown-menu dropdown-menu-end">
                <li>
                    <a href="javascript:void(0);">3 days</a>
                </li>
                <li>
                    <a href="javascript:void(0);">7 days</a>
                </li>
            </ul>
        </div>
    </div>
    <div class="wg-table table-top-product">
        <ul class="flex flex-column gap14">
            <li class="product-item">
                <div class="image">
                    <img class="small-image" src="<?= Config::imagePath("armen.png") ?>" alt="">
                </div>
                <div class="flex items-center justify-between flex-grow">
                    <div class="name">
                        <a href="product-list.html" class="body-title-2">Հայրապետյան Արմեն Մանվելի</a>
                    </div>
                    <div>
                        <div class="text-tiny mb-3"><?= Translation::make($_SESSION['appLocale'], 'groupName') ?></div>
                        <div class="body-text">ՏՏ ֊ 955-1</div>
                    </div>
                    <div class="buttons">
                        <button class="btn btn-icon" aria-label="View">
                            <i class="fas fa-eye"></i>
                        </button>
                        <button class="btn btn-icon" aria-label="Download Statistics">
                            <i class="fas fa-download"></i>
                        </button>
                    </div>
                </div>
            </li>
            <li class="product-item">
                <div class="image">
                    <img class="small-image" src="<?= Config::imagePath("student2.png") ?>" alt="">
                </div>
                <div class="flex items-center justify-between flex-grow">
                    <div class="name">
                        <a href="product-list.html" class="body-title-2">Հակոբյան Վարդան Սմբատի</a>
                    </div>
                    <div>
                        <div class="text-tiny mb-3"><?= Translation::make($_SESSION['appLocale'], 'groupName') ?></div>
                        <div class="body-text">ՏՏ ֊ 955-1</div>
                    </div>
                    <div class="buttons">
                        <button class="btn btn-icon" aria-label="View">
                            <i class="fas fa-eye"></i>
                        </button>
                        <button class="btn btn-icon" aria-label="Download Statistics">
                            <i class="fas fa-download"></i>
                        </button>
                    </div>
                </div>
            </li>
            <li class="product-item">
                <div class="image">
                    <img class="small-image" src="<?= Config::imagePath("student3.jpg") ?>" alt="">
                </div>
                <div class="flex items-center justify-between flex-grow">
                    <div class="name">
                        <a href="product-list.html" class="body-title-2">Սիմոնյան Անահիտ Արմանի</a>
                    </div>
                    <div>
                        <div class="text-tiny mb-3"><?= Translation::make($_SESSION['appLocale'], 'groupName') ?></div>
                        <div class="body-text">ՏՏ ֊ 955-1</div>
                    </div>
                    <div class="buttons">
                        <button class="btn btn-icon" aria-label="View">
                            <i class="fas fa-eye"></i>
                        </button>
                        <button class="btn btn-icon" aria-label="Download Statistics">
                            <i class="fas fa-download"></i>
                        </button>
                    </div>
                </div>
            </li>
            <li class="product-item">
                <div class="image">
                    <img class="small-image" src="<?= Config::imagePath("student1.jpg") ?>" alt="">
                </div>
                <div class="flex items-center justify-between flex-grow">
                    <div class="name">
                        <a href="product-list.html" class="body-title-2">Կոստանյան Միքաել Համլետի</a>
                    </div>
                    <div>
                        <div class="text-tiny mb-3"><?= Translation::make($_SESSION['appLocale'], 'groupName') ?></div>
                        <div class="body-text">ՏՏ ֊ 855֊2</div>
                    </div>
                    <div class="buttons">
                        <button class="btn btn-icon" aria-label="View">
                            <i class="fas fa-eye"></i>
                        </button>
                        <button class="btn btn-icon" aria-label="Download Statistics">
                            <i class="fas fa-download"></i>
                        </button>
                    </div>
                </div>
            </li>
            <li class="product-item">
                <div class="image">
                    <img class="small-image" src="<?= Config::imagePath("student4.png") ?>" alt="">
                </div>
                <div class="flex items-center justify-between flex-grow">
                    <div class="name">
                        <a href="product-list.html" class="body-title-2 mb-3">Խաչատրըան Սիլվա Անդրանիկի</a>
                    </div>
                    <div>
                        <div class="text-tiny mb-3"><?= Translation::make($_SESSION['appLocale'], 'groupName') ?></div>
                        <div class="body-text">ՏՏ ֊ 855֊2</div>
                    </div>
                    <div class="buttons">
                        <button class="btn btn-icon" aria-label="View">
                            <i class="fas fa-eye"></i>
                        </button>
                        <button class="btn btn-icon" aria-label="Download Statistics">
                            <i class="fas fa-download"></i>
                        </button>
                    </div>
                </div>
            </li>
        </ul>
    </div>
</div>
