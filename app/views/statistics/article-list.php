<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.7.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>

<div class="container">
    <h2><?= Translation::make($_SESSION['appLocale'], 'articleStats') ?></h2>
    <div class="m-5">
        <label for="dateType"><?= Translation::make($_SESSION['appLocale'], 'chooseDuration') ?></label>
        <select id="dateType">
            <option><?= Translation::make($_SESSION['appLocale'], 'durationYear') ?></option>
            <option>— <?= Translation::make($_SESSION['appLocale'], 'firstCourse') ?></option>
            <option>— <?= Translation::make($_SESSION['appLocale'], 'secondCourse') ?></option>
            <option>— <?= Translation::make($_SESSION['appLocale'], 'thirdCourse') ?></option>
            <option>— <?= Translation::make($_SESSION['appLocale'], 'forthCourse') ?></option>
            <option disabled>──────────</option>
            <option><?= Translation::make($_SESSION['appLocale'], 'durationYearQuarter') ?></option>
            <option>— <?= Translation::make($_SESSION['appLocale'], 'firstYearFirstQuarter') ?></option>
            <option>— <?= Translation::make($_SESSION['appLocale'], 'firstYearSecondQuarter') ?></option>
            <option>— <?= Translation::make($_SESSION['appLocale'], 'secondYearFirstQuarter') ?></option>
            <option>— <?= Translation::make($_SESSION['appLocale'], 'secondYearSecondQuarter') ?></option>
            <option>— <?= Translation::make($_SESSION['appLocale'], 'thirdYearFirstQuarter') ?></option>
            <option>— <?= Translation::make($_SESSION['appLocale'], 'thirdYearSecondQuarter') ?></option>
            <option>— <?= Translation::make($_SESSION['appLocale'], 'forthYearFirstQuarter') ?></option>
            <option>— <?= Translation::make($_SESSION['appLocale'], 'forthYearSecondQuarter') ?></option>
            <option disabled>──────────</option>
        </select>
    </div>
    <div class="dropdown">
        <button class="btn btn-default dropdown-toggle" type="button" data-toggle="dropdown">
            Առարկաներ
        </button>
        <ul class="dropdown-menu">
            <li class="dropdown-submenu">
                <a class="test" tabindex="-1" href="#">Մաթեմատիկա <span class="caret"></span></a>
                <ul class="dropdown-menu">
                    <li class="dropdown-submenu">
                        <a class="test" tabindex="-1" href="#">Խումբ AB-233 <span class="caret"></span></a>
                        <ul class="dropdown-menu">
                            <li><a tabindex="-1" href="#">Անի Գրիգորյան</a></li>
                            <li><a tabindex="-1" href="#">Տիգրան Սահակյան</a></li>
                            <li><a tabindex="-1" href="#">Մարիամ Պետրոսյան</a></li>
                        </ul>
                    </li>
                    <li class="dropdown-submenu">
                        <a class="test" tabindex="-1" href="#">Խումբ BA-144 <span class="caret"></span></a>
                        <ul class="dropdown-menu">
                            <li><a tabindex="-1" href="#">Արմեն Հարությունյան</a></li>
                            <li><a tabindex="-1" href="#">Լիլիթ Կարապետյան</a></li>
                            <li><a tabindex="-1" href="#">Հասմիկ Սարգսյան</a></li>
                        </ul>
                    </li>
                </ul>
            </li>
            <li class="dropdown-submenu">
                <a class="test" tabindex="-1" href="#">Քիմիա <span class="caret"></span></a>
                <ul class="dropdown-menu">
                    <li class="dropdown-submenu">
                        <a class="test" tabindex="-1" href="#">Խումբ CH-321 <span class="caret"></span></a>
                        <ul class="dropdown-menu">
                            <li><a tabindex="-1" href="#">Սոֆի Ավագյան</a></li>
                            <li><a tabindex="-1" href="#">Նարեկ Դանիելյան</a></li>
                            <li><a tabindex="-1" href="#">Արփի Հովհաննիսյան</a></li>
                        </ul>
                    </li>
                </ul>
            </li>
            <li class="dropdown-submenu">
                <a class="test" tabindex="-1" href="#">Անգլերեն <span class="caret"></span></a>
                <ul class="dropdown-menu">
                    <li class="dropdown-submenu">
                        <a class="test" tabindex="-1" href="#">Խումբ EN-101 <span class="caret"></span></a>
                        <ul class="dropdown-menu">
                            <li><a tabindex="-1" href="#">Դավիթ Մանուկյան</a></li>
                            <li><a tabindex="-1" href="#">Նունե Հակոբյան</a></li>
                            <li><a tabindex="-1" href="#">Գոռ Մելքոնյան</a></li>
                        </ul>
                    </li>
                </ul>
            </li>
        </ul>

    </div>
    <button style="margin-top: 5%;"><?= Translation::make($_SESSION['appLocale'], 'search') ?></button>

</div>
<script>
    $(document).ready(function () {
        $('.dropdown-submenu a.test').on("click", function (e) {
            $(this).next('ul').toggle();
            e.stopPropagation();
            e.preventDefault();
        });
    });
</script>

