<div class="wg-box">
    <div class="flex items-center justify-between">
        <h5><?= Translation::make($_SESSION['appLocale'], 'exams') ?></h5>
        <div class="dropdown default">
            <button class="btn btn-secondary dropdown-toggle" type="button" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <span class="view-all"><?= Translation::make($_SESSION['appLocale'], 'viewAll') ?><i class="icon-chevron-down"></i></span>
            </button>
            <ul class="dropdown-menu dropdown-menu-end">
                <li>
                    <a href="javascript:void(0);">3 days</a>
                </li>
                <li>
                    <a href="javascript:void(0);">7 days</a>
                </li>
            </ul>
        </div>
    </div>
    <div class="wg-table table-product-overview">
        <ul class="table-title flex gap20 mb-14">
            <li>
                <div class="body-title"><?= Translation::make($_SESSION['appLocale'], 'examName') ?></div>
            </li>
            <li>
                <div class="body-title"><?= Translation::make($_SESSION['appLocale'], 'groupName') ?></div>
            </li>
            <li>
                <div class="body-title"><?= Translation::make($_SESSION['appLocale'], 'duration') ?></div>
            </li>
            <li>
                <div class="body-title"><?= Translation::make($_SESSION['appLocale'], 'article') ?></div>
            </li>
            <li>
                <div class="body-title"><?= Translation::make($_SESSION['appLocale'], 'test') ?></div>
            </li>
            <li>
                <div class="body-title"><?= Translation::make($_SESSION['appLocale'], 'status') ?></div>
            </li>
        </ul>

        <!-- exam list -->
        <ul class="flex flex-column gap10"></ul>
    </div>
    <div class="divider"></div>
    <div class="flex items-center justify-between flex-wrap gap10">
        <ul class="wg-pagination">
            <li>
                <a href="#"><i class="icon-chevron-left"></i></a>
            </li>
            <li class="active">
                <a href="#">1</a>
            </li>
            <li class="">
                <a href="#">2</a>
            </li>
            <li>
                <a href="#">3</a>
            </li>
            <li>
                <a href="#"><i class="icon-chevron-right"></i></a>
            </li>
        </ul>
    </div>
</div>