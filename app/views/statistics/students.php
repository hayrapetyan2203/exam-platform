<!DOCTYPE html><!--[if IE 8 ]><html class="ie" xmlns="http://www.w3.org/1999/xhtml" xml:lang="en-US" lang="en-US"> <![endif]--><!--[if (gte IE 9)|!(IE)]><!-->
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en-US" lang="en-US"><!--<![endif]-->

<head>
    <!-- Basic Page Needs -->
    <meta charset="utf-8">
    <!--[if IE]><meta http-equiv='X-UA-Compatible' content='IE=edge,chrome=1'><![endif]-->
    <title><?= Translation::make($_SESSION['appLocale'], 'statistics') ?></title>

    <meta name="author" content="themesflat.com">

    <!-- Mobile Specific Metas -->
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

    <!-- Theme Style -->
    <?= $this->import('statistics/animate.min', 'css'); ?>
    <?= $this->import('statistics/animation', 'css'); ?>
    <?= $this->import('statistics/bootstrap', 'css'); ?>
    <?= $this->import('statistics/bootstrap-select.min', 'css'); ?>
    <?= $this->import('statistics/style', 'css'); ?>

    <?= $this->cdnFactory('fontawesome'); ?>

    <!-- Font -->
    <?= $this->import('statistics/fonts', 'css'); ?>

    <!-- Icon -->
    <?= $this->import('statistics/style_1', 'css'); ?>

    <!-- Favicon and Touch Icons  -->
    <link rel="shortcut icon" href="favicon.png">
    <link rel="apple-touch-icon-precomposed" href="favicon.png">

</head>

<body class="body">

    <!-- #wrapper -->
    <div id="wrapper">
        <!-- #page -->
        <div id="page" class="">
            <!-- layout-wrap -->
            <div class="layout-wrap">
                <!-- preload -->
                <div id="preload" class="preload-container">
                    <div class="preloading">
                        <span></span>
                    </div>
                </div>

                <!-- /preload -->
                <!-- section-menu-left -->
                <?= $this->render('sidebar', []); ?>
                <!-- /section-menu-left -->
                <!-- section-content-right -->
                <div class="section-content-right">
                    <!-- header-dashboard -->
                    <?= $this->render('header', []); ?>
                    <!-- /header-dashboard -->
                    <!-- main-content -->
                    <div class="main-content">
                        <!-- main-content-wrap -->
                        <div class="main-content-inner">
                            <!-- main-content-wrap -->
                            <div class="main-content-wrap">
                                <?= $this->render('student-list', []); ?>
                            </div>
                            <!-- /main-content-wrap -->
                        </div>
                        <!-- /main-content-wrap -->
                    </div>
                    <!-- /main-content -->
                </div>
                <!-- /section-content-right -->
            </div>
            <!-- /layout-wrap -->
        </div>
        <!-- /#page -->
    </div>
    <!-- /#wrapper -->

    <!-- Javascript -->
    <?= $this->import('statistics/jquery.min', 'js'); ?>
    <?= $this->import('statistics/bootstrap.min', 'js'); ?>
    <?= $this->import('statistics/bootstrap-select.min', 'js'); ?>
    <?= $this->import('statistics/zoom', 'js'); ?>
    <?= $this->import('statistics/apexcharts', 'js'); ?>
    <?= $this->import('statistics/line-chart-1', 'js'); ?>
    <?= $this->import('statistics/line-chart-2', 'js'); ?>
    <?= $this->import('statistics/line-chart-3', 'js'); ?>
    <?= $this->import('statistics/line-chart-4', 'js'); ?>
    <?= $this->import('statistics/line-chart-5', 'js'); ?>
    <?= $this->import('statistics/line-chart-6', 'js'); ?>
    <?= $this->import('statistics/switcher', 'js'); ?>
    <?= $this->import('statistics/theme-settings', 'js'); ?>
    <?= $this->import('statistics/main', 'js'); ?>



</body>

</html>