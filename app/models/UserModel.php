<?php
require_once (__DIR__ . '/BaseModel.php');
class UserModel extends BaseModel
{
    public static $conn;
    const ROLE_PROFESSOR = 1;
    const ROLE_STUDENT = 0;
    const INACTIVE = 0;
    const ACTIVE = 1;

    /**
     * @throws Exception
     */
    public static function register($postParams)
    {
        self::$conn = self::dbConn();

        $username = $postParams['username'];
        $userSurname = $postParams['userSurname'];
        $userFathername = $postParams['userFathername'];
        $useremail = $postParams['useremail'];
        $password = password_hash($postParams['password'], PASSWORD_BCRYPT);
        $role = (isset($postParams['role']) && $postParams['role'] == 'professor') ? self::ROLE_PROFESSOR : self::ROLE_STUDENT;
        //$activation_code = rand(100000, 999999);
        $activation_code = 100000;
        $is_active = self::INACTIVE;
        try {
            // set the PDO error mode to exception
            $sql = "INSERT INTO users (username, userSurname, userFathername, useremail, password, role, activation_code, is_active)
                     VALUES ('$username', '$userSurname', '$userFathername', '$useremail', '$password', '$role', '$activation_code', '$is_active')";
            self::$conn->exec($sql);
            return true;
        } catch(PDOException $e) {
            throw new Exception($e->getMessage(), $e->getCode());
        }
    }

    public static function findByEmail($useremail)
    {
        self::$conn = self::dbConn();
        try {
            $stmt = self::$conn->prepare("SELECT * FROM users WHERE useremail = :useremail");
            $stmt->bindParam(':useremail', $useremail);
            $stmt->execute();

            // Fetch the first row as an associative array
            return $stmt->fetch(PDO::FETCH_ASSOC);
        } catch (Exception $e) {
            return null;
        }
    }

    public static function edit($userInfo)
    {
        try {

            $user = self::findOne($userInfo['id']);
                
            $filePath = '';
            if($user['imagePath'] === $userInfo['imagePath']){
                $filePath = $user['imagePath'];
            }else{
                if (isset($_FILES['image'])) {
                    $fileUploadResult = self::handleFileUpload($_FILES['image']);
                    if ($fileUploadResult['success']) {
                        $filePath = $fileUploadResult['filePath'];
                    } else {
                       throw new Exception("Error saving file", 1);
                    }
                }
            }
               
            $stmt = self::dbConn()->prepare("UPDATE users SET 
                username        = :username, 
                userSurname     = :userSurname, 
                userFathername  = :userFathername, 
                useremail       = :useremail,
                phone           = :phone, 
                address         = :address,
                imagePath       = :imagePath,
                fullname        = :fullname
                    WHERE id = :id");

            $fullname = $userInfo['username'] . ' ' . $userInfo['userSurname'] . ' ' .$userInfo['userFathername'];

            $stmt->bindParam(':id', $userInfo['id']);
            $stmt->bindParam(':username', $userInfo['username']);
            $stmt->bindParam(':userSurname', $userInfo['userSurname']);
            $stmt->bindParam(':userFathername', $userInfo['userFathername']);
            $stmt->bindParam(':useremail', $userInfo['useremail']);
            $stmt->bindParam(':phone', $userInfo['phone']);
            $stmt->bindParam(':address', $userInfo['address']);
            $stmt->bindParam(':imagePath', $filePath);
            $stmt->bindParam(':fullname', $fullname);

            return $stmt->execute();
            
        } catch (PDOException $e) {
                echo "line: " . __LINE__ . " " . __FILE__ . "<pre>"; 
                print_r([$e->getMessage()]); 
                echo "</pre>"; 
                exit;
            // Log or handle the exception appropriately
            error_log('Error: ' . $e->getMessage());
            return false; // Return false to indicate failure
        }
    }

    public static function handleFileUpload($file) {
        $uploadDir = __DIR__ . '/../uploads/';
        $uploadFile = $uploadDir . basename($file['name']);
        $returnPath = '/uploads/' . basename($file['name']);
        $uploadOk = 1;
        $imageFileType = strtolower(pathinfo($uploadFile,PATHINFO_EXTENSION));

        // Check file size (you can adjust the size as needed)
        if ($file['size'] > 5000000) { // 5 MB
            return ['success' => false, 'message' => 'File is too large.'];
        }

        // Allow certain file formats (you can modify/add more as needed)
        if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
            && $imageFileType != "gif" ) {
            return ['success' => false, 'message' => 'Only JPG, JPEG, PNG & GIF files are allowed.'];
        }

        // Check if $uploadOk is set to 0 by an error
        if ($uploadOk == 0) {
            return ['success' => false, 'message' => 'File upload failed.'];
        } else {
            // Try to upload file
            if (move_uploaded_file($file['tmp_name'], $uploadFile)) {
                return ['success' => true, 'filePath' => $returnPath];
            } else {
                return ['success' => false, 'message' => 'Error uploading file.'];
            }
        }
    }

    public static function newActivation($useremail)
    {
        try {
            $stmt = self::dbConn()->prepare("UPDATE users SET activation_code = :activation_code WHERE useremail = :useremail");
            $stmt->bindParam(':useremail', $useremail);
            //$rand = rand(100000, 999999);
            $rand = 100000;
            $stmt->bindParam(':activation_code', $rand);
            $stmt->execute();

            // Fetch the first row as an associative array
            return $stmt->fetch(PDO::FETCH_ASSOC);
        } catch (Exception $e) {
            return null;
        }
    }
    public static function setNewPassword($useremail, $password)
    {
        self::$conn = self::dbConn();
        try {
            $stmt = self::$conn->prepare("UPDATE users SET password = :password WHERE useremail = :useremail");
            $stmt->bindParam(':useremail', $useremail);
            $stmt->bindParam(':password', $password);
            $stmt->execute();

            // Fetch the first row as an associative array
            return $stmt->fetch(PDO::FETCH_ASSOC);
        } catch (Exception $e) {
            return null;
        }
    }
    public static function listStudents()
    {
        self::$conn = self::dbConn();
        try {
            $stmt = self::$conn->prepare("SELECT id, username, userSurname, fullname FROM users WHERE role = 0");
            $stmt->execute();

            // Fetch the first row as an associative array
            return $stmt->fetchAll(PDO::FETCH_ASSOC);
        } catch (Exception $e) {
            return null;
        }
    }    

    public static function list($text)
    {
        self::$conn = self::dbConn();
        $currentUserId = $_SESSION['user']['id'];
        try {
            if ($text !== '') {
                $stmt = self::$conn->prepare("SELECT id, username, userSurname FROM users WHERE username LIKE :text OR userSurname LIKE :text OR userFathername LIKE :text AND id != $currentUserId");
                $searchText = "%$text%";
                $stmt->bindParam(':text', $searchText, PDO::PARAM_STR);
            } else {
                $stmt = self::$conn->prepare("SELECT id, username, userSurname FROM users where id != $currentUserId");
            }
            $stmt->execute();

            return $stmt->fetchAll(PDO::FETCH_ASSOC);
        } catch (Exception $e) {
            return null;
        }
    }    

    public static function listConversations(int $userId)
    {
        self::$conn = self::dbConn();
        try {
            
            $stmt = self::$conn->prepare("
                select distinct sender_id, recipient_id from messages m  where m.sender_id  = $userId OR recipient_id  = $userId;
            ");
            $stmt->execute();

            $userIds =  $stmt->fetchAll(PDO::FETCH_ASSOC);
            self::removeKeysWithValue($userIds, $userId);
            $models = [];
            foreach ($userIds as $key => $value) {
                $value = array_values($value);
                $stmt = self::$conn->prepare("
                    select * from users where id = $value[0];
                ");
                $stmt->execute();

                $models[] =  $stmt->fetchAll(PDO::FETCH_ASSOC);
                    
            }
               
            // Flatten and unique the array
            $uniqueUsers = [];

            foreach ($models as $subarray) {
                foreach ($subarray as $user) {
                    $uniqueUsers[$user['id']] = $user; // Use user ID as key for uniqueness
                }
            }

            // Convert associative array to indexed array if needed
            $uniqueUsers = array_values($uniqueUsers);
                
            return $uniqueUsers;
        } catch (Exception $e) {
            return null;
        }
    }

    public static function removeKeysWithValue(&$array, $valueToRemove) {
        foreach ($array as &$subArray) {
            foreach ($subArray as $key => $value) {
                if (intval($value) === intval($valueToRemove)) {
                    unset($subArray[$key]);
                }
            }
        }
    }
    
    public static function getProfessorGroups($professor_id, $limit = 20)
    {
        self::$conn = self::dbConn();
        try {
            // Prepare the SQL statement with placeholders
            $stmt = self::$conn->prepare("SELECT DISTINCT * FROM professor_group WHERE professor_id = :professor_id ORDER BY id DESC LIMIT :limit");

            // Bind values to the placeholders
            $stmt->bindParam(':professor_id', $professor_id, PDO::PARAM_INT);
            $stmt->bindParam(':limit', $limit, PDO::PARAM_INT);

            // Execute the statement
            $stmt->execute();

            // Fetch all rows as an associative array
            return $stmt->fetchAll(PDO::FETCH_ASSOC);
        } catch (PDOException $e) {
            // Log or handle the exception appropriately
            error_log('Error: ' . $e->getMessage());
            return []; // Return an empty array or throw an exception
        }
    }

    public static function getStudentGroups($student_id, $limit = 20): array
    {
        try {
           $conn = self::dbConn();

            $groupsStmt = $conn->query("
                SELECT
                    *
                FROM
                    professor_group pg
                JOIN group_members gm ON
                    pg.id = gm.group_id
                WHERE
                    gm.student_id = $student_id;");
            
            return $groupsStmt->fetchAll(PDO::FETCH_ASSOC);

        } catch (PDOException $e) {
            // Log or handle the exception appropriately
            error_log('Error: ' . $e->getMessage());
            return []; // Return an empty array or throw an exception
        }
    }

    public static function findOne($id)
    {
        return self::dbConn()->query("select * from users where id = $id")->fetch(PDO::FETCH_ASSOC);
    }

    public static function getStudentGroupTests($student_id, $limit = 20): array
    {
        try {
            // Prepare the SQL statement with placeholders
            $stmt = self::dbConn()->prepare("SELECT DISTINCT * FROM test 
            LEFT JOIN student_group ON test.group_id = student_group.group_id 
            WHERE student_group.student_id = :student_id 
            ORDER BY student_group.id DESC LIMIT :limit");

            // Bind values to the placeholders
            $stmt->bindParam(':student_id', $student_id, PDO::PARAM_INT);
            $stmt->bindParam(':limit', $limit, PDO::PARAM_INT);

            // Execute the statement
            $stmt->execute();

            // Fetch all rows as an associative array
            $results =  $stmt->fetchAll(PDO::FETCH_ASSOC);

            // Filter out duplicates based on 'group_id'
            $uniqueResults = [];
            $seenGroupIds = [];

            foreach ($results as $row) {
                $groupId = $row['name'];
                if (!in_array($groupId, $seenGroupIds)) {
                    $uniqueResults[] = $row;
                    $seenGroupIds[] = $groupId;
                }
            }

            return $uniqueResults;

        } catch (PDOException $e) {
            // Log or handle the exception appropriately
            error_log('Error: ' . $e->getMessage());
            return []; // Return an empty array or throw an exception
        }
    }

    public static function getOne($userID)
    {
        self::$conn = self::dbConn();
        try {
            $stmt = self::$conn->prepare("SELECT id, username, userSurname FROM users WHERE id = $userID");
            $stmt->execute();

            // Fetch the first row as an associative array
            return $stmt->fetch(PDO::FETCH_ASSOC);
        } catch (Exception $e) {
            return null;
        }
    }

    public static function getUsersById($userIDs)
    {
        self::$conn = self::dbConn();
        try {
            $stmt = self::$conn->prepare("SELECT id, username, userSurname FROM users WHERE id IN ($userIDs)");
            $stmt->execute();

            // Fetch the first row as an associative array
            return $stmt->fetchAll(PDO::FETCH_ASSOC);
        } catch (Exception $e) {
            return null;
        }
    }
}