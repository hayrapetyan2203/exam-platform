<?php

namespace backend\models;
use BaseModel;
use PDO;

require_once(__DIR__ . '/BaseModel.php');

class MessageModel extends BaseModel
{
    public static $conn;

    public static function findAll($recipientId, $senderId ,$limit = 100)
    {
        // Prepare the SQL statement with placeholders
        $stmt = self::dbConn()->query("SELECT DISTINCT * FROM messages WHERE (recipient_id = $recipientId AND sender_id = $senderId )
            or (recipient_id = $senderId AND sender_id = $recipientId )
            ORDER BY `date` ASC LIMIT $limit");
        // Fetch the first row as an associative array
        return $stmt->fetchAll(PDO::FETCH_ASSOC);
    }    

    public static function send(int $recipientId, int $senderId, string $text)
    {
        // Prepare the SQL statement with placeholders
        $stmt = self::dbConn()->prepare("INSERT INTO messages (recipient_id, sender_id, `text`) VALUES (:recipientId, :senderId, :text)");
        
        // Bind the parameters to the statement
        $stmt->bindParam(':recipientId', $recipientId, PDO::PARAM_INT);
        $stmt->bindParam(':senderId', $senderId, PDO::PARAM_INT);
        $stmt->bindParam(':text', $text, PDO::PARAM_STR);
        
        // Execute the statement and return the result
        return $stmt->execute();
    }
}