<?php

namespace backend\models;

use BaseModel;
use Exception;
use PDO;

require_once(__DIR__ . '/BaseModel.php');

class GroupModel extends BaseModel
{
    public static $conn;

    public static function create($groupName, $groupMemberNames, $professor_id)
    {
        try {
            if (self::groupExisted($groupName, $professor_id)) {
                return true;
            }

            // Insert the group into the professor_group table
            $stmt = self::dbConn()->prepare("
                INSERT INTO professor_group (name, professor_id) 
                VALUES (:groupName, :professor_id)
            ");
            $stmt->bindParam(':groupName', $groupName);
            $stmt->bindParam(':professor_id', $professor_id);
            $stmt->execute();

            // Get the last inserted group ID
            $groupId = self::dbConn()->query("
                SELECT MAX(id) as id FROM professor_group
            ")->fetch()['id'];

            // Insert members into the group_members table
            $query = self::dbConn()->prepare("
                INSERT INTO group_members (student_id, group_id) 
                VALUES (:student_id, :group_id)
            ");

            foreach ($groupMemberNames as $userId) {
                $query->bindParam(':student_id', $userId);
                $query->bindParam(':group_id', $groupId);
                $query->execute();
            }

            return true;
        } catch (Exception $e) {
            return null;
        }
    }

    public static function addMessage($message, $user_id, $group_id)
    {
        try {
            // Insert the group into the professor_group table
            $stmt = self::dbConn()->prepare("
                INSERT INTO group_messages (message, user_id, group_id) 
                VALUES (:message, :user_id, :group_id)
            ");
            $stmt->bindParam(':message', $message);
            $stmt->bindParam(':user_id', $user_id);
            $stmt->bindParam(':group_id', $group_id);
            $stmt->execute();
            return true;
        } catch (Exception $e) {
            throw new Exception($e->getMessage());
        }
    }

    public static function findOne($id)
    {
        $stmt = self::dbConn()->query("select * from professor_group where id = $id");
        return $stmt->fetch(PDO::FETCH_ASSOC);
    }

    public static function getMembers($id)
    {
        $stmt = self::dbConn()->query("select * from group_members where group_id = $id");
        return $stmt->fetchAll(PDO::FETCH_ASSOC);
    }

    public static function getMembersData($id)
    {
        $stmt = self::dbConn()->query("
            SELECT * 
            FROM group_members 
            LEFT JOIN users ON group_members.student_id = users.id 
            WHERE group_id = $id
        ");
        return $stmt->fetchAll(PDO::FETCH_ASSOC);
    }

    public static function getMessages($id)
    {
        $stmt = self::dbConn()->query("
            SELECT * 
            FROM group_messages
            LEFT JOIN users ON group_messages.user_id = users.id 
            WHERE group_id = $id
        ");
        return $stmt->fetchAll(PDO::FETCH_ASSOC);
    }

    public static function groupExisted($groupName, $professor_id)
    {
        // Prepare the SQL statement with placeholders
        $stmt = self::dbConn()->prepare("SELECT id FROM professor_group WHERE name = :name AND professor_id = :professor_id");

        // Bind values to the placeholders
        $stmt->bindParam(':name', $groupName);
        $stmt->bindParam(':professor_id', $professor_id);

        // Execute the statement
        $stmt->execute();

        // Fetch the first row as an associative array
        return $stmt->fetch(PDO::FETCH_ASSOC);
    }

    public static function delete($id)
    {
        try {
            $stmt = self::dbConn()->prepare("delete from professor_group where id = :id");
            $stmt->bindParam(':id', $id);
            $stmt->execute();

            $stmt2 = self::dbConn()->prepare("delete from group_members where group_id = :id");
            $stmt2->bindParam(':id', $id);
            $stmt2->execute();
            return true;
        } catch (Exception $e) {
            return false;
        }
    }

    public static function getUseridsByNames($groupMemberNames)
    {
        $separatedMames = [];

        foreach ($groupMemberNames as $memberName) {
            $data = explode(' ', $memberName);
            $separatedMames[] = [
                'username' => $data[0],
                'userSurname' => $data[1]
            ];
        }
        self::$conn = self::dbConn();

        try {
            $returnData = [];
            foreach ($separatedMames as $userData) {
                $username = $userData['username'];
                $userSurname = $userData['userSurname'];

                // Prepare the SQL statement with placeholders
                $stmt = self::$conn->prepare("SELECT id FROM users WHERE username = :username AND userSurname = :userSurname");

                // Bind values to the placeholders
                $stmt->bindParam(':username', $username);
                $stmt->bindParam(':userSurname', $userSurname);

                // Execute the statement
                $stmt->execute();

                // Fetch the first row as an associative array
                $row = $stmt->fetch(PDO::FETCH_ASSOC);

                // Store the username, surname, and ID in the $returnData array
                if ($row) {
                    $returnData[] = $row['id'];
                }
            }

            return $returnData;
        } catch (Exception $e) {
            return null;
        }
    }
}
