<?php

namespace backend\models;
use BaseModel;
use PDO;

require_once(__DIR__ . '/BaseModel.php');

class StatisticsModel extends BaseModel
{
    const MONTH = 0;
    const PROFESSOR = 0;

    public static $conn;

    public static function annual(int $per = self::MONTH, int $userId = 0) : array
    {
        switch ($per) {
            case self::MONTH:
                return self::getAnnualByMonths($userId);

            default:
                return [];
                break;
        }
    }

    public static function getAnnualByMonths(int $userId) : array
    {
        // Assuming you have a method to get a database connection
        $conn = self::dbConn();
        $stmt = $conn->prepare("
            SELECT MONTH(es.started_at) AS month, COUNT(exam.id) AS exam_count
            FROM exam
            JOIN exam_startings es ON exam.id = es.exam_id
            WHERE professor_id = :professor_id AND es.is_finished = 1 AND YEAR(es.started_at) = YEAR(CURDATE())
            GROUP BY MONTH(es.started_at);
        ");
        $stmt->execute([':professor_id' => $userId]);
        $results = $stmt->fetchAll(PDO::FETCH_ASSOC);
        
        // Initialize an array with 12 elements, one for each month
        $monthlyExams = array_fill(0, 12, 0);
        
        // Populate the array with the query results
        foreach ($results as $row) {
            $month = (int) $row['month'];
            $examCount = (int) $row['exam_count'];
            $monthlyExams[$month - 1] = $examCount; // months are 1-based, array is 0-based
        }
        
        return $monthlyExams;
    }

}