<?php

namespace backend\models;
use BaseModel;
use Exception;
use PDO;

require_once(__DIR__ . '/BaseModel.php');

class SecurityModel extends BaseModel
{
    public static $conn;

    /**
     * @throws Exception
     */
    public static function addFailLoginAttempt($user, $ip): bool
    {
        $stmt = self::dbConn()->prepare("INSERT INTO fail_login_attempts (user, ip, email ) VALUES (:user, :ip, :email)");

        $email = json_encode($user['useremail']);
        $user = json_encode($user);


        $stmt->bindParam(':user', $user);
        $stmt->bindParam(':ip', $ip);
        $stmt->bindParam(':email', $email);

        // Execute the statement and return the result
        return $stmt->execute();
    }

    /**
     * @throws Exception
     */
    public static function checkLoginAttempts($user, $ip) : bool
    {
        //exit(print_r($user, true));
        $stmt = self::dbConn()->prepare(
            "SELECT COUNT(*) as attempt_count 
         FROM fail_login_attempts 
         WHERE email = :email AND ip = :ip"
        );

        if(isset($user['useremail'])){
            $email = json_encode($user['useremail']);

            $stmt->bindParam(':email', $email);
            $stmt->bindParam(':ip', $ip);
            $stmt->execute();

            $result = $stmt->fetch(PDO::FETCH_ASSOC);
            // Check if the count exceeds the threshold (e.g., 3)
            return $result['attempt_count'] > 3;
        }

        return false;
    }

}