<?php

namespace backend\models;
use BaseModel;
use PDO;

require_once(__DIR__ . '/BaseModel.php');

class ArticleModel extends BaseModel
{
    public static $conn;

    public static function get()
    {
        $stmt = self::dbConn()->query("select * from article");
        return $stmt->fetchAll(PDO::FETCH_ASSOC);
    }    

    public static function findOne($id)
    {
        $stmt = self::dbConn()->query("select * from article where id = $id");
        return $stmt->fetch(PDO::FETCH_ASSOC);
    }

}