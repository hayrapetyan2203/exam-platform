<?php

namespace backend\models;
use BaseModel;
use PDO;

require_once(__DIR__ . '/BaseModel.php');

class TestModel extends BaseModel
{
    public static $conn;

    public static function create($post, $professor_id)
    {
        try {
            $rowCount = 0;

            $stmt = self::dbConn()->prepare("INSERT INTO test (group_id, max_points, max_questions, name, professor_id, articles, questions, easyQuestionPoints, midQuestionPoints, hardQuestionPoints) VALUES (:group_id, :max_points, :max_questions, :name, :professor_id, :articles, :questions, :easyQuestionPoints, :midQuestionPoints, :hardQuestionPoints)");

            $stmt->bindParam(':group_id', $post['chooseGroop']);

            $stmt->bindParam(':easyQuestionPoints', $post['easyQuestionPoints']);
            $stmt->bindParam(':midQuestionPoints' , $post['midQuestionPoints']);
            $stmt->bindParam(':hardQuestionPoints', $post['hardQuestionPoints']);

            $stmt->bindParam(':max_points', $post['maxPoints']);
            $stmt->bindParam(':max_questions', $post['maxQuestions']);                
            $stmt->bindParam(':name', $post['testName']);
            $stmt->bindParam(':professor_id', $professor_id);
            $articles = json_encode($post['articles'], JSON_UNESCAPED_UNICODE);
            $stmt->bindParam(':articles', $articles);
            $encodedQuestion  = json_encode($post['questions'], JSON_UNESCAPED_UNICODE);
            $stmt->bindParam(':questions', $encodedQuestion);

            $stmt->execute();
            $rowCount += $stmt->rowCount(); // Increment the row count

            return $rowCount;
        } catch (Exception $e) {
            return null;
        }
    }

    public static function findOne($id)
    {
        $stmt = self::dbConn()->query("select * from test where id = $id");
        return $stmt->fetch(PDO::FETCH_ASSOC);
    }

    public static function get($limit = 5)
    {
        // Prepare the SQL statement with placeholders
        $stmt = self::dbConn()->query("SELECT DISTINCT * FROM test ORDER BY id DESC LIMIT $limit");
        // Fetch the first row as an associative array
        return $stmt->fetchAll(PDO::FETCH_ASSOC);
    }

    public static function delete($id){
        $stmt = self::dbConn()->prepare("delete from test where id = :id");
        $stmt->bindParam(':id', $id);
        return $stmt->execute();
    }

}