<?php

class BaseModel
{
    public static $dbConfigs = [];

    /**
     * @throws Exception
     */
    public static function dbConn(){
        $servername = '127.0.0.1';
        $username = 'root';
        $password = '';
        $dbName = 'exam_platform';

        try {
            $conn = new PDO("mysql:host=$servername;dbname=$dbName", $username, $password);
            // set the PDO error mode to exception
            $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            return $conn;
        } catch(PDOException $e) {
            throw new Exception($e->getMessage());
        }
    }
}