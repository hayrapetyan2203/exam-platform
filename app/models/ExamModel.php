<?php

namespace backend\models;
use BaseModel;
use PDO;

require_once(__DIR__ . '/BaseModel.php');

class ExamModel extends BaseModel
{
    public static $conn;

    public static function get()
    {
        // Prepare the SQL statement with placeholders
        $stmt = self::dbConn()->query("select * from exam");
        // Fetch the first row as an associative array
        return $stmt->fetchAll(PDO::FETCH_ASSOC);
    }

    public static function create($examType, $examName, $examGroup, $examDuration, $examArticles, $examTests, $professorId)
    {
        try {
            $stmt = self::dbConn()->prepare("INSERT INTO exam (type, name, group_id, duration, articles, tests, professor_id) VALUES (:examType, :examName, :examGroup, :duration, :articles, :tests, :professor_id)");
            
            $stmt->bindParam(':examType', $examType);
            $stmt->bindParam(':examName', $examName);

            $json_encode = json_encode($examArticles, JSON_UNESCAPED_UNICODE);
            $stmt->bindParam(':articles', $json_encode);            

            $json_encode_tests = json_encode($examTests, JSON_UNESCAPED_UNICODE);
            $stmt->bindParam(':tests', $json_encode_tests);

            $stmt->bindParam(':examGroup', $examGroup);
            $stmt->bindParam(':duration', $examDuration);
            $stmt->bindParam(':professor_id', $professorId);
            $stmt->execute();
            return true;
        } catch (Exception $e) {
            return null;
        }
    }

    public static function delete($id){
        try {
            $stmt = self::dbConn()->prepare("delete from exam where id = :id");
            $stmt->bindParam(':id', $id);
            $stmt->execute();
            return true;
        }catch (Exception $e){
            return false;
        }
    }

    public static function findOne($id)
    {
        return self::dbConn()->query("select * from exam where id = $id")->fetch(PDO::FETCH_ASSOC);
    }      

    public static function findExamResultForStudent($id, $student_id)
    {
        return self::dbConn()->query("select * from exam_result where exam_id = $id AND student_id = $student_id")->fetch(PDO::FETCH_ASSOC);
    }    

    public static function findOneStarted($id)
    {
        return self::dbConn()->query("select * from exam_startings where exam_id = $id AND is_finished = 0")
                             ->fetch(PDO::FETCH_ASSOC);
    }     

    public static function findOneStarting($id)
    {
        return self::dbConn()->query("select * from exam_startings where exam_id = $id")
                             ->fetch(PDO::FETCH_ASSOC);
    }    

    public static function findOneFinished($id)
    {
        return self::dbConn()->query("select * from exam_startings where exam_id = $id AND is_finished = 1")
                             ->fetch(PDO::FETCH_ASSOC);
    }    

    public static function finishExam($id)
    {
        return self::dbConn()->query("UPDATE exam_startings SET is_finished = 1 WHERE exam_id = $id");
    } 

    public static function showResults($id)
    {
        $results = self::dbConn()->query("SELECT * from exam_result where exam_id = $id")->fetchAll(PDO::FETCH_ASSOC);

        $data                       = [];
        $data['studentCount']       = count($results);
        $data['eachStudentResult']  = $results;
        $data['badCount']           = 0;
        $data['mediumCount']        = 0;
        $data['goodCount']          = 0;

        foreach ($results as $key => $result) {
            switch (json_decode($result['result'])->results->resultCategory) {
                case 'bad':
                    $data['badCount']++;
                    break;                
                case 'medium':
                    $data['mediumCount']++;
                    break;                
                case 'good':
                    $data['goodCount']++;
                    break;
                
                default:
                    // code...
                    break;
            }
        }

        return $data;
    }     

    public static function getParticipants($id)
    {
        return self::dbConn()->query("
            SELECT
                users.id,
                users.fullname as text
            FROM
                users
            join exam_result er on
                users.id = er.student_id
            WHERE
                er.exam_id = $id
            group by id;
        ")->fetchAll(PDO::FETCH_ASSOC);
    }    

    public static function examNotStarted($id)
    {
        return self::dbConn()->query("select * from exam_startings where exam_id = $id")
                             ->fetch(PDO::FETCH_ASSOC) ? false : true;
    }

    public static function start($id){

        $exam = self::findOne($id);
        $duration = (int) filter_var($exam['duration'], FILTER_SANITIZE_NUMBER_INT);

        try {
            if(!self::findOneStarted($id)){
                $stmt = self::dbConn()->prepare("
                    INSERT INTO exam_startings(exam_id, duration)
                        VALUES(:id, :duration)
                ");

                $stmt->bindParam(':id', $id);
                $stmt->bindParam(':duration', $duration);
                $stmt->execute();
            }
            return true;
        }catch (Exception $e){
            return false;
        }
    }    

    public static function saveResults(int $examId, array $result, int $studentId){
        try {
            $stmt = self::dbConn()->prepare("
                INSERT INTO exam_result (exam_id, result, student_id)
                VALUES (:exam_id, :result, :student_id)
            ");

            $resultJson = json_encode($result, JSON_UNESCAPED_UNICODE);

            $stmt->bindParam(':exam_id', $examId);
            $stmt->bindParam(':result', $resultJson);
            $stmt->bindParam(':student_id', $studentId);

            $stmt->execute();
            return true;
        } catch (PDOException $e) {
            // You might want to log the error message here
            // error_log($e->getMessage());
            return false;
        }
    }

    public static function getStartingStatus($id){
        try {
            $finished = self::findOneFinished($id);

            if($finished){
                return [
                    'color' => 'gray',
                    'description' => 'examFinished'
                ];
            }

            $started = self::findOneStarted($id);

            if($started){
                return [
                    'color' => 'orange',
                    'description' => 'examStarted'
                ];
            }

            if(!$started){
                return [
                    'color' => 'green',
                    'description' => 'examNotStarted'
                ];
            }

        }catch (Exception $e){
            return false;
        }
    }
    
    /**
     * Retrieves a list of exams for a specific professor.
     * 
     * This function fetches the exams associated with the given professor ID from the database, 
     * including related group and test information. The exams are ordered by their ID in descending 
     * order, and a limit can be applied to restrict the number of exams returned.
     * 
     * @param int $professor_id The ID of the professor for whom to fetch the exams.
     * @param int $limit The maximum number of exams to return (default is 20).
     * 
     * @return array An associative array containing exam details, including:
     *               - exam attributes (e.g., ID, name, description, etc.)
     *               - professor group name
     *               - associated test name
     *               - exam status (examNotStarted, examStarted, examFinished)
     *               If no exams are found, an empty array is returned.
     * 
     * @throws PDOException If there is an error with the database query.
     */
    public static function getProfessorExams($professor_id, $limit = 20)
    {
        self::$conn = self::dbConn();
        try {
            // Prepare the SQL statement with placeholders
            $stmt = self::$conn->prepare("SELECT DISTINCT exam.*, 
                                                 professor_group.name AS group_name,
                                                 test.name AS test_name
                                          FROM exam
                                          JOIN `professor_group` ON `professor_group`.id = exam.group_id
                                          LEFT JOIN test ON test.group_id = professor_group.id
                                          WHERE exam.professor_id = :professor_id
                                          ORDER BY exam.id DESC
                                          LIMIT :limit");

            // Bind the parameters
            $stmt->bindParam(':professor_id', $professor_id, PDO::PARAM_INT);
            $stmt->bindParam(':limit', $limit, PDO::PARAM_INT);

            // Execute the statement
            $stmt->execute();

            // Fetch all rows as an associative array
            $res = $stmt->fetchAll(PDO::FETCH_ASSOC);

            // Loop through each exam and fetch article names
            foreach ($res as $key => &$exam) {
                // Decode the articles JSON column
                $articleIds = json_decode($exam['articles'], true); // Assuming articles column is JSON

                if(!is_array($articleIds)){
                    $articleIds = [$articleIds];
                }

                if ($articleIds) {
                    // Fetch article names for each article ID
                    $articleNames = [];
                    foreach ($articleIds as $articleId) {
                        // Fetch article name based on article ID
                        $articleStmt = self::$conn->prepare("SELECT name FROM article WHERE id = :article_id");
                        $articleStmt->bindParam(':article_id', $articleId, PDO::PARAM_INT);
                        $articleStmt->execute();
                        $article = $articleStmt->fetch(PDO::FETCH_ASSOC);
                        if ($article) {
                            $articleNames[] = $article['name'];
                        }
                    }
                    // Store the article names as a comma-separated string
                    $exam['article_names'] = implode(', ', $articleNames);
                } else {
                    $exam['article_names'] = ''; // No articles if the array is empty
                }

                $exam['status'] = self::getStartingStatus($exam['id'])['description'];
                $exam['image'] = \Config::imagePath('exam.png');
            }

            return $res; // Return the results with article names

        } catch (PDOException $e) {
            // Log or handle the exception appropriately
            error_log('Error: ' . $e->getMessage());
            return []; // Return an empty array or throw an exception
        }
    }

    public static function getStudentExams($student_id, $limit = 20)
    {
        try {
            $conn      = self::dbConn();
            $examsStmt = $conn->query("
                SELECT
                    e.*
                FROM
                    exam e
                JOIN group_members gm on
                    e.group_id = gm.group_id
                WHERE
                    gm.student_id = 2;
                ");

            return $examsStmt->fetchAll(PDO::FETCH_ASSOC);
        } catch (PDOException $e) {
            error_log('Error: ' . $e->getMessage());
            return [];
        }
    }

    public static function checkExamFinished($id)
    {
        $currentTimestamp = time();
        $examStarted      = self::findOneStarted($id);

        if($examStarted)
        {
            $startedTimestamp = strtotime($examStarted['started_at']);
            $duration         = $examStarted['duration']; 

            if ($startedTimestamp) {
                // Convert duration to seconds
                if ($duration <= 24) {
                    // If duration is less than or equal to 24, it's in hours
                    $durationInSeconds = $duration * 3600; // Convert hours to seconds
                } else {
                    // If duration is greater than 24, it's in minutes
                    $durationInSeconds = $duration * 60; // Convert minutes to seconds
                }

                // Calculate the end timestamp of the exam
                $endTimestamp = $startedTimestamp + $durationInSeconds;

                // Check if the current timestamp is greater than or equal to the end timestamp
                if ($currentTimestamp >= $endTimestamp) {
                    // Exam is finished
                    self::finishExam($id);
                }
            } 
        }
    }

}