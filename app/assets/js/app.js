
async function getTranslation(word) {
    const payload = { word: word };
  
    try {
        const data = await $.get("?controller=translation&action=make", payload);
        let response = JSON.parse(data);
        if (response.status === 200) {
            return response.word;
        } else {
            throw new Error('Translation failed');
        }
    } catch (error) {
        console.error('Error:', error);
        throw error;
    }
  }