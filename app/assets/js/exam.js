$(document).ready(function () {
    
    /*create exam*/    
    $('#submitArticleCreate').click(function () {

        const payload = {
            examName    : $('#examName').val(),
            examGroup   : $('#chooseGroop').val(),
            examArticles: $('#chooseArticle').val(),
            examTests   : $('#chooseTest').val(),
            examDuration: $('#chooseDuration').val(),
            examType: $('#chooseExamType').val()
        };

        $.post("?controller=exam&action=create", payload)
        .done(function (data) {
            let response = JSON.parse(data);
            if (response.status === 200) {
                location.reload();
            }
        });
    });

    /*start*/
    $('.startExamButton').click(function(event) {
        let examId = $(this).attr('data-id');
        let startedText = "<?=$startedText?>"
        let payload = { 
            examId: examId, 
        };

        $.post("?controller=exam&action=start", payload)
            .done(function(data) {
                let response = JSON.parse(data);
                if (response.status === 200) {
                    location.reload();
                }
            });
    });

    /*edit*/
    $('.showExamStatistics').click(function(e) {
        let examId = $(this).attr('data-id');
        $.post("?controller=exam&action=showResults", { id:  examId}, async function(response) {
            let parsedResponse = JSON.parse(response);
            if (parsedResponse.status === 200) {

                const badCount       = parsedResponse.data.badCount;
                const mediumCount    = parsedResponse.data.mediumCount;
                const goodCount      = parsedResponse.data.goodCount;
                const studentCount   = parsedResponse.data.studentCount;
                $('#participantsCountSpan').text(studentCount);

                $('#exam-id').val(examId);

                try {

                    renderChart(badCount, mediumCount, goodCount, studentCount);
                    renderTable(parsedResponse.data.eachStudentResult);
                    
                    $('#examStatisticsModal').modal('toggle');
                } catch (error) {
                    console.error('Error during translation:', error);
                }
            } else {
                console.log("Error: Unable to fetch data");
            }
        });
    });

    async function renderTable(eachStudentResult) {
        // Clear existing content
        document.getElementById('studentResultsTable').innerHTML = '';

        let fullname                = await getTranslation('fullname');
        let correctAnswers          = await getTranslation('correctAnswers');
        let incorrectAnswers        = await getTranslation('incorrectAnswers');
        let totalQuestions          = await getTranslation('totalQuestions');
        let examResult              = await getTranslation('examResult');
        let participantStatistics   = await getTranslation('participantStatistics');
        let value                   = await getTranslation('value');

        // Iterate through each student's result
        for (const data of eachStudentResult) {

            let result = JSON.parse(data.result);
            let studentId = data.student_id;
            let student = await getUser(studentId);
            let translatedCategory = await getTranslation(result.results.resultCategory.toLowerCase());
            console.log(student);

            // Calculate percentage of correct and incorrect answers
            const correctPercentage = ((Number(result.results.correctAnswers) / Number(result.results.quastionCount)) * 100).toFixed(2);
            const incorrectPercentage = ((Number(result.results.incorrectAnswers) / Number(result.results.quastionCount)) * 100).toFixed(2);

            // Create a unique ID for the chart
            const chartId = 'chart-' + studentId;

            // Build the table and chart HTML
            const studentHTML = `
                <div class="row mt-3 border border-info">
                    <div class="col-md-6">
                        <table class="table table-bordered">
                            <thead>
                                <tr>
                                    <th scope="col">${participantStatistics}</th>
                                    <th scope="col">${value}</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <th scope="row">${fullname}</th>
                                    <td>${student.fullname}</td>
                                </tr>
                                <tr>
                                    <th scope="row">${correctAnswers} (%)</th>
                                    <td>${correctPercentage}</td>
                                </tr>
                                <tr>
                                    <th scope="row">${incorrectAnswers} (%)</th>
                                    <td>${incorrectPercentage}</td>
                                </tr>
                                <tr>
                                    <th scope="row">${totalQuestions}</th>
                                    <td>${Number(result.results.quastionCount)}</td>
                                </tr>
                                <tr>
                                    <th scope="row">${examResult}</th>
                                    <td>${translatedCategory}</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <div class="col-md-6">
                        <canvas id="${chartId}" width="400" height="400"></canvas>
                    </div>
                </div>
            `;

            // Append the table and chart HTML to the studentResultsTable div
            document.getElementById('studentResultsTable').innerHTML += studentHTML;

            // Render the chart
            // Move the chart rendering to after the HTML has been appended
            setTimeout(() => {
                const ctx = document.getElementById(chartId).getContext('2d');
                new Chart(ctx, {
                    type: 'pie',
                    data: {
                        labels: [correctAnswers, incorrectAnswers],
                        datasets: [{
                            data: [result.results.correctAnswers, result.results.incorrectAnswers],
                            backgroundColor: ['rgba(75, 192, 192, 0.7)', 'rgba(255, 99, 132, 0.7)'],
                            hoverBackgroundColor: ['rgba(75, 192, 192, 1)', 'rgba(255, 99, 132, 1)'],
                            borderWidth: 1
                        }]
                    }
                });
            }, 500);  // Use a timeout to ensure the DOM is updated before rendering the chart
        }
    }

    async function renderChart(badCount, mediumCount, goodCount, studentCount) {
        let bad                 = await getTranslation('bad');
        let medium              = await getTranslation('medium');
        let good                = await getTranslation('good');
        let participantsCount   = await getTranslation('participantsCount');
        var ctxStudentsCountBar = document.getElementById('studentsCountBarChart').getContext('2d');

        // Check if the chart instance exists, and destroy it if it does
        if (window.studentsCountBarChart instanceof Chart) {
            window.studentsCountBarChart.destroy();
        }

        // Create a new chart instance
        window.studentsCountBarChart = new Chart(ctxStudentsCountBar, {
            type: 'bar',
            data: {
                labels: [bad, medium, good],
                datasets: [{
                    label: participantsCount,
                    data: [badCount, mediumCount, goodCount],
                    backgroundColor: ['rgba(255, 99, 132, 0.7)', 'rgba(255, 206, 86, 0.7)', 'rgba(75, 192, 192, 0.7)'],
                    borderColor: ['rgba(255, 99, 132, 1)', 'rgba(255, 206, 86, 1)', 'rgba(75, 192, 192, 1)'],
                    borderWidth: 1
                }]
            },
            options: {
                scales: {
                    y: {
                        beginAtZero: true
                    }
                }
            }
        });
    }
    
    async function getUser(userId) {
        try {
            const user = await $.post("?controller=user&action=findOne", { id: userId });
            return JSON.parse(user);
        } catch (error) {
            console.error("Error fetching user data:", error);
        }
    }

    async function getExam(id){
        try {
            const exam = await $.post("?controller=exam&action=findOne", { id: id });
            return JSON.parse(exam);
        } catch (error) {
            console.error("Error fetching exam data:", error);
        }
    }

    $('.editExam').click( async function () {
        const exam = await getExam($(this).data('id'));
        console.log(exam );
        $('#examName').val(exam.name);
        $('#chooseGroop').val(exam.group_id);
        $('#chooseDuration').val(Number(exam.duration));

        const articleSelect = $("#chooseArticle");

        articleSelect.val([JSON.parse(exam.articles)]);
        articleSelect.trigger('change');

        $('#createExamModal').modal('show');
    })
});
