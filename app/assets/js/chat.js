$(document).ready(function () {

    /*load conversations when modal opens*/
    $('#messagesModal').on('shown.bs.modal', function(){
        loadConversations();
    })

    /*user search*/
    $('.searchUsers').keyup(function () {
        $.get("?controller=user&action=list&text=" + encodeURIComponent($(this).val()), function(data) {
            appendUsers(JSON.parse(data));
        });
    });

    function loadConversations() {
        $.get("?controller=user&action=listConversations", function(data) {
            appendUsers(JSON.parse(data));
        });
    }

    /*append users to the left sidebar*/
    function appendUsers(users) {

        try {
            let filteredUsers = [];
            // Clear previous results
            $('.selectedMembers').empty();

            // Populate div with filtered results
            users.forEach(function(user) {
                let userDiv = $(`<div class="user-info alert alert-primary" data-id="${user.id}"></div>`);

                // Construct HTML content for each user
                let userInfo = `
                    <span>${user.username}</span>
                    <span>${user.userSurname}</span>
                    <span>ID: ${user.id}</span>
                `;

                // Append HTML content to the userDiv
                userDiv.html(userInfo);

                // Append userDiv to the selectedMembers div
                $('.selectedMembers').append(userDiv);
            });

        } catch (error) {
            console.error("Error parsing data:", error);
        }
    }

    async function getUser(userId) {
        try {
            const user = await $.post("?controller=user&action=findOne", { id: userId });
            return JSON.parse(user);
        } catch (error) {
            console.error("Error fetching user data:", error);
        }
    }

    let messageInterval; // Define interval variable outside the function

    function updateDM(user) {
        localStorage.setItem('user', JSON.stringify(user));
        updateNames(user);
        
        // Clear previous interval before setting a new one
        clearInterval(messageInterval);
        
        // Update messages for the current user immediately
        updateMessages(user.id);
        
        // Set interval to update messages periodically for the current user
        messageInterval = setInterval(function() {
            updateMessages(user.id);
        }, 1000);
    }

    function updateNames(user) {
        
        $('#dmUser img').remove();

        const dmUserDiv = $('#dmUser');

        const imagePath = user.imagePath ? '/app' + user.imagePath : 'https://i.pinimg.com/originals/ba/d7/86/bad786dfe4f227555be6fa2484b0b9a3.jpg';
        let img = document.createElement('img');
        img.src = imagePath;
        img.width = 100;
        img.height = 100;
        dmUserDiv.prepend(img);

        $('#username').text(user.username);
        $('#userSurname').text(user.userSurname);
    }

    async function updateMessages(userId) {
        try {
            const messages = await getDM(userId);
            const wrapper = $('.messageWrapper');
            
            // Clear the wrapper before appending new messages
            wrapper.empty();
            
            messages.forEach(message => {
                // Create a span element for the date and style it
                const span = $('<span></span>').text(message.date).css('color', 'gray');
                
                // Create a p element for the message text
                const p = $('<p class="single-message"></p>').text(message.text);
                
                // Create a div to wrap span and p based on sender or recipient
                const messageContainer = $('<div class="message-container"></div>');
                // Determine class and alignment based on sender or recipient
                if (message.recipient_id == userId) {
                    //my messages
                    messageContainer.addClass('alert alert-warning message-float-left');
                } else {
                    messageContainer.addClass('alert alert-primary message-float-right');
                }
                
                // Append span and p to messageContainer
                messageContainer.append(span).append(p);
                
                // Append messageContainer to the wrapper
                wrapper.append(messageContainer);

                //markMessageAsViewed(message);
            });

        } catch (error) {
            console.error("Error updating messages:", error);
        }
    }

    async function getDM(userId) {
        try {
            const dm = await $.post("?controller=message&action=getDM", { recipientId: userId });
            return JSON.parse(dm).messages;
        } catch (error) {
            console.error("Error fetching user data:", error);
        }
    }

    function sendMessage(text) {
        $('#message').val('');
        const user = JSON.parse(localStorage.getItem('user'));
        $.post("?controller=message&action=send", { recipientId: user.id, text : text });
        updateDM(user);
    }

    $('#sendMessage').click(function(){
        sendMessage($('#message').val());
    }) 

    $(document).on('click', '.user-info', async function() {
        $('.user-info').not(this).removeClass('alert alert-primary');
        $(this).addClass('alert alert-primary');
        let user = await getUser($(this).attr('data-id'));
        updateDM(user);
    });

});