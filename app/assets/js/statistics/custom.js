$(document).ready(async () => {
    loadExams();
});


async function loadExams() {
    try {
        // Fetch the exam data from the server
        const response = await fetch("?controller=exam&action=list", {
            method: "POST",
            headers: {
                "Content-Type": "application/json"
            },
        });
        const hourTrasnlation = await getTranslation('hour');
        const examNotStartedTranslation = await getTranslation('examNotStarted');
        const examStartedTranslation = await getTranslation('examStarted');
        const examFinishedTranslation = await getTranslation('examFinished');

        // Check if the response is okay (status code 200)
        if (!response.ok) {
            throw new Error("Failed to fetch exams data");
        }

        // Parse the JSON response
        const exams = await response.json();

        // Generate the list dynamically
        const $container = $(".flex.flex-column.gap10");

        exams.forEach(exam => {
        const statusData = exam.status; // This will be one of 'examNotStarted', 'examStarted', 'examFinished'

        let statusClass = "";
        let statusText = "";

        switch(statusData) {
            case "examNotStarted":
                statusClass = "block-available";
                statusText = examNotStartedTranslation;
                break;
            case "examStarted":
                statusClass = "block-ongoing";
                statusText = examStartedTranslation;
                break;
            case "examFinished":
                statusClass = "block-not-available";
                statusText = examFinishedTranslation
                break;
            default:
                statusClass = "block-not-available";
                statusText = "Unknown Status";
                break;
        }

            $container.append(`
                <li class="product-item gap14">
                    <div class="image">
                        <img src="${exam.image}" alt="${exam.name}">
                    </div>
                    <div class="flex items-center justify-between flex-grow">
                        <div class="name">
                            <a href="product-list.html" class="body-text">${exam.name}</a>
                        </div>
                        <div class="body-text">${exam.group_name}</div>
                        <div class="body-text">${exam.duration + ' ' + hourTrasnlation}</div>
                        <div class="body-text">${exam.article_names}</div>
                        <div class="body-text">${exam.test_name}</div>
                        <div class="${statusClass}">${statusText}</div>
                    </div>
                </li>
            `);
        });

    } catch (error) {
        console.error("Error loading exams:", error);
        alert("There was an error loading the exam data. Please try again later.");
    }
}
