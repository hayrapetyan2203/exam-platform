$(document).ready(function () {
    /*create*/
    $('#submitGroupCreate').click(function () {
        const payload = { 
            groupName: $('#groupName').val(), 
            groupMembers : $('#groupMembers').val() 
        };
        $.post( "?controller=group&action=create", payload)
            .done(function( data ) {
                if(JSON.parse(data).status === 200){
                    location.reload();
                }
        });
    })

    /*delete*/
    $('.groupDeleteButton').click(function(e) {
            let groupId = $(this).attr('data-id');
            $.get("?controller=group&action=delete&id=" + groupId, function(data) {
                let response = JSON.parse(data);
                if (response.status === 200) {
                    location.reload();
                }
            });
    });

    /*edit*/
    $('.groupEditButton').click(function(e) {
        let id = $(this).attr('data-id');
        let data = {};
        $.get("?controller=group&action=one&id=" + id, function(response) {
            let parsedResponse = JSON.parse(response);
            if (parsedResponse.status === 200) {
                data = parsedResponse.data;
                $('#groupName').val(data.group.name)

                    
                // // Create an array of member IDs
                // let memberIds = data.members.map(function(member) {
                //     return member.id;
                // });

                // // Set the selected options for the select2 element
                // $('#groupMembers').val(memberIds).trigger('change')

                


                $('#createGroupModal').modal('toggle');
            } else {
                console.log("Error: Unable to fetch data");
            }
        });
    });

});