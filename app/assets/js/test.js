//create test
function submitTestCreate() {
    console.log('create test');

    // Collect form input values
    let chooseGroop = $('#chooseGroop').val();
    let articles = $('#articles').val();
    let maxPoints = $('#maxPoints').val();
    let maxQuestions = $('#maxQuestions').val();
    let testName = $('#testName').val();
    
    let easyQuestionPoints = $('#easyQuestionPoints').val();
    let midQuestionPoints = $('#midQuestionPoints').val();
    let hardQuestionPoints = $('#hardQuestionPoints').val();

    let questionsArray = [];

    // Iterate over each question section
    $('.question-section .question').each(function () {
        let questionText = $(this).find('.question-text').val(); // Get question text
        let answers = []; // Array to hold answers

        // Iterate over each answer for the current question
        $(this).find('.answer').each(function () {
            answers.push($(this).val()); // Add answer to the answers array
        });

        let correctAnswerIndex = $(this).find('.correct-answer').eq(1).val(); // Get index of correct answer

        // ComidQuestionPointsnstruct question object
        let questionObject = {
            questionText: questionText,
            answers: answers,
            correctAnswerIndex: correctAnswerIndex,
            questionType:  $(this).find('.question-type').val(),
            questionDifficulty: $(this).find('.question-difficulty-level').val()
        };

        // Add question object to questions array
        questionsArray.push(questionObject);
    });

    // Construct payload
    let payload = {
        chooseGroop: chooseGroop,
        easyQuestionPoints: easyQuestionPoints,
        midQuestionPoints: midQuestionPoints,
        hardQuestionPoints: hardQuestionPoints,
        maxPoints: maxPoints,
        articles: articles,
        maxQuestions: maxQuestions,
        testName: testName,
        questions: questionsArray
    };

    console.log(payload);

    // Send payload to the server
    $.post("?controller=test&action=create", payload)
        .done(function (data) {
            let response = JSON.parse(data);
            if (response.status === 200) {
                location.reload();
            }
        });
}

$(document).ready(function (event) {

    // Step navigation
    $('#nextStep').click(function () {

        if ($('.step2').is(':visible')) {
            submitTestCreate();
        }

        $('.step1').hide();
        $('.step2').show();
    });
    $('#prevStep').click(function () {
        $('.step2').hide();
        $('.step1').show();
    });

    $('#moveRight').click(function () {
        $('#leftOptions option:selected').appendTo('#rightOptions');
    });

    $('#moveLeft').click(function () {
        $('#rightOptions option:selected').appendTo('#leftOptions');
    });

    $('#newOptionInput').keypress(function (event) {
        if (event.which == 13) { // Check if Enter key is pressed
            event.preventDefault(); // Prevent form submission
            var newOptionName = $(this).val();
            if (newOptionName.trim() !== '') { // Check if the input is not empty
                $('#leftOptions').append($('<option>', {
                    value: newOptionName,
                    text: newOptionName
                }));
                $(this).val(''); // Clear the input field after adding the option
            }
        }
    });
});


$('.groupDeleteButton').click(function (e) {
    let groupId = $(this).attr('data-id');
    $.get("?controller=group&action=delete&id=" + groupId, function (data) {
        let response = JSON.parse(data)
        if (response.status === 200) {
            location.reload();
        }
    });
})

$(document).ready(function () {
    // Function to toggle between question types
    function toggleQuestionType($question) {
        var selectedType = $question.find('.question-type').val();
        if (selectedType === 'multipleChoice') {
            $question.find('.multiple-choice-section').show();
            $question.find('.text-question-section').hide();
        } else {
            $question.find('.multiple-choice-section').hide();
            $question.find('.text-question-section').show();
        }
    }

    // Initialize toggle for the first question
    toggleQuestionType($('.question:first'));

    // Add Question Button Click Event
    $('.add-question').click(function () {
        var questionLimit = $('#maxQuestions').val(); // Get the question limit from step one
        var questionCount = $('.question-section .question').length; // Count the current questions
        if (questionCount < questionLimit) { // Check if the question limit is not reached
            var newQuestion = $('.question:first').clone(); // Clone the first question input
            newQuestion.find('.question-text').val(''); // Clear question text input
            newQuestion.find('.answer').val(''); // Clear answers
            newQuestion.find('.correct-answer').val(''); // Clear correct answer
            newQuestion.find('.text-answer').val(''); // Clear text answer input

            // Append new question
            $('.question-section').append(newQuestion);

            // Re-enable toggling for the new question
            toggleQuestionType(newQuestion);
            newQuestion.find('.question-type').on('change', function () {
                toggleQuestionType($(this).closest('.question'));
            });

            if (questionCount + 1 === questionLimit) {
                $('.add-question').prop('disabled', true); // Disable add button if the limit is reached
            }
            $('.remove-question').prop('disabled', false); // Enable remove buttons
        }
    });

    // Remove Question Button Click Event
    $('.question-section').on('click', '.remove-question', function () {
        var questionCount = $('.question-section .question').length; // Count the current questions
        if (questionCount > 1) { // Check if there is more than one question
            $(this).closest('.question').remove(); // Remove the closest question
            $('.add-question').prop('disabled', false); // Enable add button
        }
    });

    // Add Answer Button Click Event
    $('.question-section').on('click', '.add-answer', async function () {
        let $question = $(this).closest('.question');
        let answerCount = $question.find('.answer').length + 1;
        let answer = await getTranslation('answer');

        let newAnswer = `
            <div class="answer-wrapper d-flex align-items-center mt-1">
                <input type="text" class="form-control answer me-2"
                    placeholder="${answer + ' ' + answerCount}">
                <button type="button" class="btn btn-sm btn-danger remove-answer">X</button>
            </div>
        `;
        $question.find('.questionAnswers').append(newAnswer);
    });

    // Remove Answer Button Click Event
    $('.question-section').on('click', '.remove-answer', async function () {
        let answer = await getTranslation('answer');
        let $question = $(this).closest('.question');
        let $answers = $question.find('.answer-wrapper');

        if ($answers.length > 1) { // Ensure at least one answer remains
            $(this).closest('.answer-wrapper').remove();

            // Re-number the remaining answers
            $question.find('.answer').each(function (index) {
                $(this).attr('placeholder', answer + ' ' + (index + 1));
            });
        }
    });

    // Handle change event for dynamically created question types
    $('.question-section').on('change', '.question-type', function () {
        toggleQuestionType($(this).closest('.question'));
    });
});
