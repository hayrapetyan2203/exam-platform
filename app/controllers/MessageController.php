<?php

use backend\models\MessageModel;

require_once(__DIR__ . '/BaseController.php');
require_once(__DIR__ . '/../models/MessageModel.php');

/**
 * Class MessageController
 *
 * Handles operations related to messaging, including retrieving direct messages (DMs) and sending messages.
 */
class MessageController extends BaseController
{
    /**
     * Retrieves direct messages between the current user and a specified recipient.
     * 
     * This method fetches all messages exchanged between the logged-in user and the recipient identified by `recipientId`.
     * The messages are returned as a JSON response.
     *
     * @throws Exception If an error occurs during message retrieval.
     * @return void
     */
    public function getDM()
    {
        try {
            echo json_encode([
                'status' => 200, 
                'messages' => MessageModel::findAll($this->post['recipientId'], $_SESSION['user']['id'])
            ]);
        } catch (Exception $e) {
            throw new Exception($e->getMessage());
        }
    }    

    /**
     * Sends a message to a specified recipient.
     * 
     * This method sends a message from the logged-in user to the recipient identified by `recipientId`.
     * The status of the operation is returned as a JSON response.
     *
     * @throws Exception If an error occurs during message sending.
     * @return void
     */
    public function send()
    {
        try {
            MessageModel::send($this->post['recipientId'], $_SESSION['user']['id'], $this->post['text']);
            echo json_encode(['status' => 200]);
        } catch (Exception $e) {
            throw new Exception($e->getMessage());
        }
    }    

    // /**
    //  * Marks a message as viewed.
    //  * 
    //  * This method marks a specific message as viewed by the logged-in user.
    //  * The status of the operation is returned as a JSON response.
    //  *
    //  * @throws Exception If an error occurs during the operation.
    //  * @return void
    //  */
    // public function viewed()
    // {
    //     try {
    //         MessageModel::viewed($this->post['messageId'], $_SESSION['user']['id']);
    //         echo json_encode(['status' => 200]);
    //     } catch (Exception $e) {
    //         throw new Exception($e->getMessage());
    //     }
    // }
}
