<?php

require_once(__DIR__ . '/BaseController.php');

/**
 * Class StatisticsController
 *
 * Manages operations related to tests, including creating, deleting, and listing tests.
 */
class StatisticsController extends BaseController
{
    public function index()
    {
        try {
            $this->render('index', []);
        } catch (Exception $e) {
            throw new Exception($e->getMessage());
        }
    }

    /**
     * @throws Exception
     */
    public function students()
    {
        try {
            $this->render('students', []);
        } catch (Exception $e) {
            throw new Exception($e->getMessage());
        }
    }

    /**
     * @throws Exception
     */
    public function articleStats()
    {


        try {
            $this->render('articleStats', []);
        } catch (Exception $e) {
            throw new Exception($e->getMessage());
        }
    }
}
