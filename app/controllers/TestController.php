<?php

use backend\models\TestModel;

require_once(__DIR__ . '/BaseController.php');
require_once(__DIR__ . '/../models/TestModel.php');

/**
 * Class TestController
 *
 * Manages operations related to tests, including creating, deleting, and listing tests.
 */
class TestController extends BaseController
{
    /**
     * Creates a new test.
     * 
     * This method creates a new test using the data provided in the POST request. The ID of the current user is also included.
     * The status of the operation is returned as a JSON response.
     *
     * @throws Exception If an error occurs during test creation.
     * @return void
     */
    public function create()
    {
        try {
            TestModel::create($this->post, $_SESSION['user']['id']);
            echo json_encode(['status' => 200]);
        } catch (Exception $e) {
            throw new Exception($e->getMessage());
        }
    }

    /**
     * Deletes a test.
     * 
     * This method deletes a test identified by the ID provided in the GET request.
     * The status of the operation is returned as a JSON response.
     *
     * @throws Exception If an error occurs during test deletion.
     * @return void
     */
    public function delete()
    {
        try {
            TestModel::delete($this->get['id']);
            echo json_encode(['status' => 200]);
        } catch (Exception $e) {
            throw new Exception($e->getMessage());
        }
    }

    /**
     * Displays a list of tests.
     * 
     * This method renders the `testList` view, displaying a list of tests. The list of tests is retrieved from the `TestModel`.
     * The current locale is also passed to the view.
     *
     * @return void
     */
    public function list()
    {
        $this->render('testList', ['tests' => backend\models\TestModel::get(), 'locale' => $this->get['locale']]);
    }
}
