<?php
session_start();

/**
 * Class BaseController
 * 
 * Base controller that provides common functionalities for handling HTTP requests, 
 * rendering views, and managing session data.
 */
class BaseController
{
    /** @var array $post Contains the POST data */
    public $post = [];
    
    /** @var array $get Contains the GET data */
    public $get = [];
    
    /** @var array $session Contains the session data */
    public $session = [];

    /**
     * BaseController constructor.
     * 
     * Initializes the controller by setting up the POST, GET, and SESSION variables.
     */
    function __construct()
    {
        $this->post = $_POST;
        $this->get = $_GET;
        $this->session = $_SESSION;
    }

    /**
     * Throws a 404 Not Found exception.
     * 
     * This function is used to signal that the requested page could not be found.
     * 
     * @return void
     * @throws Exception Thrown when the requested page is not found.
     */
    public function notFound()
    {
        throw new Exception('The requested page could not be found', 404);
    }

    /**
     * Renders a view file.
     * 
     * This function renders the specified view with the given parameters. It also sets the application locale.
     * 
     * @param string $view The name of the view file to render.
     * @param array $params An array of parameters to pass to the view.
     * @param string|null $controllerName The name of the controller (optional).
     * 
     * @return void
     */
    public function render($view, array $params = [], $controllerName = null)
    {
        $controller = $controllerName ?? $this->get['controller'];
        $_SESSION['appLocale'] = $_GET['locale'];
        extract($params);
        require (__DIR__ . '/../views/' . $controller . '/' . $view . '.php');
    }

    /**
     * Imports a JavaScript or CSS file.
     * 
     * This function returns the HTML tag for importing a specified JavaScript or CSS file.
     * 
     * @param string $fileName The name of the file to import (without extension).
     * @param string $type The type of file ('js' or 'css').
     * 
     * @return string The HTML tag for importing the file.
     */
    public function import(string $fileName, string $type): string
    {
        if($this->isWindowsOs()){
            $path = '/app/assets/'.$type.'/'.$fileName.'.'.$type;
        }else{
            $path = '/assets/'.$type.'/'.$fileName.'.'.$type;
        }

        switch ($type) {
            case 'js':
                return '<script src="'.$path.'"></script>';
            case 'css':
                return '<link rel="stylesheet" href="'.$path.'">';            
            default:
                return '';
        }
    }

    public function isWindowsOs(): bool
    {
        return strtoupper(substr(PHP_OS, 0, 3)) === 'WIN';
    }

    public function cdnFactory(string $cdn)
    {
        switch ($cdn) {
            case 'fontawesome' : {
                return $this->fontawesome();
            }
        }
    }

    public function fontawesome(): string
    {
        return '<link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css" rel="stylesheet">';
    }
}
