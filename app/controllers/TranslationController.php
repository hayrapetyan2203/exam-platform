<?php

require_once(__DIR__ . '/BaseController.php');

/**
 * Class TranslationController
 *
 * Manages translation-related operations.
 */
class TranslationController extends BaseController
{
    /**
     * Translates a word.
     * 
     * This method translates a given word based on the current application locale. The translated word is returned as a JSON response.
     *
     * @throws Exception If an error occurs during the translation process.
     * @return void
     */
    public function make()
    {
        try {
            echo json_encode([
                'status' => 200,
                'word' => Translation::make($_SESSION['appLocale'], $this->get['word'])
            ]);
        } catch (Exception $e) {
            throw new Exception($e->getMessage());
        }
    }    
}
