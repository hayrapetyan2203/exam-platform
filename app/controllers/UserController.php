<?php
require_once(__DIR__ . '/BaseController.php');
require_once(__DIR__ . '/../models/UserModel.php');

/**
 * Class UserController
 *
 * Manages user-related operations such as listing, editing, and retrieving user information.
 */
class UserController extends BaseController
{
    /**
     * Retrieves a list of all students.
     * 
     * This method returns a JSON-encoded list of students.
     *
     * @return void
     */
    public function studentList()
    {
        echo json_encode(UserModel::listStudents());
    }

    /**
     * Retrieves a filtered list of users.
     * 
     * This method returns a JSON-encoded list of users filtered by the provided search text and the current user's ID.
     *
     * @return void
     */
    public function list()
    {
        echo json_encode(UserModel::list($this->get['text'], $_SESSION['user']['id'], true));
    }

    /**
     * Retrieves a list of conversations for the current user.
     * 
     * This method returns a JSON-encoded list of conversations that the current user is part of.
     *
     * @return void
     */
    public function listConversations()
    {
        echo json_encode(UserModel::listConversations($_SESSION['user']['id']));
    }

    /**
     * Retrieves a single user by ID.
     * 
     * This method returns a JSON-encoded representation of the user identified by the provided ID.
     *
     * @return void
     */
    public function findOne()
    {
        echo json_encode(UserModel::findOne($this->post['id']));
    }

    /**
     * Retrieves a list of users by their IDs.
     * 
     * This method returns a JSON-encoded list of users identified by the provided IDs.
     *
     * @return void
     */
    public function getUsersById()
    {
        echo json_encode(UserModel::getUsersById($this->get['ids']));
    }

    /**
     * Edits a user's information.
     * 
     * This method updates a user's information based on the provided data and returns a JSON-encoded status code.
     *
     * @throws Exception If an error occurs during the user update process.
     * @return void
     */
    public function edit()
    {
        try {
            $userInfo = [
                'id'              => $this->post['id'],
                'username'        => $this->post['username'],
                'userSurname'     => $this->post['userSurname'],
                'userFathername'  => $this->post['userFathername'],
                'useremail'       => $this->post['useremail'],
                'phone'           => $this->post['phone'],
                'address'         => $this->post['address'],
                'imagePath'       => $this->post['imagePath'],
            ];

            echo json_encode(['status' => UserModel::edit($userInfo) ? 200 : 500]);
        } catch (Exception $e) {
            throw new Exception($e->getMessage());
        }
    }
}
