<?php
require_once(__DIR__ . '/BaseController.php');
require_once(__DIR__ . '/../models/UserModel.php');
require_once(__DIR__ . '/../models/ArticleModel.php');
require_once(__DIR__ . '/../models/TestModel.php');
require_once(__DIR__ . '/../services/SecurityService.php');

use backend\services\SecurityService;
/**
 * Class AuthController
 *
 * Handles user authentication, registration, activation, and profile management.
 */
class AuthController extends BaseController
{
    /**
     * Registers a new user.
     * 
     * This method handles the user registration process and renders the appropriate view.
     *
     * @throws Exception If an error occurs during registration.
     * @return void
     */
    public function register()
    {
        try {
            if($this->checkPasswordInCommonWords($this->post['password'])) {
                $this->render('error', ['errorTranslationKey' => 'commonPasswordError']);
            }
            UserModel::register($this->post);
            $this->render('confirmEmail', ['useremail' => $this->post['useremail']]);
        } catch (Exception $e) {
            $sqlErrors = require_once(__DIR__ . '/../messages/sqlErrors.php');
            $this->render('error', ['errorTranslationKey' => $sqlErrors[$e->getCode()]]);
        }
    }

    public function checkPasswordInCommonWords($password): bool
    {
        $commonWords = [
            "Հայաստան", "հայ", "Երևան", "սեր", "բարեւ", "կյանք", "դպրոց", "ընկեր", "մայրիկ",
            "հայրիկ", "աշխատանք", "գիրք", "երեխան", "սիրելիս", "արև", "ընտանիք", "հավատ",
            "եկեղեցի", "հայրենիք", "երջանկություն", "երթուղի", "տոն", "ոսկի", "ընկերություն",
            "բարեկամ", "հույս", "ազատություն", "քաղաք", "փող", "բանկ", "սար", "լեռ", "գետ",
            "ջուր", "տուն", "ապրել", "խաղ", "երեխա", "ուսուցիչ", "աշակերտ", "դաս", "երգ",
            "խոսել", "լսել", "տեսնել", "գրել", "հայերեն", "լեզու", "եղանակ", "շուն", "կատու",
            "ծառ", "ծաղիկ", "ճանապարհ", "մեքենա", "մետրո", "դասարան", "համալսարան", "նկար",
            "արվեստ", "քույր", "եղբայր", "պապիկ", "տատիկ", "հանգստանալ", "ճամփորդություն",
            "պատմություն", "հայրենասիրություն", "գիտելիք", "երաժշտություն", "խաղալ", "ուրախություն",
            "նամակ", "հեռախոս", "համակարգիչ", "ինտերնետ", "նկարիչ", "գիտություն", "առողջություն",
            "բժշկություն", "ապագա", "հիշողություն", "նպատակ", "հաջողություն", "կանաչ", "կարմիր",
            "սև", "սպիտակ", "ծով", "լուսին", "աստղ", "երկինք", "ժպիտ", "խոսք", "սեղան",
            "աթոռ", "հագուստ", "շոր", "կոշիկ", "գրիչ", "մատիտ", "փողոց", "խանութ", "շուկա",
            "պաղպաղակ", "հաց", "ջեմ", "թեյ", "սուրճ", "շաքար", "կարագ", "կարագով հաց", "պանիր"
        ];

        foreach ($commonWords as $word) {
            if (mb_strpos($password, $word) !== false) {
                return true;
            }
        }

        return false;
    }

    /**
     * Activates a user account.
     * 
     * This method handles account activation based on the provided activation code and renders the appropriate view.
     *
     * @return void
     */
    public function activate()
    {
        $user = UserModel::findByEmail($this->post['useremail']);

        if ($user && (int)$user['activation_code'] === (int)$this->post['activationCode']) {
            if (isset($this->get['additional']) && $this->get['additional'] == 'forgotPassword') {
                $this->render('setNewPassword', ['useremail' => $this->post['useremail']]);
            } else {
                $this->render('activated');
            }
        } else {
            $this->render('notActivated');
        }
    }

    /**
     * Activates a user account after resetting the password.
     * 
     * This method sets a new password for the user and activates the account.
     *
     * @throws Exception If an error occurs during activation.
     * @return void
     */
    public function activateAfterForgotPassword()
    {
        try {
            UserModel::setNewPassword($this->post['useremail'], password_hash($this->post['password'], PASSWORD_BCRYPT));
            $this->render('activated');
        } catch (Exception $e) {
            $this->render('loginError', ['errorTranslationKey' => 'activationFailed']);
        }
    }

    /**
     * Re-sends the activation email.
     * 
     * This method generates a new activation code and sends it to the user's email.
     *
     * @throws Exception If an error occurs during the process.
     * @return void
     */
    public function approve()
    {
        try {
            UserModel::newActivation($this->post['useremail']);

            $url = parse_url($_SERVER['HTTP_REFERER']);
            parse_str($url['query'], $params);
            $this->render('confirmRecoveredEmail', ['useremail' => $this->post['useremail'], 'additional' => $params['action']]);
        } catch (Exception $e) {
            $this->render('loginError', ['errorTranslationKey' => 'activationFailed']);
        }
    }

    /**
     * Logs in the user.
     *
     * This method authenticates the user and redirects them to the appropriate profile page.
     *
     * @return void
     * @throws Exception
     */
    public function login()
    {
        if (SecurityService::checkLoginAttempts($this->post)) {
            $this->render('loginError', ['errorTranslationKey' => 'tooManyLoginAttempts']);
            return;
        }
        $isHash = false;
        if (!isset($this->post['useremail']) && isset($_SESSION['user'])) {
            $this->post = $_SESSION['user'];
            $isHash = true;
        }

        $user = UserModel::findByEmail($this->post['useremail']);

        if (!$user) {
            $this->render('loginError', ['errorTranslationKey' => 'userNotFound']);
            return;
        }

        if (password_verify($this->post['password'], $user['password'])) {
            $_SESSION['user'] = $user;

            if ((int)$user['role'] === UserModel::ROLE_PROFESSOR) {
                $params = [
                    'groups' => UserModel::getProfessorGroups($user['id'], 3),
                    'articles' => backend\models\ArticleModel::get(),
                    'tests' => backend\models\TestModel::get(3),
                    'is_windows_os' => $this->isWindowsOs()
                ];
                $this->render('professor_profile', $params);
            } else {
                $this->render('student_profile');
            }
        } else {
            if ($isHash && $this->post['password'] === $user['password']) {
                $_SESSION['user'] = $user;

                if ((int)$user['role'] === UserModel::ROLE_PROFESSOR) {
                    $params = [
                        'groups' => UserModel::getProfessorGroups($user['id'], 3),
                        'articles' => backend\models\ArticleModel::get(),
                        'tests' => backend\models\TestModel::get(3),
                        'is_windows_os' => $this->isWindowsOs()
                    ];
                    $this->render('professor_profile');
                } else {
                    $this->render('student_profile');
                }
            } else {
                SecurityService::addFailLoginAttempt($this->post);
                $this->render('loginError', ['errorTranslationKey' => 'incorrectPassword']);
            }
        }
    }

    public function isWindowsOs(): bool
    {
        return strtoupper(substr(PHP_OS, 0, 3)) === 'WIN';
    }

    /**
     * Renders the forgot password page.
     * 
     * This method displays the forgot password form.
     *
     * @return void
     */
    public function forgotPassword()
    {
        $this->render('forgotPassword');
    }

    /**
     * Logs out the current user.
     * 
     * This method ends the user's session and redirects them to the login page.
     *
     * @return void
     */
    public function logout()
    {
        if (isset($_SESSION['user'])) {
            unset($_SESSION['user']);
        }

        $locale = 'hy';
        if (isset($_SESSION['appLocale'])) {
            $locale = $_SESSION['appLocale'];
        }
        require_once(__DIR__ . '/../redirect.php');
    }
}
