<?php

use backend\models\ExamModel;

require_once(__DIR__ . '/BaseController.php');
require_once(__DIR__ . '/../models/ExamModel.php');

/**
 * Class ExamController
 * 
 * Controller for handling operations related to exams such as creation, deletion, 
 * starting exams, saving results, and retrieving exam details.
 */
class ExamController extends BaseController
{
    /**
     * Create a new exam.
     * 
     * This function handles the creation of a new exam with the provided details.
     * It responds with a JSON object containing the status and result.
     * 
     * @return void
     */
    public function create()
    {
        try {
            echo json_encode([
                'status' => 200,
                'result' => ExamModel::create(
                    $this->post['examType'],
                    $this->post['examName'],
                    $this->post['examGroup'],
                    $this->post['examDuration'],
                    implode(',', $this->post['examArticles']),
                    implode(',', $this->post['examTests']),
                    $_SESSION['user']['id']
                )
            ]);
        } catch (Exception $e) {
            echo json_encode(['status' => 500, 'error' => $e->getMessage()]);
        }
    }

    /**
     * Delete an exam.
     * 
     * This function deletes an exam based on the provided exam ID.
     * It responds with a JSON object containing the status of the operation.
     * 
     * @return void
     * @throws Exception If the deletion fails.
     */
    public function delete()
    {
        try {
            ExamModel::delete($this->get['id']);
            echo json_encode(['status' => 200]);
        } catch (Exception $e) {
            throw new Exception($e->getMessage());
        }
    }

    /**
     * Start an exam.
     * 
     * This function starts the exam with the given ID and returns the started exam's details.
     * It responds with a JSON object containing the status and the started exam's data.
     * 
     * @return void
     * @throws Exception If starting the exam fails.
     */
    public function start()
    {
        try {
            ExamModel::start($this->post['examId']);
            echo json_encode(['status' => 200, 'startedExam' => ExamModel::findOneStarted($this->post['examId'])]);
        } catch (Exception $e) {
            throw new Exception($e->getMessage());
        }
    }

    /**
     * Save exam results.
     * 
     * This function saves the results of an exam for the current user.
     * It responds with a JSON object containing the status and a flag indicating whether the results were saved.
     * 
     * @return void
     * @throws Exception If saving the results fails.
     */
    public function saveExamResults()
    {
        try {
            $examId = (int) $this->post['examId'];
            $result = $this->post['result'];

            echo json_encode(['status' => 200, 'saved' => ExamModel::saveResults($examId, $result, (int) $_SESSION['user']['id'])]);
        } catch (Exception $e) {
            throw new Exception("Error Processing Request " . json_encode($e), 1);
        }
    }

    /**
     * Show exam results.
     * 
     * This function retrieves and displays the results of an exam based on the provided exam ID.
     * It responds with a JSON object containing the status and the exam results data.
     * 
     * @return void
     * @throws Exception If retrieving the results fails.
     */
    public function showResults()
    {
        try {
            $id = (int) $this->post['id'];
            echo json_encode(['status' => 200, 'data' => ExamModel::showResults($id)]);
        } catch (Exception $e) {
            throw new Exception("Error Processing Request " . json_encode($e), 1);
        }
    }

    /**
     * Get exam participants.
     * 
     * This function retrieves the participants of a specific exam.
     * It responds with a JSON object containing the status and the list of participants.
     * 
     * @return void
     * @throws Exception If retrieving the participants fails.
     */
    public function getParticipants()
    {
        try {
            $id = (int) $this->post['examId'];
            echo json_encode(['status' => 200, 'items' => ExamModel::getParticipants($id)]);
        } catch (Exception $e) {
            throw new Exception("Error Processing Request " . json_encode($e), 1);
        }
    }

    /**
     * Enter an exam.
     * 
     * This function checks if the exam is finished for the current student.
     * If not, it renders the exam entry page; otherwise, it renders a page indicating the exam is finished.
     * 
     * @return void
     */
    public function enter()
    {
        ExamModel::checkExamFinished($this->get['exam_id']);
        if (!ExamModel::findExamResultForStudent($this->get['exam_id'], $this->session['user']['id'])) {
            $this->render('enter', ['exam' => ExamModel::findOne($this->get['exam_id'])]);
        } else {
            $this->render('examIsFinishedForStudent', ['user' => $this->session['user']]);
        }
    }
    
    /**
     * List all exams for the professor.
     * 
     * This function retrieves and renders a list of all exams created by the current professor.
     * It handles both `POST` and `GET` requests:
     * - For `POST` requests, it returns a JSON-encoded list of exams.
     * - For `GET` requests, it renders an HTML view of the exam list.
     * 
     * If the request method is not `POST` or `GET`, it returns a 405 Method Not Allowed response.
     * 
     * @return void
     */
    public function list()
    {
        $userId = $_SESSION['user']['id'];
        $exams = ExamModel::getProfessorExams($userId);

        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            $this->handlePostRequest($exams);
        } elseif ($_SERVER['REQUEST_METHOD'] === 'GET') {
            $this->handleGetRequest($exams);
        } else {
            http_response_code(405); // Method Not Allowed
            echo json_encode(['error' => 'Invalid request method']);
        }
    }

    /**
     * Handle POST request to return the list of exams as a JSON response.
     *
     * This method is called when the request method is `POST`. It sets the response
     * header to indicate JSON content and then returns the list of exams in JSON format.
     * 
     * @param array $exams List of exams to be returned in the response.
     * @return void
     */
    private function handlePostRequest(array $exams)
    {
        header('Content-Type: application/json');
        echo json_encode($exams);
    }

    /**
     * Handle GET request to render the exam list view.
     *
     * This method is called when the request method is `GET`. It renders the view
     * for the exam list, passing the list of exams as data to be displayed.
     * 
     * @param array $exams List of exams to be displayed in the view.
     * @return void
     */
    private function handleGetRequest(array $exams)
    {
        $this->render('examList', ['exams' => $exams]);
    }

    /**
     * List all exams for the student.
     * 
     * This function retrieves and renders a list of all exams available to the current student.
     * 
     * @return void
     */
    public function studentExamList()
    {
        $this->render('studentExamList', ['exams' => ExamModel::getStudentExams($_SESSION['user']['id'])]);
    }

    /**
     * Find one exam by ID.
     * 
     * This function retrieves the details of a specific exam based on the provided ID.
     * It responds with a JSON object containing the exam details.
     * 
     * @return void
     */
    public function findOne()
    {
        echo json_encode(ExamModel::findOne($this->post['id']));
    }
}
