<?php

use backend\models\GroupModel;

require_once(__DIR__ . '/BaseController.php');
require_once(__DIR__ . '/../models/GroupModel.php');
require_once(__DIR__ . '/../models/UserModel.php');

/**
 * Class GroupController
 *
 * Handles operations related to groups, including creation, deletion, listing, and viewing groups.
 */
class GroupController extends BaseController
{
    /**
     * Creates a new group.
     * 
     * This method creates a group using the provided group name and members, associating it with the current user.
     * Returns a JSON response indicating success or failure.
     *
     * @return void
     */
    public function create()
    {
        try {
            echo json_encode([
                'status' => 200,
                'result' => GroupModel::create($this->post['groupName'], $this->post['groupMembers'], $_SESSION['user']['id'])
            ]);
        } catch (Exception $e) {
            echo json_encode(['status' => 500, 'error' => $e->getMessage()]);
        }
    }

    /**
     * Deletes a group.
     * 
     * This method deletes the group specified by the provided group ID.
     * Throws an exception if the deletion fails.
     *
     * @throws Exception If an error occurs during deletion.
     * @return void
     */
    public function delete()
    {
        try {
            GroupModel::delete($this->get['id']);
            echo json_encode(['status' => 200]);
        } catch (Exception $e) {
            throw new Exception($e->getMessage());
        }
    }

    /**
     * Lists all groups for the current user.
     * 
     * This method renders a view displaying the list of groups associated with the current professor user.
     *
     * @return void
     */
    public function list()
    {
        $this->render('groupList', [
            'groups' => UserModel::getProfessorGroups($_SESSION['user']['id']),
            'locale' => $this->get['locale']
        ]);
    }

    /**
     * Views a single group.
     * 
     * This method renders a view showing details of a specific group based on the provided group ID.
     *
     * @return void
     */
    public function view()
    {
        $this->render('view', [
            'group' => GroupModel::findOne($this->get['id']),
            'members' => GroupModel::getMembersData($this->get['id']),
            'messages' => GroupModel::getMessages($this->get['id']),
        ]);
    }

    public function addMessage()
    {
        GroupModel::addMessage($this->post['message'], $this->post['user_id'], $this->get['id']);
        $this->render('view', [
            'group' => GroupModel::findOne($this->get['id']),
            'members' => GroupModel::getMembersData($this->get['id']),
            'messages' => GroupModel::getMessages($this->get['id']),
        ]);
    }

    /**
     * Retrieves and returns detailed information about a specific group.
     * 
     * This method retrieves group details and its members, then returns the data as a JSON response.
     * Throws an exception if an error occurs during retrieval.
     *
     * @throws Exception If an error occurs during data retrieval.
     * @return void
     */
    public function one()
    {
        try {
            $data['group'] = GroupModel::findOne($this->get['id']);
            $data['members'] = GroupModel::getMembers($this->get['id']);
            foreach ($data['members'] as $key => $member) {
                $data['members'][$key]['fullname'] = UserModel::findOne($member['student_id'])['fullname'];
            }
            echo json_encode(['status' => 200, 'data' => $data]);
        } catch (Exception $e) {
            throw new Exception($e->getMessage());
        }
    }

    /**
     * Lists all groups for a student user.
     * 
     * This method renders a view displaying the list of groups associated with the current student user.
     *
     * @return void
     */
    public function studentGroupList()
    {
        $this->render('studentGroupList', [
            'groups' => UserModel::getStudentGroups($_SESSION['user']['id']),
            'locale' => $this->get['locale']
        ]);
    }
}
