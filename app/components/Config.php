<?php

class Config
{
    public static function init(): bool
    {
        error_reporting(E_ALL);
        ini_set('display_errors', 1);

        $_SESSION['appLocale'] = isset($_GET['locale']) ? $_GET['locale'] : 'hy';
        return true;
    }

    public static function imagePath($path) : string {
        $is_windows_os = strtoupper(substr(PHP_OS, 0, 3)) === 'WIN';
        return  '/assets/img/statistics/' . $path;
    }
}