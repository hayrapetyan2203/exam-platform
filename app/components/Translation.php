<?php
class Translation
{
    public static function make($locale, $translationKey)
    {
        // Define the path to the messages directory
        $messagesDir = __DIR__ . '/../messages';

        // Construct the path to the locale-specific directory
        $localeDir = $messagesDir . '/' . $locale;

        // Check if the locale directory exists
        if (!is_dir($localeDir)) {
            $locale = 'en';
            $localeDir = $messagesDir . '/' . $locale;
        }

        // Construct the path to the app.php file inside the locale directory
        $appFilePath = $localeDir . '/app.php';

        // Check if the app.php file exists
        if (!file_exists($appFilePath)) {
            return "Translation file not found for locale '$locale'";
        }

        // Include the app.php file to get the translations
        $translations = include $appFilePath;

        // Check if the translation key exists in the translations array
        if (!array_key_exists($translationKey, $translations)) {
            return "Translation key '$translationKey' not found for locale '$locale'";
        }

        // Return the translation corresponding to the key
        return $translations[$translationKey];
    }
}

