-- MySQL dump 10.13  Distrib 8.0.41, for Linux (x86_64)
--
-- Host: localhost    Database: exam_platform
-- ------------------------------------------------------
-- Server version	8.0.41-0ubuntu0.24.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `article`
--

DROP TABLE IF EXISTS `article`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `article` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `article`
--

LOCK TABLES `article` WRITE;
/*!40000 ALTER TABLE `article` DISABLE KEYS */;
INSERT INTO `article` VALUES (1,'Ինֆորմացիոն անվտանգություն'),(2,'ՏԳԹՊ'),(3,'Վեբ ծրագրավորում');
/*!40000 ALTER TABLE `article` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `exam`
--

DROP TABLE IF EXISTS `exam`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `exam` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(256) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `group_id` int NOT NULL,
  `articles` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `professor_id` int NOT NULL,
  `duration` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `tests` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `exam`
--

LOCK TABLES `exam` WRITE;
/*!40000 ALTER TABLE `exam` DISABLE KEYS */;
INSERT INTO `exam` VALUES (2,'info sec exam',10,'\"1\"',1,'2','\"12\"',NULL),(3,'test',11,'\"2\"',1,'1','\"12\"','1');
/*!40000 ALTER TABLE `exam` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `exam_result`
--

DROP TABLE IF EXISTS `exam_result`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `exam_result` (
  `id` int NOT NULL AUTO_INCREMENT,
  `exam_id` int NOT NULL,
  `student_id` int DEFAULT NULL,
  `result` text COLLATE utf8mb4_unicode_ci,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `exam_result`
--

LOCK TABLES `exam_result` WRITE;
/*!40000 ALTER TABLE `exam_result` DISABLE KEYS */;
/*!40000 ALTER TABLE `exam_result` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `exam_startings`
--

DROP TABLE IF EXISTS `exam_startings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `exam_startings` (
  `id` int NOT NULL AUTO_INCREMENT,
  `exam_id` int NOT NULL,
  `duration` tinyint NOT NULL,
  `started_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `is_finished` int NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `exam_startings`
--

LOCK TABLES `exam_startings` WRITE;
/*!40000 ALTER TABLE `exam_startings` DISABLE KEYS */;
/*!40000 ALTER TABLE `exam_startings` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `fail_login_attempts`
--

DROP TABLE IF EXISTS `fail_login_attempts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `fail_login_attempts` (
  `id` int NOT NULL AUTO_INCREMENT,
  `user` json DEFAULT NULL,
  `ip` varchar(100) DEFAULT NULL,
  `attempt_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `email` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `fail_login_attempts`
--

LOCK TABLES `fail_login_attempts` WRITE;
/*!40000 ALTER TABLE `fail_login_attempts` DISABLE KEYS */;
INSERT INTO `fail_login_attempts` VALUES (13,'{\"password\": \"Qajari98*\", \"useremail\": \"hayrapetyan2203@gmail.com\"}','127.0.0.1','2025-03-02 14:00:25','\"hayrapetyan2203@gmail.com\"');
/*!40000 ALTER TABLE `fail_login_attempts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `group_members`
--

DROP TABLE IF EXISTS `group_members`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `group_members` (
  `id` int NOT NULL AUTO_INCREMENT,
  `student_id` int NOT NULL,
  `group_id` int NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `group_members`
--

LOCK TABLES `group_members` WRITE;
/*!40000 ALTER TABLE `group_members` DISABLE KEYS */;
INSERT INTO `group_members` VALUES (7,2,10),(8,3,10),(9,2,11),(10,3,11);
/*!40000 ALTER TABLE `group_members` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `group_messages`
--

DROP TABLE IF EXISTS `group_messages`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `group_messages` (
  `id` int NOT NULL AUTO_INCREMENT,
  `message` text,
  `user_id` int NOT NULL,
  `group_id` int NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `group_messages`
--

LOCK TABLES `group_messages` WRITE;
/*!40000 ALTER TABLE `group_messages` DISABLE KEYS */;
INSERT INTO `group_messages` VALUES (2,'Barevner',1,10,'2024-12-24 15:53:07'),(3,'Barevner',1,10,'2024-12-24 15:53:07'),(4,'Barevner',1,10,'2024-12-24 15:53:07'),(5,'Barevner',1,10,'2024-12-24 15:53:07'),(6,'Barevner',1,10,'2024-12-24 15:53:07'),(7,'Barevner',1,10,'2024-12-24 15:53:07'),(8,'Barevner',1,10,'2024-12-24 15:53:43'),(9,'Barevner',1,10,'2024-12-24 15:54:08'),(10,'Barevner',1,10,'2024-12-24 15:54:27'),(11,'Barevner',1,10,'2024-12-24 15:54:43'),(12,'Barevner',1,10,'2024-12-24 15:54:47'),(13,'Barevner',1,10,'2024-12-24 15:55:44'),(14,'Barevner',1,10,'2024-12-24 15:56:13'),(15,'Barevner',1,10,'2024-12-24 15:56:18'),(16,'Barevner',1,10,'2024-12-24 15:56:35'),(17,'Barevner',1,10,'2024-12-24 15:56:51'),(18,'Barevner',1,10,'2024-12-24 15:56:55'),(19,'hello',1,11,'2024-12-24 15:57:47');
/*!40000 ALTER TABLE `group_messages` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `messages`
--

DROP TABLE IF EXISTS `messages`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `messages` (
  `id` int NOT NULL AUTO_INCREMENT,
  `sender_id` int NOT NULL,
  `recipient_id` int DEFAULT NULL,
  `text` text COLLATE utf8mb4_unicode_ci,
  `date` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `viewed` int NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `messages`
--

LOCK TABLES `messages` WRITE;
/*!40000 ALTER TABLE `messages` DISABLE KEYS */;
INSERT INTO `messages` VALUES (1,1,2,'Բարև','2024-06-09 13:45:39',0),(2,1,2,'hello','2025-03-02 14:19:00',0),(3,1,3,'barev','2025-03-02 14:25:00',0);
/*!40000 ALTER TABLE `messages` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `professor_group`
--

DROP TABLE IF EXISTS `professor_group`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `professor_group` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(254) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `professor_id` int NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `professor_group`
--

LOCK TABLES `professor_group` WRITE;
/*!40000 ALTER TABLE `professor_group` DISABLE KEYS */;
INSERT INTO `professor_group` VALUES (10,'tt-911','2024-06-09 11:02:52',1),(11,'test xumb','2024-12-24 15:57:25',1);
/*!40000 ALTER TABLE `professor_group` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `test`
--

DROP TABLE IF EXISTS `test`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `test` (
  `id` int NOT NULL AUTO_INCREMENT,
  `group_id` int DEFAULT NULL,
  `max_points` int NOT NULL,
  `max_questions` int NOT NULL,
  `name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `professor_id` int NOT NULL,
  `articles` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `finished` int NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `questions` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `test`
--

LOCK TABLES `test` WRITE;
/*!40000 ALTER TABLE `test` DISABLE KEYS */;
INSERT INTO `test` VALUES (12,10,10,10,'info sec 2024',1,'[\"1\"]',0,'2024-06-09 19:50:31','[{\"questionText\":\"Ինչո՞վ է տարբերվում հոսքային գաղտնագիրը Վերնամի գաղտնագրից։\",\"answers\":[\"Վերնամի գաղտնագրի բանալու երկարութունը կարող է լինել կարճ գաղտնագրվող տեքստի երկարությունից\",\"2-ճիշտ)Հոսքային գաղտնագրի բանալու երկարութունը կարող է լինել կարճ գաղտնագրվող տեքստի երկարությունից\",\"Հոսքային գաղտնագրի բանալու երկարութունը պետք է հավասար լինի  գաղտնագրվող տեքստի երկարությանը\"],\"correctAnswerIndex\":\"0\"},{\"questionText\":\"Ինչո՞վ է տարբերվում հոսքային գաղտնագիրը Վերնամի գաղտնագրից։\",\"answers\":[\"Վերնամի գաղտնագրի բանալու երկարութունը կարող է լինել կարճ գաղտնագրվող տեքստի երկարությունից\",\"2-ճիշտ)Հոսքային գաղտնագրի բանալու երկարութունը կարող է լինել կարճ գաղտնագրվող տեքստի երկարությունից\",\"Հոսքային գաղտնագրի բանալու երկարութունը պետք է հավասար լինի  գաղտնագրվող տեքստի երկարությանը\"],\"correctAnswerIndex\":\"1\"},{\"questionText\":\"Ինչո՞վ է տարբերվում հոսքային գաղտնագիրը Վերնամի գաղտնագրից։\",\"answers\":[\"Վերնամի գաղտնագրի բանալու երկարութունը կարող է լինել կարճ գաղտնագրվող տեքստի երկարությունից\",\"2-ճիշտ)Հոսքային գաղտնագրի բանալու երկարութունը կարող է լինել կարճ գաղտնագրվող տեքստի երկարությունից\",\"Հոսքային գաղտնագրի բանալու երկարութունը պետք է հավասար լինի  գաղտնագրվող տեքստի երկարությանը\"],\"correctAnswerIndex\":\"0\"},{\"questionText\":\"Ինչո՞վ է տարբերվում հոսքային գաղտնագիրը Վերնամի գաղտնագրից։\",\"answers\":[\"Վերնամի գաղտնագրի բանալու երկարութունը կարող է լինել կարճ գաղտնագրվող տեքստի երկարությունից\",\"2-ճիշտ)Հոսքային գաղտնագրի բանալու երկարութունը կարող է լինել կարճ գաղտնագրվող տեքստի երկարությունից\",\"Հոսքային գաղտնագրի բանալու երկարութունը պետք է հավասար լինի  գաղտնագրվող տեքստի երկարությանը\"],\"correctAnswerIndex\":\"0\"},{\"questionText\":\"Ինչո՞վ է տարբերվում հոսքային գաղտնագիրը Վերնամի գաղտնագրից։\",\"answers\":[\"Վերնամի գաղտնագրի բանալու երկարութունը կարող է լինել կարճ գաղտնագրվող տեքստի երկարությունից\",\"2-ճիշտ)Հոսքային գաղտնագրի բանալու երկարութունը կարող է լինել կարճ գաղտնագրվող տեքստի երկարությունից\",\"Հոսքային գաղտնագրի բանալու երկարութունը պետք է հավասար լինի  գաղտնագրվող տեքստի երկարությանը\"],\"correctAnswerIndex\":\"1\"},{\"questionText\":\"Ինչո՞վ է տարբերվում հոսքային գաղտնագիրը Վերնամի գաղտնագրից։\",\"answers\":[\"Վերնամի գաղտնագրի բանալու երկարութունը կարող է լինել կարճ գաղտնագրվող տեքստի երկարությունից\",\"2-ճիշտ)Հոսքային գաղտնագրի բանալու երկարութունը կարող է լինել կարճ գաղտնագրվող տեքստի երկարությունից\",\"Հոսքային գաղտնագրի բանալու երկարութունը պետք է հավասար լինի  գաղտնագրվող տեքստի երկարությանը\"],\"correctAnswerIndex\":\"1\"},{\"questionText\":\"Ինչո՞վ է տարբերվում հոսքային գաղտնագիրը Վերնամի գաղտնագրից։\",\"answers\":[\"Վերնամի գաղտնագրի բանալու երկարութունը կարող է լինել կարճ գաղտնագրվող տեքստի երկարությունից\",\"2-ճիշտ)Հոսքային գաղտնագրի բանալու երկարութունը կարող է լինել կարճ գաղտնագրվող տեքստի երկարությունից\",\"Հոսքային գաղտնագրի բանալու երկարութունը պետք է հավասար լինի  գաղտնագրվող տեքստի երկարությանը\"],\"correctAnswerIndex\":\"1\"},{\"questionText\":\"Ինչո՞վ է տարբերվում հոսքային գաղտնագիրը Վերնամի գաղտնագրից։\",\"answers\":[\"Վերնամի գաղտնագրի բանալու երկարութունը կարող է լինել կարճ գաղտնագրվող տեքստի երկարությունից\",\"2-ճիշտ)Հոսքային գաղտնագրի բանալու երկարութունը կարող է լինել կարճ գաղտնագրվող տեքստի երկարությունից\",\"Հոսքային գաղտնագրի բանալու երկարութունը պետք է հավասար լինի  գաղտնագրվող տեքստի երկարությանը\"],\"correctAnswerIndex\":\"1\"},{\"questionText\":\"Ինչո՞վ է տարբերվում հոսքային գաղտնագիրը Վերնամի գաղտնագրից։\",\"answers\":[\"Վերնամի գաղտնագրի բանալու երկարութունը կարող է լինել կարճ գաղտնագրվող տեքստի երկարությունից\",\"2-ճիշտ)Հոսքային գաղտնագրի բանալու երկարութունը կարող է լինել կարճ գաղտնագրվող տեքստի երկարությունից\",\"Հոսքային գաղտնագրի բանալու երկարութունը պետք է հավասար լինի  գաղտնագրվող տեքստի երկարությանը\"],\"correctAnswerIndex\":\"0\"},{\"questionText\":\"Ինչո՞վ է տարբերվում հոսքային գաղտնագիրը Վերնամի գաղտնագրից։\",\"answers\":[\"Վերնամի գաղտնագրի բանալու երկարութունը կարող է լինել կարճ գաղտնագրվող տեքստի երկարությունից\",\"2-ճիշտ)Հոսքային գաղտնագրի բանալու երկարութունը կարող է լինել կարճ գաղտնագրվող տեքստի երկարությունից\",\"Հոսքային գաղտնագրի բանալու երկարութունը պետք է հավասար լինի  գաղտնագրվող տեքստի երկարությանը\"],\"correctAnswerIndex\":\"1\"}]');
/*!40000 ALTER TABLE `test` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `users` (
  `id` int NOT NULL AUTO_INCREMENT,
  `username` varchar(256) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `userSurname` varchar(256) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `userFathername` varchar(256) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `password` varchar(256) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `role` int NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `activation_code` int NOT NULL,
  `is_active` int NOT NULL,
  `useremail` varchar(256) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` varchar(256) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `imagePath` varchar(512) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `fullname` varchar(700) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `useremail` (`useremail`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'Արմեն','Հայրապետյան','Մանվելի','$2y$10$KeZW4BJjloaPcXzAsAkR7OMz4kYkHUUAr0CQ3mYH059Xd4QZWanjK',1,'2024-06-08 18:08:48',100000,0,'hayrapetyan2203@gmail.com','+37477611707','Արտաշատ, Մխչյան 119/3','/uploads/avatar.jpg','Արմեն Հայրապետյան Մանվելի'),(2,'Անդրանիկ','Հարությունյան','Հակոբի','$2y$10$1oHX5rycnkD8Upnuu4ni7.MJCeIVp5yXsSR8qmNdSPqpLxaOsKP.S',0,'2024-06-08 21:55:03',100000,0,'ando2203@gmail.com',NULL,NULL,NULL,'Անդրանիկ Հարությունյան Հակոբի'),(3,'Արման','Ամիրխանյան','Սիրակի','$2y$10$JXhVRrpR9JEtJmYs6h1uwuBBnJQTD6Q65.xc756lNbkn.MbV0SoUa',0,'2024-06-08 21:57:27',100000,0,'arman2203@gmail.com',NULL,NULL,NULL,'Արման Ամիրխանյան Սիրակի'),(4,'ss','dd','dd','$2y$12$rlJ8c.KzYBpWbnXZoDk07OxeWzIZN7YXHSLyyRuuk/UfaJ0plHQjS',1,'2024-12-24 16:17:08',100000,0,'hayrapetyan2233@gmail.com',NULL,NULL,NULL,NULL),(5,'ss','sa','sdad','$2y$12$HYfif6WoAEIs5jg6WzeaNucmT02OmMeu5mbmyakRrjvRHG9h/OL9u',0,'2024-12-24 16:17:35',100000,0,'sadasd@mail.ru',NULL,NULL,NULL,NULL);
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2025-03-02 20:22:28
